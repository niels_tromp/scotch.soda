*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_General_Profile.robot


*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_{}_{2}.csv

*** Test Cases ***
TRB-2025 Employee Discount sale transaction €10 (CASH) SCOTCH good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 28-10-2020 |
    ...     | Goal: | Return an item with a variable amount discount |
    ...     | Expected result: | The return is processed correctly and is visible in the booking |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Apply a variable discount of 20 Euro |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | 5 | Create a return of the order |
    ...     | 6 | Checkout the return with a cash payment |
    ...     | 7 | Close the browser and log into the ADMIN |
    ...     | 8 | Go to Cookbook>Events |
    ...     | 9 | Process the financial events |
    ...     | 10 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 11 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good  Return  Discount  AUT
    [Setup]  run keywords
    ...         ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}  ${Environment}
    ...  AND    S_Login.Login                                       ADMIN  ${Organization}  Title=${Country}-FH-POS
    ...  AND    ADMIN_S_MainKeywords.DE_Select General Profile
    ...  AND    ADMIN_S_General_Profile.DE_Change Language          English
    ...  AND    SeleniumLibrary.Close Browser

                        S_Login.Login                                   POS  ${Organization}  Title=${Country}-FH-POS
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                        POS_S_Basket.Add Customer To Order              Firstname=Assistant
                        POS_S_Checkout.Add Discount                     Type=Employee Discount
                        POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2025
    ${Order_Nr}         POS_A_Basket.Assert Order Number
                        SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  ${Organization}  Title=${Country}-FH-POS
                        ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          run keywords  SeleniumLibrary.Close Browser
...             AND     S_Login.Login                                       ADMIN  ${Organization}  Title=${Country}-FH-POS
...             AND     ADMIN_S_MainKeywords.Select General Profile
...             AND     ADMIN_S_General_Profile.Change Language             German
...             AND     SeleniumLibrary.Close Browser
