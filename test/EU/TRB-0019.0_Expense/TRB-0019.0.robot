*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_General_Ledgers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_General_Ledgers.robot


Suite Setup     Close All Stations
Suite Teardown  Open All Stations

*** Variables ***
${testdir}=         ${CURDIR}

*** Keywords ***
Close All Stations
    [Documentation]
    ...  This setup will close all stations and the safe to end a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_More_Options.Close Cash Drawer            Amount=10  Correction=${True}  Correction_Reason=TRB-0019.0_Startposition
                            POS_S_More_Options.Close Safe                   Amount=10  Correction=${True}  Correction_Reason=TRB-0019.0_Startposition
                            POS_S_More_Options.Close Financial Period
                            [Teardown]  SeleniumLibrary.Close Browser

Open All Stations
    [Documentation]
    ...  This setup will open all stations and the safe to start a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
    S_Login.Login                                   POS  ${Organization}  Station=${False}
    POS_S_More_Options.Open Financial Period Via Popup
    POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10  Correction=${True}  Correction_Reason=TRB-0019.0_Start situation
    POS_S_More_Options.Open Safe Via GUI            Amount=10  Correction=${True}  Correction_Reason=TRB-0019.0_Start situation
    [Teardown]  SeleniumLibrary.Close Browser

*** Test Cases ***
TRB-0019.0 Expense
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 28-10-2020 |
    ...     | Goal: | Adding a new expense |
    ...     | Expected result: | A new expense is added to the 'Income and expenses list for the selected shop' |
    ...     Steps:
    ...     | Setup: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Close the open cash drawer with a total of 100 Euros |
    ...     | 2 | Close the open cash drawer with a total of 100 Euros |
    ...     | 3 | Close the open safe with a total of 100 dollars |
    ...     | 4 | Close the financial period |
    ...     | 5 | Close the browser |
    ...     | Test: |  |
    ...     | 6 | Log into the POS |
    ...     | 7 | Open a new financial period with the popup |
    ...     | 8 | Open the cash drawer with 100 Euros |
    ...     | 9 | Check the financial period number |
    ...     | 10 | Navigate to 'Income and expenses' |
    ...     | 11 | Enter a new expense with the given parameters |
    ...     | 12 | Verify the new expense is succesfully added |
    ...     | 13 | Close the cash drawer with the same amount |
    ...     | 14 | Close the financial period |
    ...     | 15 | Close the browser |
    ...     | 16 | Open the browser and log into the ADMIN |
    ...     | 17 | Select the correct store |
    ...     | 18 | Go to Cookbook>Events |
    ...     | 19 | Process the financial events of the financial period |
    ...     | 20 | Go to Finance>Overview and select the financial period |
    ...     | 21 | Verify the bookings |
    ...     | 22 | Go to Finance>General Ledgers |
    ...     | 23 | Verify the correct accounts for the event |
    ...     | 24 | Close the browser |
    ...     | Teardown: |  |
    ...     | 25 | Open the browser |
    ...     | 26 | Open a new financial period with the popup |
    ...     | 27 | Open the cash drawer with 100 Euros to create the start situation |
    ...     | 28 | Open the safe with 100 Euros to create the start situation |
    ...     | 29 | Close the browser |
    [Tags]  Opening_Closing  Expense
                            S_Login.Login                                       POS  ${Organization}  Station=${False}
                            POS_S_More_Options.Open Financial Period Via Popup
                            POS_S_More_Options.Open Cash Drawer Via GUI         Amount=10   Correction=${False}
                            POS_S_More_Options.Add Income Or Expense            Cost_Type=Expense  Type=Lieferkosten  Tax=High  Amount=10
    ...                                                                         Description=Extra delivery
    ${Financial_Period}=    POS_S_More_Options.Get Financial Period Nr
                            POS_S_More_Options.Close Cash Drawer                Amount=9  Correction=${False}
                            POS_S_More_Options.Close Financial Period
                            SeleniumLibrary.Close Browser

    S_Login.Login                                       ADMIN  ${Organization}
    ADMIN_S_MainKeywords.Select Store                   ${Organization}
    ADMIN_S_MainKeywords.Select Cookbook Events
    ADMIN_S_Cookbook_Events.Process Financial Event     Financial_Period=${Financial_Period}
    ADMIN_S_MainKeywords.Select Finance Overview
    ADMIN_S_Finance_Overview.Open Bookkeeping           ${Financial_Period}
    ADMIN_A_Finance_Overview.Verify Bookkeeping         Station=Station  Moment=Expense  Amount=10.00
    ADMIN_S_MainKeywords.Select Finance General Ledgers
    ADMIN_S_Finance_General_Ledgers.Search Ledgers          ${Financial_Period}
    ADMIN_A_Finance_General_Ledgers.Verify General Ledgers  Type=Expense  Destination=Expense  Amount=10.00
    [Teardown]  SeleniumLibrary.Close Browser
