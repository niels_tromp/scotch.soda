*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_{}_{2}.csv

*** Test Cases ***
TRB-2003.0 Return sales transaction Return For Other Size (CASH) SCOTCH good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 02-11-2020 |
    ...     | Goal: | Return an item for another size |
    ...     | Expected result: | The swap of products is succesful and visible in the booking |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Checkout the item with a cash payment |
    ...     | 4 | Create a return of the order |
    ...     | 5 | Search for the same product in a different size  |
    ...     | 6 | Checkout the return |
    ...     | 7 | Close the browser and log into the ADMIN |
    ...     | 8 | Go to Cookbook>Events |
    ...     | 9 | Process the financial events |
    ...     | 10 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 11 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good  Return
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}  ${Environment}
                        S_Login.Login                                   POS  ${Organization}
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                        POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2003.0
    ${Order_Nr}         POS_A_Basket.Assert Order Number
    ${Return_Order_Nr}  POS_S_Orders.Create Return                      ${Order_Nr}
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_2}
                        POS_S_Basket.Checkout Basket Without Paying     Remark=TRB-2003.0 return
                        SeleniumLibrary.Close Browser

                        S_Login.Login                                    ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                     ${Return_Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event  Order_Nr=${Return_Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Return_Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview  ${Return_Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview  ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser