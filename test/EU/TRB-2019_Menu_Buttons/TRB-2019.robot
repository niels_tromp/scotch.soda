*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Tasks.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Logout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Customers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Cash_Drawer.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot

Suite Setup     S_Login.Login                   POS  ${Organization}
Suite Teardown  SeleniumLibrary.Close Browser
*** Variables ***
${testdir}=             ${CURDIR}


*** Test Cases ***
TRB-2019a Main Menu
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 05-11-2020 |
    ...     | Goal: | Testing if all menu buttons are present en working |
    ...     | Expected result: | Al buttons work en screenshots are made of the screens |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Click on the Menu Button Basket and make a screenshot |
    ...     | 3 | Click on the Menu Button Search and make a screenshot |
    ...     | 4 | Click on the Menu Button Orders and make a screenshot |
    ...     | 5 | Click on the Menu Button Customers and make a screenshot |
    ...     | 6 | Click on the Menu Button Barcode, cancel the scanner and make a screenshot |
    ...     | 7 | Click on the Menu Button Cash Drawer and make a screenshot |
    ...     | 8 | Click on the Menu Button Tasks and make a screenshot |
    ...     | 9 | Click on the Menu Button More options and make a screenshot |
    ...     | 10 | Click on the Menu Button Logout and make a screenshot |

    ADMIN_S_MainKeywords.Take Screenshot  Menu Buttons

TRB-2019b Basket
    POS_E_Basket.Select Basket Menu
    ADMIN_S_MainKeywords.Take Screenshot  Basket

TRB-2019c Search
    POS_E_Search.Select Search Menu
    ADMIN_S_MainKeywords.Take Screenshot  Search

TRB-2019d Orders
    POS_E_Orders.Select Orders Menu
    ADMIN_S_MainKeywords.Take Screenshot  Orders

TRB-2019e Customers
    POS_E_Customers.Select Customers Menu
    ADMIN_S_MainKeywords.Take Screenshot  Customers

TRB-2019f Barcode
    POS_E_Barcode.Select Barcode Menu
    ADMIN_S_MainKeywords.Take Screenshot  Barcode scanner
    POS_E_Barcode.Cancel Barcode Scanner

TRB-2019g Cash Drawer
    POS_E_Cash_Drawer.Select Cash Drawer Menu

TRB-2019h Tasks
    POS_E_Tasks.Select Tasks Menu
    ADMIN_S_MainKeywords.Take Screenshot  Tasks

TRB-2019i More Options
    POS_E_More_Options.Select More Options Menu
    ADMIN_S_MainKeywords.Take Screenshot  More Options

TRB-2019j Logout
    POS_E_Logout.Logout
    ADMIN_S_MainKeywords.Take Screenshot  Logout

