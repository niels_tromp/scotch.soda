*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_{}_{2}.csv

*** Test Cases ***
TRB-2009 Standard Sale Multiple Products (CASH)
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 09-12-2020 |
    ...     | Goal: | Execute a standard sale transaction with multiple product types and check the financial booking |
    ...     | Expected result: | All products are added to the order and the financial bookings are correct |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Search for a Footwear product  |
    ...     | 4 | Search for a Acces product  |
    ...     | 5 | Search for a Garment product  |
    ...     | 6 | Checkout the item with a cash payment |
    ...     | 7 | Checkout the order with cash |
    ...     | 8 | Close the browser and log into the ADMIN |
    ...     | 9 | Go to Cookbook>Events |
    ...     | 10 | Process the financial events |
    ...     | 11 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 12 | Close the browser |
    [Tags]  Dayrun  Scotch-good  Licenced  Cash
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}  ${Environment}
                        S_Login.Login                                   POS  ${Organization}
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                        POS_S_Search.Search Product On Product Code     ${Dev_Licenced_Product_Code_2}  ${Dev_Licenced_Size_2}
                        POS_S_Search.Search Product On Product Code     ${Dev_Licenced_Product_Code_3}  ${Dev_Licenced_Size_1}
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_2}  ${Dev_Scotch_Size_2}
                        POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2009 Multiple products
    ${Order_Nr}         POS_A_Basket.Assert Order Number
                        SeleniumLibrary.Close Browser

                        S_Login.Login                                    ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                     ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event  Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order               ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview  ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview  ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser