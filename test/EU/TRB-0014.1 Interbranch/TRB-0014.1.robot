*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Purchase_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Stock_Receive_Goods.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Purchase_Orders.robot


*** Variables ***
${testdir}=     ${CURDIR}
${csv}=         ${testdir}${/}input${/}products.csv
${Receiver}=    AT003

*** Test Cases ***
TRB-0014.1 Interbranch
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 09-03-2021 |
    ...     | Goal: | Return an item with a variable amount discount |
    ...     | Expected result: | The return is processed correctly and is visible in the booking |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Apply a variable discount of 20 Euro |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | 5 | Create a return of the order |
    ...     | 6 | Checkout the return with a cash payment |
    ...     | 7 | Close the browser and log into the ADMIN |
    ...     | 8 | Go to Cookbook>Events |
    ...     | 9 | Process the financial events |
    ...     | 10 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 11 | Close the browser |
    [Tags]  #Dayrun  Interbranch  AUT
                    S_Login.Login                                       ADMIN  ${Organization}
                    ADMIN_S_MainKeywords.Select Store                   ${Organization}
                    ADMIN_S_MainKeywords.Select Purchase Orders
                    ADMIN_S_Purchase_Orders.Create New Purchase Order
                    ADMIN_S_Purchase_Orders.Set Supplier And Receiver   ${Organization}  ${Receiver}
                    ADMIN_S_Purchase_Orders.Enter Products              ${csv}
                    ADMIN_S_Purchase_Orders.Set Delivery Date
    ${Order_Nr}=    ADMIN_S_Purchase_Orders.Place Order
                    ADMIN_S_MainKeywords.Select Store                   ${Receiver}
                    ADMIN_S_MainKeywords.Select Purchase Orders
                    ADMIN_S_Purchase_Orders.Create New Shipment         ${Order_Nr}
                    ADMIN_S_Purchase_Orders.Receive Order               ${Order_Nr}
    [Teardown]      SeleniumLibrary.Close Browser

