*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_2021.1_{}_{2}.csv

*** Test Cases ***
TRB-2021.1 Price Modification Downwards with (CASH) SCOTCH good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 18-03-2021 |
    ...     | Goal: | Modify the original price downwards with a manual price correction |
    ...     | Expected result: | The price is changed and the bookings are as expected |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Apply a price modification down to €100 |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | 5 | Close the browser and log into the ADMIN |
    ...     | 6 | Go to Cookbook>Events |
    ...     | 7 | Process the financial events |
    ...     | 8 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 9 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good  Price Modification
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}  ${Environment}
                    S_Login.Login                                   POS  ${Organization}
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_S_Basket.Apply Price Modification           Amount=100

                    POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2021.1
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser

