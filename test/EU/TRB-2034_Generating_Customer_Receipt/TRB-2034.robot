*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_{}_{2}.csv

*** Test Cases ***
TRB-2034 Generating Customer Receipt(Invoice, SaleReceipt)
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 19-11-2020 |
    ...     | Goal: | Execute a standard sale transaction, print the receipt and check in the ADMIN if the document and invoice are created |
    ...     | Expected result: | The receipt is succefully created and is shown in the ADMIN |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Checkout the item with a cash payment |
    ...     | 3 | Print the receipt |
    ...     | 4 | Close the browser and log into the ADMIN |
    ...     | 5 | Go to Cookbook>Events |
    ...     | 6 | Process the financial events |
    ...     | 7 | Verify if the document and invoice tabs are showing an entry for the created receipt |
    ...     | Teardown: |  |
    ...     | 8 | Close the browser |
    [Tags]  Dayrun  Receipt
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}  ${Environment}
                    S_Login.Login                                   POS  ${Organization}
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2034
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    POS_S_Basket.Print Receipts
                    SeleniumLibrary.Close Browser

                S_Login.Login                                       ADMIN  ${Organization}
                ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                ADMIN_S_MainKeywords.Select Cookbook Events
                ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}  OrderID
                ADMIN_S_Cookbook_Events.Check Documents
                ADMIN_S_Cookbook_Events.Check Invoices
    [Teardown]  SeleniumLibrary.Close Browser

