*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_General_Ledgers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_General_Ledgers.robot

Suite Setup     Close And Open All Stations
Suite Teardown  Open All Stations

*** Variables ***
${testdir}=         ${CURDIR}
*** Keywords ***
Close And Open All Stations
    [Documentation]
    ...  This setup will close all stations and the safe to end a financial period. It will put 100 euro in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_More_Options.Close Cash Drawer            Amount=10   Correction=${True}  Correction_Reason=TRB-2028_Startposition
                            POS_S_More_Options.Close Safe
                            POS_S_More_Options.Close Financial Period
                            POS_S_More_Options.Open Financial Period Via GUI
                            POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10   Correction=${False}
                            POS_S_More_Options.Open Safe Via GUI
                            [Teardown]  SeleniumLibrary.Close Browser


Open All Stations
    [Documentation]
    ...  This setup will open all stations and the safe to start a financial period. It will put 100 euro in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
    S_Login.Login                                   POS  ${Organization}  Station=${False}
    POS_S_More_Options.Open Financial Period Via Popup
    POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10  Correction=${True}  Correction_Reason=TRB-2028_Start situation
    POS_S_More_Options.Open Safe Via GUI            Amount=10  Correction=${False}
    [Teardown]  SeleniumLibrary.Close Browser

*** Test Cases ***
TRB-2028_Move_Cash_With_Store_Cash_Limit
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 16-02-2021 |
    ...     | Goal: | Close the cash drawer after cash has been trasnferred to the safe because of the cash limit of €1000 |
    ...     | Expected result: | The cash drawer can be closed after transferring money to the safe and the correct bookings are shown in the ADMIN |
    ...     Steps:
    ...     | Setup: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Close the open cash drawer with a total of 100 euro |
    ...     | 3 | Close the open safe with a total of 100 euro |
    ...     | 4 | Close the financial period |
    ...     | 5 | Open the cash drawer with a total of 100 Euros |
    ...     | 6 | Open the Safe with a total of 100 Euros |
    ...     | 7 | Open the financial period |
    ...     | 8 | Close the browser |
    ...     | Test: |  |
    ...     | 9 | Log into the POS |
    ...     | 10 | Search for a Scotch product |
    ...     | 11 | Checkout ten items to go above 1000 euros and pay with cash |
    ...     | 12 | Check the cash balance of the cash drawer |
    ...     | 13 | Try to close the cash drawer with the actual balance |
    ...     | 14 | Acknowledge the cash limit popup |
    ...     | 15 | Transfer moeny from the cash drawer to the safe to get under €1000 cash |
    ...     | 16 | Close the safe |
    ...     | 17 | Check the financial period number |
    ...     | 18 | Close the financial period |
    ...     | 19 | Close the browser |
    ...     | 20 | Open the browser and log into the ADMIN |
    ...     | 21 | Select the correct store |
    ...     | 22 | Go to Cookbook>Events |
    ...     | 23 | Process the financial events of the financial period |
    ...     | 24 | Go to Finance>General Ledgers |
    ...     | 25 | Verify the correct accounts for the event |
    ...     | 26 | Go to Finance>Overview and select the financial period |
    ...     | 27 | Verify the bookings |
    ...     | 28 | Close the browser |
    ...     | Teardown: |  |
    ...     | 29 | Open the browser |
    ...     | 30 | Open a new financial period with the popup |
    ...     | 31 | Open the cash drawer with 100 Euros to create the start situation |
    ...     | 32 | Open the safe with 100 Euros to create the start situation |
    ...     | 33 | Close the browser |
    [Tags]  Opening_Closing
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                            POS_S_Basket.Add Same Product                   1  9
                            POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2028 Cash Limit
    ${Cash_Amount}          POS_S_More_Options.Get Cash Drawer Amount
                            POS_S_More_Options.Close Cash Drawer            Amount=${Cash_Amount}   Correction=${False}  Limit=${True}
                            POS_S_More_Options.Move Cash                    Source_Station=Sentinel  Destination_Station=Safe  Amount=60
                            POS_S_More_Options.Close Cash Drawer            Amount=59950  Correction=${False}  Limit=${True}
                            POS_S_More_Options.Close Safe                   Correction=False
    ${Financial_Period}=    POS_S_More_Options.Get Financial Period Nr
                            POS_S_More_Options.Close Financial Period
                            SeleniumLibrary.Close Browser

                            S_Login.Login                                           ADMIN  ${Organization}
                            ADMIN_S_MainKeywords.Select Store                       ${Organization}
                            ADMIN_S_MainKeywords.Select Cookbook Events
                            ADMIN_S_Cookbook_Events.Process Financial Event         Financial_Period=${Financial_Period}
                            ADMIN_S_MainKeywords.Select Finance General Ledgers
                            ADMIN_S_Finance_General_Ledgers.Search Ledgers          ${Financial_Period}
                            ADMIN_A_Finance_General_Ledgers.Verify General Ledgers  Type=Move Cash  Destination=Safe  Amount=600.00
                            ADMIN_S_MainKeywords.Select Finance Overview
                            ADMIN_S_Finance_Overview.Open Bookkeeping               ${Financial_Period}
                            ADMIN_A_Finance_Overview.Verify Bookkeeping         Station=Safe  Moment=Cash-Limit  Amount=600.00
                            [Teardown]  SeleniumLibrary.Close Browser
