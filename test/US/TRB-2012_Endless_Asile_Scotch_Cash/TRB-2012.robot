*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
TRB-2012 Endless Aisle (CASH) SCOTCH good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 15-12-2020 |
    ...     | Goal: | Execute a standard sale transaction and select the delivery option to check the endless Aisle flow |
    ...     | Expected result: | The delivery transaction is succeful and there are no bookings present |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Select the delivery option |
    ...     | 4 | Add a customer to the order |
    ...     | 5 | Checkout the item with a cash payment |
    ...     | 6 | Close the browser and log into the ADMIN |
    ...     | 7 | Go to Cookbook>Events |
    ...     | 8 | Process the financial events |
    ...     | 9 | Verify there are no bookings |
    ...     | Teardown: |  |
    ...     | 10 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good  Endless Aisle
                    S_Login.Login                                   POS  ${Organization}
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_S_Basket.Select Endless Aisle
                    POS_S_Basket.Add Customer To Order              Firstname=Frank
                    POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2012 Endless Aisle
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    SeleniumLibrary.Close Browser

    S_Login.Login                                       ADMIN  ${Organization}
    ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
    ADMIN_S_MainKeywords.Select Cookbook Events
    ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
    ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
    run keyword and expect error                        Financial events were not processed correctly
    ...                                                 ADMIN_E_Cookbook_Events.Select Tab Bookings
    ADMIN_A_Cookbook_Events.Verify Empty Booking
    [Teardown]     SeleniumLibrary.Close Browser

