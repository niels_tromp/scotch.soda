*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
TRB-2015 Change Seller Of The Order
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 08-12-2020 |
    ...     | Goal: | Add a different seller to an order |
    ...     | Expected result: | The transaction is succeful and the bookings are as expected |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Checkout the item with a cash payment |
    ...     | 4 | Close the browser and log into the ADMIN |
    ...     | 5 | Go to Cookbook>Events |
    ...     | 6 | Process the financial events |
    ...     | 7 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 8 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good

    S_Login.Login                                   POS  ${Organization}
    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
    POS_E_Basket.Select checkout
    POS_E_Checkout.Select Sold By
    POS_E_Checkout.Select Other Seller
    POS_E_Checkout.Select Cash
    POS_E_Checkout.Pay with Cash
    [Teardown]      SeleniumLibrary.Close Browser

