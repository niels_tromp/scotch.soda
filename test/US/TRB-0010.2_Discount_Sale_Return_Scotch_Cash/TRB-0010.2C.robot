*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking percentage_{}.csv

*** Test Cases ***
TRB-0010.2C Discount sale transaction 10% Return (CASH) SCOTCH good variable percentage
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 28-10-2020 |
    ...     | Goal: | Return an item with a fixed variable percentage discount |
    ...     | Expected result: | The return is processed correctly and is visible in the booking |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Apply a variable discount of 10 percent |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | 5 | Create a return of the order |
    ...     | 6 | Checkout the return with a cash payment |
    ...     | 7 | Close the browser and log into the ADMIN |
    ...     | 8 | Go to Cookbook>Events |
    ...     | 9 | Process the financial events |
    ...     | 10 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 11 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good  Return  Discount
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}
                        S_Login.Login                                   POS  ${Organization}
                        POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                        POS_S_Checkout.Add Discount                     Type=variable percentage  Amount=10
                        POS_S_Basket.Checkout Basket With Cash          Remark=TRB-0010.2C
    ${Order_Nr}         POS_A_Basket.Assert Order Number
    ${Return_Order_Nr}  POS_S_Orders.Create Return                      ${Order_Nr}
                        POS_S_Basket.Checkout Return With Cash          Remark=TRB-0010.2C_R
                        SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                        ${Return_Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Return_Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Return_Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Return_Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser

