*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking percentage_{}.csv

*** Test Cases ***
TRB-0010.3B Discount sale transaction 10% (EFT) License good variable percentage
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Sell an item with a discount with a variable percentage with EFT |
    ...     | Expected result: | The discount is aplied and visible in the booking |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Licenced product |
    ...     | 3 | Apply a variable percentage of 10 percent |
    ...     | 4 | Checkout the item with a EFT payment |
    ...     | 5 | Close the browser and log into the ADMIN |
    ...     | 6 | Go to Cookbook>Events |
    ...     | 7 | Process the financial events |
    ...     | 8 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 9 | Close the browser |
    [Tags]  Dayrun  EFT  Licenced  Discount
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}
                    S_Login.Login                                   POS  ${Organization}
                    POS_S_Search.Search Product On Product Code     ${Dev_Licenced_Product_Code_1}  ${Dev_Licenced_Size_1}
                    POS_S_Checkout.Add Discount                     Type=variable percentage  Amount=10
                    POS_S_Basket.Checkout Basket With Manual EFT    Remark=TRB-0010.3B
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser

