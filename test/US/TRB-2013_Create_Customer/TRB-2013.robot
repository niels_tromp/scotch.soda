*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Customers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Customers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Management_Customers.robot


*** Variables ***
${testdir}=             ${CURDIR}


*** Test Cases ***
TRB-2013 Creating a Customer
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 22-10-2020 |
    ...     | Goal: | Create a new customer in the POS and delete it afterwards in the Admin |
    ...     | Expected result: | The customer is correctly created, saved and deleted into the system |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Navigate to the customers menu and click on 'New' |
    ...     | 3 | Fill in all the fields |
    ...     | 4 | Create the new customer |
    ...     | 5 | Close the POS and open the ADMIN |
    ...     | 6 | Navigate to Management>Customers |
    ...     | 7 | Search for the new customer and delete the entry |
    ...     | Teardown: |  |
    ...     | 8 | Close the browser |
    [Tags]  Dayrun  Customer
                    S_Login.Login                                       POS  ${Organization}
                    POS_E_Customers.Select Customers Menu
                    POS_S_Customers.Create New Customer                 Firstname=Pieter  Lastname=Hamersma  Email=Pieter@Hamersma.gg
    ...                                                                 Phone=0612345678  Adress=Houtwed Reservoir  Zipcode=68943
    ...                                                                 Street=Houtwed Reservoir  Housenumber=12  City=Hardy
                    SeleniumLibrary.Close Browser

                    S_Login.Login                                       ADMIN  ${Organization}
                    ADMIN_S_MainKeywords.Select Management Customers
                    ADMIN_S_Management_Customers.Delete Customer        Name=Pieter
    [Teardown]      SeleniumLibrary.Close Browser

