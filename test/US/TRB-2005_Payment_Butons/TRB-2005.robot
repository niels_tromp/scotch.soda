*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot

*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
TRB-2005 Payment buttons
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 05-11-2020 |
    ...     | Goal: | Check of all payment buttons are visible and working |
    ...     | Expected result: | All payment buttons are present and working correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Check on the checkout screen if all Order options and checkout buttons are present |
    ...     | 4 | Click all order options and checkout buttons to verify if they work |
    ...     | Teardown: |  |
    ...     | 5 | Close the browser |
    [Tags]  Dayrun
    S_Login.Login                                   POS  ${Organization}
    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
    POS_E_Basket.Select checkout
    POS_A_Checkout.Verify All Buttons
    Pos_S_Checkout.Select All Buttons
    [Teardown]  SeleniumLibrary.Close Browser
