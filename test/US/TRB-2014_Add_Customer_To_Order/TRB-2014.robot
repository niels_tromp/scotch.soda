*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Customers.robot


*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
TRB-2014 Add Customer To Order
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 07-12-2020 |
    ...     | Goal: | Execute a standard sale transaction with adding a customer to the order |
    ...     | Expected result: | The transaction is succeful and the customer is added to the order |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Add a customer to the order |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | Teardown: |  |
    ...     | 5 | Close the browser |
    [Tags]  Dayrun  Customer  Cash  Scotch-good

                    S_Login.Login                                   POS  ${Organization}
#                    POS_E_Customers.Select Customers Menu
                    ### US CUSTOMER ####
#                    POS_S_Customers.Create New Customer             Firstname=Frank  Lastname=Peggy  Email=Frank@Peggy.gg
#    ...                                                             Phone=0612345678  Adress=Houtwed Reservoir  Zipcode=68943
#    ...                                                             Housenumber=12  City=Hardy
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_S_Basket.Add Customer To Order              Firstname=Frank
                    POS_S_Basket.Checkout Basket With Cash          Remark=TRB-2014 Add customer
    [Teardown]      SeleniumLibrary.Close Browser

