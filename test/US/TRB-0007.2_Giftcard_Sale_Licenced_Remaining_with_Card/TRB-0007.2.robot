*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Mainkeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_{}.csv
${Giftcard_Number}=     999000920001

*** Test Cases ***
TRB-0007.2 Giftcard Sale Under Order Amount, Rest Amount with Card-Licenced good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 24-11-2020 |
    ...     | Goal: | Sell a Scotch good by paying a portion with a giftcard and the rest with card |
    ...     | Expected result: | The product can be sold with the combination of a giftcard and card and the bookings are as expected |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Checkout a portion with a Giftcash and the remainder with card |
    ...     | 4 | Close the browser and log into the ADMIN |
    ...     | 5 | Go to Cookbook>Events |
    ...     | 6 | Process the financial events |
    ...     | 7 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 8 | Close the browser |
    [Tags]  Dayrun  Giftcard  EFT  Licenced
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}
                    S_Login.Login                                   POS  ${Organization}
                    POS_S_Search.Search Product On Product Code     ${Dev_Licenced_Product_Code_1}  ${Dev_Licenced_Size_1}
                    POS_S_Basket.Checkout Basket With Giftcard      ${Giftcard_Number}  Remark=TRB-0007.2
                    POS_S_MainKeywords.Wait For Payment to process
                    POS_E_Checkout.Select Manual EFT
                    POS_E_Checkout.Pay with Manual EFT
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  ${Organization}
                        ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser

