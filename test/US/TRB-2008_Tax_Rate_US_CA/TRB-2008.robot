*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot

*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
TRB-2008a Tax check California
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
      ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US004
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  Tax=8.50%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008b Tax check Colorado
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
      ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US016
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  Tax=8.81%
    [Teardown]      SeleniumLibrary.Close Browser

TRB-2008c Tax check Florida
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US001
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  7.00%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008d Tax check Georgia
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
      ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US037
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  8.90%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008e Tax check illinois
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
      ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US005
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  10.25%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008f Tax check New Jersey
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US018
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  0.00%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008g Tax check Nevada
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US003
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  8.38%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008h Tax check New York
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US006
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  8.88%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008i Tax check Oregon
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US015
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  0.00%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008j Tax check Pennsylvania
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US029
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  0.00%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008k Tax check South Carolina
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US030
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  9.00%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008l Tax check Texas
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US031
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  8.25%
    [Teardown]      SeleniumLibrary.Close Browser


TRB-2008m Tax check Washington
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 10-11-2020 |
    ...     | Goal: | Execute a standard sale transaction without paying and check the tax rate per state |
    ...     | Expected result: | The tax rate is displayed correctly |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Verify the tax amount |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  Scotch-good
                    S_Login.Login                                   POS  US014
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  10.10%
    [Teardown]      SeleniumLibrary.Close Browser
