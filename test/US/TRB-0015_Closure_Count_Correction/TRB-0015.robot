*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_General_Ledgers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_General_Ledgers.robot


Suite Setup     Close And Open All Stations
Suite Teardown  Open All Stations

*** Variables ***
${testdir}=         ${CURDIR}

*** Keywords ***
Close And Open All Stations
    [Documentation]
    ...  This setup will close all stations and the safe to end a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_More_Options.Close Cash Drawer            Amount=10   Correction=${True}  Correction_Reason=TRB-0015_Startposition
                            POS_S_More_Options.Close Safe                   Amount=10   Correction=${True}  Correction_Reason=TRB-0015_Startposition
                            POS_S_More_Options.Close Financial Period
                            POS_S_More_Options.Open Financial Period Via GUI
                            POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10   Correction=${False}
                            POS_S_More_Options.Open Safe Via GUI            Amount=10   Correction=${False}
                            [Teardown]  SeleniumLibrary.Close Browser


Open All Stations
    [Documentation]
    ...  This setup will open all stations and the safe to start a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
    S_Login.Login                                   POS  ${Organization}  Station=${False}
    POS_S_More_Options.Open Financial Period Via Popup
    POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10  Correction=${True}  Correction_Reason=TRB-0001_Start situation
    POS_S_More_Options.Open Safe Via GUI            Amount=10  Correction=${False}
    [Teardown]  SeleniumLibrary.Close Browser


*** Test Cases ***
TRB-0015 Closure Count Correction Shortage
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 18-11-2020 |
    ...     | Goal: | Close the cash drawer at the end of a Financial Period with a shortage of 10 dollar and check the financial booking |
    ...     | Expected result: | The cash drawer can be closed with a shortage and the correct bookings are shown in the ADMIN |
    ...     Steps:
    ...     | Setup: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Close the open cash drawer with a total of 100 dollars |
    ...     | 3 | Close the open safe with a total of 100 dollars |
    ...     | 4 | Close the financial period |
    ...     | 5 | Open the cash drawer with a total of 100 dollars |
    ...     | 6 | Open the Safe with a total of 100 dollars |
    ...     | 7 | Open the financial period |
    ...     | 8 | Close the browser |
    ...     | Test: |  |
    ...     | 9 | Log into the POS |
    ...     | 10 | Close the cash drawer with a shortage of 10 dollar |
    ...     | 11 | Close the safe with the exact same opening amount |
    ...     | 12 | Check the financial period number |
    ...     | 13 | Close the financial period |
    ...     | 14 | Close the browser |
    ...     | 15 | Open the browser and log into the ADMIN |
    ...     | 16 | Select the correct store |
    ...     | 17 | Go to Cookbook>Events |
    ...     | 18 | Process the financial events of the financial period |
    ...     | 19 | Go to Finance>Overview and select the financial period |
    ...     | 20 | Verify the bookings |
    ...     | 21 | Close the browser |
    ...     | Teardown: |  |
    ...     | 22 | Open the browser |
    ...     | 23 | Open a new financial period with the popup |
    ...     | 24 | Open the cash drawer with 100 dollars to create the start situation |
    ...     | 25 | Open the safe with 100 dollars to create the start situation |
    ...     | 26 | Close the browser |
    [Tags]  Opening_Closing
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_More_Options.Close Cash Drawer            Amount=9   Correction=${True}  Correction_Reason=TRB-0015_Shortage
                            POS_S_More_Options.Close Safe                   Amount=10  Correction=False
    ${Financial_Period}=    POS_S_More_Options.Get Financial Period Nr
                            POS_S_More_Options.Close Financial Period
                            SeleniumLibrary.Close Browser

                            S_Login.Login                                       ADMIN  ${Organization}
                            ADMIN_S_MainKeywords.Select Store                   ${Organization}
                            ADMIN_S_MainKeywords.Select Cookbook Events
                            ADMIN_S_Cookbook_Events.Process Financial Event     Financial_Period=${Financial_Period}
                            ADMIN_S_MainKeywords.Select Finance Overview
                            ADMIN_S_Finance_Overview.Open Bookkeeping          ${Financial_Period}
                            ADMIN_A_Finance_Overview.Verify Bookkeeping         Station=Station  Moment=Closing  Amount=10.00
                            ADMIN_S_MainKeywords.Select Finance General Ledgers
                            ADMIN_S_Finance_General_Ledgers.Search Ledgers          ${Financial_Period}
                            ADMIN_A_Finance_General_Ledgers.Verify General Ledgers  Type=Closing  Destination=Closing  Amount=10.00
                            [Teardown]  SeleniumLibrary.Close Browser



