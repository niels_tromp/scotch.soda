*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot

*** Variables ***
${testdir}=         ${CURDIR}
*** Test Cases ***
TRB-2032 Connect CompApp
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 19-01-2021 |
    ...     | Goal: | Check if a QR-code is correctly generated to connect the manual scanner to the station |
    ...     | Expected result: | The QR-code is correctly generated |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Login into the POS for the selected shop |
    ...     | 2 | Navigate to 'More options>Configure manual scanner' |
    ...     | 3 | Check if the QR-code is generated |
    ...     | Teardown: |  |
    ...     | 4 | Close the browser |
    [Tags]  Dayrun  CompApp
    S_Login.Login                               POS  ${Organization}
    POS_S_More_Options.Connect Companion App
    [Teardown]  SeleniumLibrary.Close Browser

