*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot

*** Variables ***
${Product_Name}=        Gift card
${Product_Code}=        intersolve
${Giftcard_Number}=     999000920000
${Amount}=              123
*** Test Cases ***
TRB-2030 Check Giftcard Amount
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 19-01-2021 |
    ...     | Goal: | Check if the correct giftcard amount is displayed in the POS after it is deposited |
    ...     | Expected result: | The giftcard contains the correct amount of credit |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Login into the POS for the selected shop |
    ...     | 2 | Search for a giftcard and put an specific amount on the card |
    ...     | 3 | Finalize the transaction |
    ...     | 4 | Navigate to 'More options>Check Giftcard Balance' |
    ...     | 5 | Fill in the giftcard number and check if this corresponds with the amount deposited |
    ...     | Teardown: |  |
    ...     | 6 | Close the browser |
    [Tags]  Dayrun  Cash  Giftcard
    S_Login.Login                               POS  ${Organization}
    POS_S_Search.Search Product On Name         ${Product_Name}  ${Product_Code}
    POS_S_Basket.Enter Giftcard Information     ${Giftcard_Number}  ${Amount}  ${Product_Code}
    POS_S_Basket.Checkout Basket With Cash      Remark=TRB-2030
    POS_S_More_Options.Check Giftcard Amount    ${Giftcard_Number}
    POS_A_More_Options.Verify Giftcard Balance  ${Amount}
    [Teardown]  SeleniumLibrary.Close Browser

