*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Finance_General_Ledgers.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_Overview.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Finance_General_Ledgers.robot



Suite Setup     Close All Stations
Suite Teardown  Open All Stations

*** Variables ***
${testdir}=         ${CURDIR}

*** Keywords ***
Close All Stations
    [Documentation]
    ...  This setup will close all stations and the safe to end a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
                            S_Login.Login                                   POS  ${Organization}
                            POS_S_More_Options.Close Cash Drawer            Amount=10  Correction=${True}  Correction_Reason=TRB-0001_Startposition
                            POS_S_More_Options.Close Safe                   Amount=10  Correction=${True}  Correction_Reason=TRB-0001_Startposition
                            POS_S_More_Options.Close Financial Period
                            [Teardown]  SeleniumLibrary.Close Browser

Open All Stations
    [Documentation]
    ...  This setup will open all stations and the safe to start a financial period. It will put 100 dollar in the station
    ...  and 100 in the safe as begin situation for the tests. Afterwards the GUI will be closed.
    S_Login.Login                                   POS  ${Organization}  Station=${False}
    POS_S_More_Options.Open Financial Period Via Popup
    POS_S_More_Options.Open Cash Drawer Via GUI     Amount=10  Correction=${True}  Correction_Reason=TRB-0001_Start situation
    POS_S_More_Options.Open Safe Via GUI            Amount=10  Correction=${True}  Correction_Reason=TRB-0001_Start situation
    [Teardown]  SeleniumLibrary.Close Browser


*** Test Cases ***
TRB-0001.1 Count Money In Cash Drawer 10 Dollar Surplus
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 16-11-2020 |
    ...     | Goal: | Open the cash drawer with a surplus of 10 dollar and check the financial booking |
    ...     | Expected result: | The cash drawer can be openened with a surplus and the correct bookings are shown in the ADMIN |
    ...     Steps:
    ...     | Setup: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Close the open cash drawer with a total of 100 dollars |
    ...     | 3 | Close the open safe with a total of 100 dollars |
    ...     | 4 | Close the financial period |
    ...     | 5 | Close the browser |
    ...     | Test: |  |
    ...     | 6 | Log into the POS |
    ...     | 7 | Open a new financial period with the popup |
    ...     | 8 | Open the cash drawer with a surplus of 10 dollars |
    ...     | 9 | Check the financial period number |
    ...     | 10 | Close the cash drawer with the same amount |
    ...     | 11 | Close the financial period |
    ...     | 12 | Close the browser |
    ...     | 13 | Open the browser and log into the ADMIN |
    ...     | 14 | Select the correct store |
    ...     | 15 | Go to Cookbook>Events |
    ...     | 16 | Process the financial events of the financial period |
    ...     | 17 | Go to Finance>Overview and select the financial period |
    ...     | 18 | Verify the bookings |
    ...     | 19 | Close the browser |
    ...     | Teardown: |  |
    ...     | 20 | Open the browser |
    ...     | 21 | Open a new financial period with the popup |
    ...     | 22 | Open the cash drawer with 100 dollars to create the start situation |
    ...     | 23 | Open the safe with 100 dollars to create the start situation |
    ...     | 24 | Close the browser |
    [Tags]  Opening_Closing
                            S_Login.Login                                       POS  ${Organization}  Station=${False}
                            POS_S_More_Options.Open Financial Period Via Popup
                            POS_S_More_Options.Open Cash Drawer Via GUI         Amount=11   Correction=${True}  Correction_Reason=TRB-0001_Surplus
    ${Financial_Period}=    POS_S_More_Options.Get Financial Period Nr
                            POS_S_More_Options.Close Cash Drawer                Amount=11  Correction=${False}
                            POS_S_More_Options.Close Financial Period
                            SeleniumLibrary.Close Browser

                            S_Login.Login                                       ADMIN  ${Organization}
                            ADMIN_S_MainKeywords.Select Store                   ${Organization}
                            ADMIN_S_MainKeywords.Select Cookbook Events
                            ADMIN_S_Cookbook_Events.Process Financial Event     Financial_Period=${Financial_Period}
                            ADMIN_S_MainKeywords.Select Finance Overview
                            ADMIN_S_Finance_Overview.Open Bookkeeping          ${Financial_Period}
                            ADMIN_A_Finance_Overview.Verify Bookkeeping         Station=Station  Moment=Opening  Amount=10.00
                            ADMIN_S_MainKeywords.Select Finance General Ledgers
                            ADMIN_S_Finance_General_Ledgers.Search Ledgers          ${Financial_Period}
                            ADMIN_A_Finance_General_Ledgers.Verify General Ledgers  Type=Opening  Destination=Station  Amount=10.00
                            [Teardown]  SeleniumLibrary.Close Browser



TRB-0001.2 Count Money In Safe 10 Dollar Shortage
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 16-11-2020 |
    ...     | Goal: | Open the safe with a shortage of 10 dollar and check the financial booking |
    ...     | Expected result: | The safe can be openened with a shortage and the correct bookings are shown in the ADMIN |
    ...     Steps:
    ...     | Setup: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Close the open cash drawer with a total of 100 dollars |
    ...     | 3 | Close the open safe with a total of 100 dollars |
    ...     | 4 | Close the financial period |
    ...     | 5 | Close the browser |
    ...     | Test: |  |
    ...     | 6 | Log into the POS |
    ...     | 7 | Open a new financial period with the popup |
    ...     | 8 | Open the safe with a shortage of 10 dollars |
    ...     | 9 | Check the financial period number |
    ...     | 10 | Close the safe with the same amount |
    ...     | 11 | Close the financial period |
    ...     | 12 | Close the browser |
    ...     | 13 | Open the browser and log into the ADMIN |
    ...     | 14 | Select the correct store |
    ...     | 15 | Go to Cookbook>Events |
    ...     | 16 | Process the financial events of the financial period |
    ...     | 17 | Go to Finance>Overview and select the financial period |
    ...     | 18 | Verify the bookings |
    ...     | 19 | Close the browser |
    ...     | Teardown: |  |
    ...     | 20 | Open the browser |
    ...     | 21 | Open a new financial period with the popup |
    ...     | 22 | Open the cash drawer with 100 dollars to create the start situation |
    ...     | 23 | Open the safe with 100 dollars to create the start situation |
    ...     | 24 | Close the browser |
    [Tags]  Opening_Closing
                            S_Login.Login                                       POS  ${Organization}  Station=${False}
                            POS_S_More_Options.Open Financial Period Via Popup
                            POS_S_More_Options.Open Safe Via GUI                Amount=9    Correction=${True}  Correction_Reason=TRB-0001_Surplus
    ${Financial_Period}=    POS_S_More_Options.Get Financial Period Nr
                            POS_S_More_Options.Close Safe                       Amount=9    Correction=${False}
                            POS_S_More_Options.Close Financial Period
                            SeleniumLibrary.Close Browser

                            S_Login.Login                                       ADMIN  ${Organization}
                            ADMIN_S_MainKeywords.Select Store                   ${Organization}
                            ADMIN_S_MainKeywords.Select Cookbook Events
                            ADMIN_S_Cookbook_Events.Process Financial Event     Financial_Period=${Financial_Period}
                            ADMIN_S_MainKeywords.Select Finance Overview
                            ADMIN_S_Finance_Overview.Open Bookkeeping          ${Financial_Period}
                            ADMIN_A_Finance_Overview.Verify Bookkeeping         Station=Safe  Moment=Opening  Amount=10.00
                            ADMIN_S_MainKeywords.Select Finance General Ledgers
                            ADMIN_S_Finance_General_Ledgers.Search Ledgers          ${Financial_Period}
                            ADMIN_A_Finance_General_Ledgers.Verify General Ledgers  Type=Opening  Destination=Safe  Amount=10.00
                            [Teardown]  SeleniumLibrary.Close Browser
