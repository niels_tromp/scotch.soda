*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_Basket.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_e_Basket.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Search.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Barcode.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Cookbook_Events.robot

*** Variables ***
${testdir}=             ${CURDIR}
${Expected_Booking}=    ${testdir}${/}input${/}expected booking_1_{}.csv

*** Test Cases ***
TRB-0102.1 Special Tax (CASH) SCOTCH good
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 18-12-2020 |
    ...     | Goal: | Buy two products where one is under 110 dollar and one above, in order to trigger the special tax rule |
    ...     | Expected result: | Both taxes for the products are shown correctly and are visisble in the bookings |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product above 110 dollar |
    ...     | 3 | Search for a Scotch product underneath 110 dollar |
    ...     | 4 | Checkout the item with a cash payment |
    ...     | 5 | Close the browser and log into the ADMIN |
    ...     | 6 | Go to Cookbook>Events |
    ...     | 7 | Process the financial events |
    ...     | 8 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 9 | Close the browser |
    [Tags]  Dayrun  Cash  Scotch-good
    [Setup]  ADMIN_S_MainKeywords.Booking Selector  ${Organization}  ${Expected_Booking}
                    S_Login.Login                                   POS  029
                    POS_S_Search.Search Product On Product Code     ${Dev_Scotch_Product_Code_1}  ${Dev_Scotch_Size_1}
                    POS_S_Search.Search Product On Product Code     ${Dev_Licenced_Product_Code_3}  ${Dev_Licenced_Size_1}
                    POS_E_Basket.Select checkout
                    POS_A_Basket.Check Correct Tax                  Tax=0.00%  Row=1
                    POS_A_Basket.Check Correct Tax                  Tax=8.00%  Row=2
                    POS_S_Checkout.Add Order Remark                 Remark=TRB-0102.1
                    POS_E_Checkout.Select Cash
                    POS_E_Checkout.Pay with Cash
    ${Order_Nr}     POS_A_Basket.Assert Order Number
                    SeleniumLibrary.Close Browser

                        S_Login.Login                                       ADMIN  029
                        ADMIN_A_Home.Verify Order_Nr                        ${Order_Nr}
                        ADMIN_S_MainKeywords.Select Cookbook Events
                        ADMIN_S_Cookbook_Events.Process Financial Event     Order_Nr=${Order_Nr}
                        ADMIN_S_Cookbook_Events.Open Order                  ${Order_Nr}     OrderID
                        ADMIN_S_Cookbook_Events.Calculate Balance
    ${GUI_Booking_df}   ADMIN_S_Cookbook_Events.Create Booking Overview     ${Order_Nr}
                        ADMIN_A_Cookbook_Events.Verify Booking Overview     ${Expected_Booking}  ${GUI_Booking_df}
    [Teardown]          SeleniumLibrary.Close Browser

