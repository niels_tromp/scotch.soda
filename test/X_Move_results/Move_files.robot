*** Settings ***
Library     String
Library     OperatingSystem
Library     ArchiveLibrary
Library     ${sourcedir}${/}SUT${/}SO_Datetime.py
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}E_Login.robot

*** Variables ***
${testdir}=             ${CURDIR}

*** Test Cases ***
Move files and add build number
    [Tags]  Report
    ${Date}=        SO_Datetime.Get Current Datetime
    ${Date}=        SO_Datetime.Format Datetime                   ${Date}  %Y-%m-%d-(%H;%M)

                    E_Login.Open Browser                    ${POS_URL}
    ${Build_Nr}=    E_Login.Fetch Build Number
    ${Build_Nr}=    String.Fetch From Right                 string=${Build_Nr}    marker=/
    ${New_Name}=    set variable                            ${Date}${space}\[${Build_Nr}]
#    ${New_Name}=    Replace String                          ${New_Name}  /  _
                    Create Zip From Files In Directory      ${Source}  ${Destination}${New_Name}.zip   sub_directories=${true}
    [Teardown]      SeleniumLibrary.Close Browser



