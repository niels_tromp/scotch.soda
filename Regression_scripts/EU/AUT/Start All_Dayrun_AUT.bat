robot  --loglevel DEBUG ^
-d %cd%/results/Dayrun ^
-v ENV:staging_eu_aut ^
-v Regression:True ^
-v Folder:%cd%/results/Dayrun ^
-v Booking_Output_Folder:%cd%/results/Dayrun/Bookings ^
-v sourcedir:%cd%/../../../Source/EU ^
--log Dayrun_AUT.html ^
--include Dayrun ^
--exclude Coupon ^
--Exclude EGC ^
--Exclude Endless_Aisle ^
--Exclude Big_Bag ^
%cd%/../../../test/EU