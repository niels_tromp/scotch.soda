robot  --loglevel DEBUG ^
-d %cd%/results/Opening_Closing ^
-v ENV:staging_eu_deu ^
-v Regression:True ^
-v Folder:%cd%/results/Opening_Closing ^
-v sourcedir:%cd%/../../../Source/EU ^
--log Opening_Closing_DEU.html ^
--include Opening_Closing  ^
--exclude Defect ^
%cd%/../../../test/EU