:: Dayrun testen nogmaals runnen welk zijn gefaald
robot --loglevel DEBUG -v ENV:staging_eu_aut -d %cd%/results/rerun ^
-v Regression:True -v Folder:%cd%/results/rerun ^
-v Booking_Output_Folder:%cd%/results/rerun/Bookings ^
-v sourcedir:%cd%/../../../Source/EU ^
--log dayrun.html ^
--rerunfailed %cd%/results/Dayrun/output.xml ^
--output %cd%/results/rerun/rerun_Dayrun.xml ^
--Exclude AUT ^
%cd%/../../../test/EU

:: Opening_Closing testen nogmaals runnen welk zijn gefaald
robot --loglevel DEBUG -v ENV:staging_eu_deu -d %cd%/results/rerun ^
-v Regression:True -v Folder:%cd%/results/rerun ^
-v sourcedir:%cd%/../../../Source/EU ^
--log Opening_Closing.html ^
--rerunfailed %cd%/results/Opening_Closing/output.xml ^
--output %cd%/results/rerun/rerun_Opening_Closing.xml  ^
%cd%/../../../test/EU


:: Samenvoegen van de oude- en nieuwe Opening_Closing resultaten en plaatsen in results/rerun/
rebot --name rerun_Opening_Closing_combined ^
--outputdir %cd%/results/rerun/Opening_Closing ^
--merge %cd%/results/Opening_Closing/output.xml ^
%cd%/results/rerun/rerun_Opening_Closing.xml 

:: Samenvoegen van de oude- en nieuwe dayrun resultaten en plaatsen in results/rerun/
rebot --name rerun_Dayrun_combined ^
--outputdir %cd%/results/rerun/Dayrun ^
--merge %cd%/results/Dayrun/output.xml ^
%cd%/results/rerun/rerun_Dayrun.xml 

:: Samenvoegen van de nieuwe Opening&Closing en Dayrun reslutaten
rebot --name rerun_combined ^
--outputdir %cd%/results/rerun/combined ^
--merge %cd%/results/rerun/Dayrun.xml ^
%cd%/results/rerun/Opening_Closing.xml ^