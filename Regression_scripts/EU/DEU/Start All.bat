Call %cd%/"Start All_Dayrun_Deu.bat"  
Call %cd%/"Start All_Opening&Closing_DEU.bat"  
set currdate=%date%
Rebot --name Combined_results ^
--outputdir %cd%/results/combined ^
--metadata  POS_Version:"scotch-eu-feature/ionic-4-upgrade 2.31.0 (5348)" ^
--metadata  ADMIN_Version:"1.4.72" ^
--metadata  Run_Date:"%currdate%" ^
-d %cd%/results/All_Tests ^
%cd%/results/Opening_Closing/output.xml ^
%cd%/results/Dayrun/output.xml
Call %cd%/"Move results.bat"