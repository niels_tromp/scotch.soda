robot  --loglevel DEBUG  ^
-d %cd%/results/Dayrun ^
-v ENV:us_staging ^
-v Regression:True ^
-v Folder:%cd%/results/Dayrun ^
-v Booking_Output_Folder:%cd%/results/Dayrun/Bookings ^
-v sourcedir:%cd%/../../Source/US ^
--log Dayrun_US.html ^
--include Dayrun ^
%cd%/../../test/US