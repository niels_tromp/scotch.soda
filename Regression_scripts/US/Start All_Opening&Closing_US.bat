robot  --loglevel DEBUG ^
-d %cd%/results/Opening_Closing ^
-v ENV:us_staging ^
-v Regression:True ^
-v Folder:%cd%/results/Opening_Closing ^
-v Booking_Output_Folder:%cd%/results/Opening_Closing/Bookings ^
-v sourcedir:%cd%/../../Source/US ^
--log Opening_Closing_US.html ^
--include Opening_Closing ^
%cd%/../../test/US