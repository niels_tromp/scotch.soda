Call %cd%/"Start All_Dayrun_US.bat"  
Call %cd%/"Start All_Opening&Closing_US.bat"  
set currdate=%date%
Rebot --name Combined_results ^
--outputdir %cd%/results/combined ^
--metadata  POS_Version:"scotch-beta-release/2.30 2.30.0-rc.6 (5277)" ^
--metadata  ADMIN_Version:"1.4.72" ^
--metadata  Run_Date:"%currdate%" ^
-d %cd%/results/All_Tests ^
%cd%/results/Opening_Closing/output.xml ^
%cd%/results/Dayrun/output.xml
Call %cd%/"Move results.bat"