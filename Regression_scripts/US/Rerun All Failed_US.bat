:: Dayrun testen nogmaals runnen welk zijn gefaald
robot --loglevel DEBUG -v ENV:us_staging -d %cd%/results/rerun ^
-v Regression:True -v Folder:%cd%/results/rerun ^
-v Booking_Output_Folder:%cd%/results/rerun/Bookings ^
-v sourcedir:%cd%/../../Source/US ^
--log dayrun.html ^
--rerunfailed %cd%/results/Dayrun/output.xml ^
--output %cd%/results/rerun/rerun_Dayrun.xml  ^
%cd%/../../test/US

:: Opening_Closing testen nogmaals runnen welk zijn gefaald
robot --loglevel DEBUG -v ENV:us_staging -d %cd%/results/rerun ^
--rerunfailed %cd%/results/Opening_Closing/output.xml ^
-v Regression:True -v Folder:%cd%/results/rerun ^
-v sourcedir:%cd%/../../Source/US ^
--log Opening_Closing.html ^
--output %cd%/results/rerun/rerun_Opening_Closing.xml ^
%cd%/../../test/US
 
:: Samenvoegen van de oude- en nieuwe dayrun resultaten en plaatsen in results/rerun/
rebot --name rerun_Dayrun_combined ^
--outputdir %cd%/results/rerun/Dayrun_Combined ^
--merge %cd%/results/Dayrun/output.xml ^
%cd%/results/rerun/rerun_Dayrun.xml

:: Samenvoegen van de oude- en nieuwe Opening_Closing resultaten en plaatsen in results/rerun/
rebot --name rerun_Opening_Closing_combined ^
--outputdir %cd%/results/rerun/Opening_Closing ^
--merge %cd%/results/Opening_Closing/output.xml ^
%cd%/results/rerun/rerun_Opening_Closing.xml 

:: Samenvoegen van de nieuwe Opening&Closing en Dayrun reslutaten
rebot --name rerun_combined ^
--outputdir %cd%/results/rerun/combined ^
--merge --merge %cd%/results/rerun/Dayrun.xml ^
%cd%/results/rerun/Opening_Closing.xml