from datetime import datetime


def Get_Current_Datetime():
    """
    This function retrieves the current datetime and formats it to the used date in the GUI. (m)m/(d)d/yyyy, (h)h.
    When the month,date or hour contains only one digit, there will be no zero-padding for it's field. Therefore the zero-padding
    should be removed.
    For the month it will check the first character of the string for a zero. If this is the case it will be removed.
            *Parameters*
            | =Returnvalue= | =Description= |
            | ``formatted_Date`` | A string containing the current datetime in the following format: (m)m/(d)d/yyyy, (h)h |
            """
    # Get the current datetime
    now = datetime.now()
    return now


def Format_datetime(date, dt_format):
    formatted_date = date.strftime(dt_format)
    return formatted_date


def Format_datetime_For_MoveCash(now):
    # Format the current datetime to exclude the zero-padding for the day and hour
    formatted_date = now.strftime('%d.%m.%Y, %H').lstrip("0").replace(".0", ".").replace(". 0", ", ")
    # Check if the current month starts with a zero(first character of the string) and replace the zero on that
    # index with EMPTY. By using slicing, not all zeroes will be removed.
    if formatted_date[0] == '0':
        formatted_date = formatted_date[:0] + '' + formatted_date[0 + 1:]
    return formatted_date

