import pandas as pd
import numpy as np
import os
import re
import logging
import math
from pathlib import Path
from datetime import datetime


def Create_Regression_List_CSV(Arg1, Arg2):
    New_csv = pd.DataFrame(
        {'Testcase': [Arg1],
         'Order_Nr': [Arg2]
         })
    return New_csv


def Create_DF_From_Multiple_Lists(account_list, price_list, order_nr, date, organization):
    bookings_list = pd.DataFrame(
        {'DateTime': date,
         'Account': account_list,
         'Amount': price_list,
         'Organization': organization,
         'Order': order_nr
         })
    bookings_list['Amount'] = bookings_list['Amount'].replace('.', ',')
    return bookings_list


def Write_To_Csv(df, folder, filename, sep=';', mode='w', header=False):
    """
    This function writes the Data Frame data to a csv where the location will be taken from folder and filename.

    *Parameters*
    | =Parameter= | =Description= | =Mandatory or Optional= |
    | ``df`` | A Data Frame | Mandatory |
    | ``folder`` | The folder where the CSV has to be created. If not present, it will be created | Mandatory |
    | ``filename`` | The filename of the CSV | Mandatory |
    | ``sep`` | The separator of the columns in the CSV | Optional (Default=';') |
    | ``header`` | A indicator if the CSV needs a header | Optional (Default=False) |
    | =Returnvalue= | =Description= |
    | ``Yes`` | The filelocation |
    """
    folder = Path(folder)
    if not os.path.exists(folder):
        os.makedirs(folder)
    df.to_csv(folder.joinpath(filename), sep=sep, index=False, header=header, mode=mode)
    return folder.joinpath(filename)


def Create_DF_From_csv(csv_file, sep=';'):
    """
    This function reads a CSV file and return it into a Data Frame, if applicable.

    *Parameters*
    | =Parameter= | =Description= | =Mandatory or Optional= |
    | ``csv`` | A CSV file | Mandatory |
    | ``sep`` | The separator to use when reading the CSV | Mandatory |
    | =Returnvalue= | =Description= |
    | ``Yes`` | A Data Frame |
    """
    df = pd.read_csv(csv_file, sep=sep, dtype=str)
    return df


def Combine_DFs(df1, df2):
    """
    This function combines two dataframes into one on the x-axis, so adding the columns. For this the dataframes need unique column names.

    *Parameters*
    | =Parameter= | =Description= | =Mandatory or Optional= |
    | ``df1`` | A dataframe | Mandatory |
    | ``df2`` | A second dataframe | Mandatory |
    | =Returnvalue= | =Description= |
    | ``df_comb`` | The combined Data Frame |
    """
    df2['Amount'] = df2['Amount'].str.replace('.', ',')
    df1['Verify_Amount'] = df1['Verify_Amount'].str.replace('.', ',')
    df_comb = pd.concat([df1, df2], axis=1)
    # df_comb.to_csv('combined_df.csv', sep=';', index=False)
    return df_comb


def Verify_Account_And_Amount(df_comb):
    """
    This function verifies in the combined dataframe if there is a mismatch in the columns for the account names and the
    booking values. It will create two new columns where it will fill True or False based on a mismatch, where a mismatch
    is filled with an value of False. From these new columns it will look for the False and append the row numbers to a list.
    These lists will be the output of the function and will help by pinpointing in which row the mismatch occurs.

    *Parameters*
    | =Parameter= | =Description= | =Mandatory or Optional= |
    | ``df_comb`` | The combination of two dataframes | Mandatory |
    | =Returnvalue= | =Description= |
    | ``wrong_accounts`` | A list that contains the row numbers where a mismatch takes place for the account names.
                           When there are no mismatches the list will be empty |
    | ``wrong_amounts`` | A list that contains the row numbers where a mismatch takes place for the account values.
                           When there are no mismatches the list will be empty |
    """
    df_comb['is_Account correct'] = np.where(df_comb['Verify_Account'] == df_comb['Account'], True, False)
    df_comb['is_Amount correct'] = np.where(df_comb['Verify_Amount'] == df_comb['Amount'], True, False)
    wrong_accounts = df_comb.index[df_comb['is_Account correct'] == False].tolist()
    wrong_amounts = df_comb.index[df_comb['is_Amount correct'] == False].tolist()
    return wrong_accounts, wrong_amounts


def Fetch_Specific_Cel(df, mismatch_list):
    """
    This function locates the account name of the row which contains a mismatch. It does this by reading into the dataframe
    into the column 'Account' and the row(s) that were found and appended to the 'mismatch_list'. It will append the account
    names to the list 'faulty_line' and returns that list.
        *Parameters*
        | =Parameter= | =Description= | =Mandatory or Optional= |
        | ``df`` | The combination of two dataframes which contain some mismatches | Mandatory |
        | ``mismatch_list`` | A list containing the rownumber(s) of mismatch(es) | Mandatory |
        | =Returnvalue= | =Description= |
        | ``faulty_line`` | A list containing the account names, containing a mismatch |
        """
    try:
        faulty_line = df.loc[mismatch_list, 'Verify_Account']
        faulty_line_list = faulty_line.values.tolist()
        return faulty_line_list
    except KeyError:
        raise KeyError("Not all expected rows could be found. Please check the output file for the present accounts")


