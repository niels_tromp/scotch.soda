*** Settings ***
Library  String
*** Keywords ***
Get Order Number
    [Documentation]
    ...  This keyword will fetch the (purchase)order number after a purchase order has been placed and confirmed.
    ...  This number will be used to select the correct order from the list in the GUI and create a shipment for it.
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Order_Nr` | String | The purchase order number | 2344 |
    ${Order_Nr}=     SeleniumLibrary.get text   //order-edit-component//div[contains(@ng-if,'OrderId')]
    ${Order_Nr}=     String.Fetch From Right    string=${Order_Nr}    marker=:${SPACE}
    [Return]  ${Order_Nr}

