*** Settings ***
Library     ${sourcedir}${/}SUT${/}SO_DF.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot

*** Variables ***
${Folder}=      ${testdir}${/}results${/}evidence${/}

*** Keywords ***
Verify General Ledgers
    [Documentation]
    ...  This keyword checks if there are lines present containing the different bookings we expect. By giving in the Station and Moment
    ...  it will decide which amount should be shown where and if it should be positive or negative. The keyword and
    ...  calculation only work with one modification being done. When any type of sale is being done inbetween the opening and
    ...  closing the station, this keyword will not perform as designed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Type` | The type of financial transaction that has been done | Move Cash | Mandatory |
    ...                 | `Destination` | The destination of the money. This will determine the positive and negative amount per account | Safe/Station | Mandatory |
    ...                 | `Amount` | The amount of the difference that was created in total amount of euro's | 10.00 | Mandatory |
    [Arguments]  ${Type}  ${Destination}  ${Amount}
                SeleniumLibrary.Wait until element Is Visible   //tbody/tr/td[7]  5
    ${status}=  Run Keyword And Return Status                   variable should exist   ${Regression}
                run keyword if  ${status}                       ADMIN_S_MainKeywords.Take Screenshot        General_Ledger  ${Folder}${/}Screenshots${/}${TEST NAME}

    ### OPENING DEVIATION ###
    run keyword if  '${Type}'=='Opening' and '${Destination}'=='Station'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Cash difference')]
    ...                                                         message=Line with 'Cash difference' is not present
    ### CLOSING DEVIATION ###
    run keyword if  '${Type}'=='Closing' and '${Destination}'=='Closing'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Cash difference')]
    ...                                                         message=Line with 'Cash difference' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present
    ### MOVE CASH ###
    run keyword if  '${Type}'=='Move Cash' and '${Destination}'=='Safe'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Suspense cash-safe')]
    ...                                                         message=Line with 'Suspense account' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present
    run keyword if  '${Type}'=='Move Cash' and '${Destination}'=='Station'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Suspense cash-safe')]
    ...                                                         message=Line with 'Suspense account' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present
    ### EXPENSE ###
    run keyword if  '${Type}'=='Expense' and '${Destination}'=='Expense'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Other Expense Type')]
    ...                                                         message=Line with 'Other Expense Type' is not present
    ### INCOME ###
    run keyword if  '${Type}'=='Income' and '${Destination}'=='Station'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€-${Amount}']/../../../td[contains(text(),'Other Expense Type')]
    ...                                                         message=Line with 'Other Expense Type' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //tbody/tr/td[2]//span[text()='€${Amount}']/../../../td[contains(text(),'Cash in hand')]
    ...                                                         message=Line with 'Cash in hand' is not present


