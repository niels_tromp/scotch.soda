*** Variables ***
${xpath_GUI_Order_Nr_Column}=    //tbody//td[position() = count(//thead//th[contains(text(),'Order number')]/preceding::th)+1]

*** Keywords ***
Verify Order_Nr
    [Documentation]
    ...  This keyword verifies if the order number form the done payment is registered and visible in the ADMIN application
    ...  homescreen. It will fetch the first order number of the table and verifies that number with the given order number.
    ...  Before looking up the number, it will wait until the table is loaded with all orders.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | String of multiple digits of the order number | 2355 | Mandatory |
    [Arguments]  ${Order_Nr}
                                SeleniumLibrary.Wait until element is visible   //table/tbody/tr[15]  10
    ${xpath_Status}=            Get element count               ${xpath_GUI_Order_Nr_Column}
    FOR  ${RC}  IN RANGE   1   ${xpath_Status}
        ${GUI_Order_Nr}     SeleniumLibrary.Get Text        //tbody//tr[${RC}]/td[1]
        ${Present}          run keyword and return status   should be equal  ${GUI_Order_Nr}   ${Order_Nr}
                            run keyword if  ${Present}      exit for loop
    ...                     ELSE IF  ${RC+1} == ${xpath_Status}  FAIL
    ...                     msg=Order number '${Order_Nr}' could not be found in the table
    END