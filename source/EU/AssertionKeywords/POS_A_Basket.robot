*** Settings ***
Library     String
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot

*** Keywords ***
Assert Order Number
    [Documentation]
    ...  This keyword fetches the order number from the POS application after a payment is done.
    ...  It will return the found order number after the '#'
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Order_Nr` | String | The order number | 2344 |
                            SeleniumLibrary.Wait until element is visible   //ion-text/p[contains(text(),'Order')]  10
    ${Screen_Order_Nr}=     SeleniumLibrary.get text                        //ion-text/p[contains(text(),'Order')]
    ${Order_Nr}=            String.Fetch From Right                         string=${Screen_Order_Nr}    marker=#
    ${status}=              Run Keyword And Return Status                   variable should exist   ${Regression}
                            run keyword if  ${status}                       POS_E_Basket.Write Order_Nr to DF  ${Order_Nr}
    [Return]  ${Order_Nr}

