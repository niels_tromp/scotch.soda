*** Settings ***
Library  Collections
Library  String
Library  ${sourcedir}${/}SUT${/}SO_DF.py
Resource   ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot

*** Variables ***
${Value_column}=            //order-bookings//table//tbody/tr/td[position()=count(//thead//th[text()='Value']/preceding-sibling::th)+1]
${Account_column}=          //order-bookings//table//tbody/tr/td[position()=count(//thead//th[text()='Account']/preceding-sibling::th)+1]
${Folder}=                  ${testdir}${/}results${/}evidence${/}
${Booking_Output_Folder}=   ${testdir}${/}results${/}evidence${/}
*** Keywords ***
Select 100 rows
    [Documentation]
    ...  This keyword selects the '100' button to show max 100 rows in the table
    SeleniumLibrary.Wait until element Is Visible   //button/span[text()='100']/parent::button
    SeleniumLibrary.Click button    //button/span[text()='100']/parent::button


Enter Search Cirteria
    [Documentation]
    ...  This keyword enters the order number or Fianacial period nr in the screen Cookbook>Events. It will first wait until the table is visible,
    ...  checks how many rows are present before and after the order number is filled in to be sure the page updated
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Number` | The order number or financial period number | 2344 | Mandatory |
    ...                 | `Column` | The  input field where the number should be added | ProcessID/FinancialPeriodID | Mandatory |
    [Arguments]  ${Number}  ${Column}
                SeleniumLibrary.Wait until element Is Visible   //cookbook-financial-events-overview//tbody//tr  5
                sleep  0.5s
    ${Before}   Get Element Count               //cookbook-financial-events-overview//tbody//tr
                SeleniumLibrary.Input Text      //cookbook-financial-events-overview//input[@name='${Column}']  ${Number}
                sleep  0.5
    ${After}    Get Element Count               //cookbook-financial-events-overview//tbody//tr
                return from keyword if          ${Before} < ${After}


Check rows difference
    [Documentation]
    ...  This keyword does a check if the rows before are greater or equeal to the rows after the order number is entered.
    ...  It will throw an error when it is not the case and the page probably didn't update
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Rows_before` | The amount of rows in the table before the order nr is entered | 45 | Mandatory |
    ...                 | `Rows_after` | The amount of rows in the table after the order nr is entered | 33 | Mandatory |
    [Arguments]  ${Rows_before}  ${Rows_after}
    run keyword if  ${Rows_before} == ${Rows_after} or ${Rows_before} > ${Rows_after}
    ...             return from keyword  fail  msg=The amount of rows of the table didn't change


Process Events
    [Documentation]
    ...  This keyword clicks on the button to process the financial events and checks if the popup is shown
    SeleniumLibrary.Click Element       //cookbook-events-overview//ul//i[contains(@class,'gear')]/ancestor::li
    SeleniumLibrary.Wait until element Is Visible       //div[text()='Financial events have been processed']  5
    SeleniumLibrary.Wait until element is not visible   //div[text()='Financial events have been processed']  10
    ${status}=  Run Keyword And Return Status       variable should exist                   ${Regression}
                run keyword if  ${status}           ADMIN_S_MainKeywords.Take Screenshot    Cookbook_Events  ${Folder}${/}Screenshots${/}${TEST NAME}


Select Order
    [Documentation]
    ...  This keyword selects the order number from the table to fetch the specific data for the order
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | The expected status to be found in the table | Processed | Mandatory |
    [Arguments]  ${Order_Nr}
    SeleniumLibrary.Wait until element Is Visible   //cookbook-financial-events-overview//tbody//tr[1]//a[contains(text(),'${Order_Nr}')]  10
    SeleniumLibrary.Click Element                   //cookbook-financial-events-overview//tbody//tr[1]//a[contains(text(),'${Order_Nr}')]
    SeleniumLibrary.Wait until element Is Visible   //eva-barcode
    SeleniumLibrary.Wait until element Is Visible   //section//li[7]
    SeleniumLibrary.Scroll Element Into View        //section//li[7]
    SeleniumLibrary.Wait until element Is Visible   //table/tbody//td[3]
    ${status}=  Run Keyword And Return Status       variable should exist                   ${Regression}
                run keyword if  ${status}           ADMIN_S_MainKeywords.Take Screenshot    Order_details  ${Folder}${/}Screenshots${/}${TEST NAME}


Select Tab Documents
    [Documentation]
    ...  This keyword selects the tab 'Invoices' after the specific order page is loaded.
    SeleniumLibrary.Click Element           //ul[contains(@class,'nav-tabs')]//a[text()='Documents']
    SeleniumLibrary.Wait until element Is Visible           //order-blobs
    Sleep  0.5s  # To be sure the full table has loaded
    ADMIN_S_MainKeywords.Take Screenshot    Documents


Select Tab Invoices
    [Documentation]
    ...  This keyword selects the tab 'Bookings' after the specific order page is loaded.
    SeleniumLibrary.Click Element           //ul[contains(@class,'nav-tabs')]//a[text()='Invoices']
    SeleniumLibrary.Wait until element Is Visible           //order-invoice-list
    Sleep  0.5s  # To be sure the full table has loaded
    ADMIN_S_MainKeywords.Take Screenshot    Invoices


Select Tab Bookings
    [Documentation]
    ...  This keyword selects the tab 'Bookings' after the specific order page is loaded.
    SeleniumLibrary.Click Element                       //ul[contains(@class,'nav-tabs')]//a[text()='Bookings']
    SeleniumLibrary.Wait until element Is Visible       //order-bookings//table
    Sleep  0.5s  # To be sure the full table has loaded
    SeleniumLibrary.Page Should Not Contain Element     //td[text()='No bookings available']
    ...                                                 message=Financial events were not processed correctly
    SeleniumLibrary.Wait until element Is Visible       //order-bookings//table/tbody//td[3]  5
    ...                                                 error=Table is empty


Calculate Credit
    [Documentation]
    ...  This keyword calculates the total credit of the selected order. It will sum up all the positive amounts from the
    ...  table and retuns that value.
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Total_Credit` | int | the total amount of debit | 623 |
    ${Credit_Value}  create list
    ${Total_Credit}  set variable  ${EMPTY}
    ${Credit_count}  SeleniumLibrary.Get Element Count  ${Value_column}//span[not(contains(text(),'-'))]
    FOR  ${row}    IN RANGE  1   ${Credit_count}
                            SeleniumLibrary.Page Should Contain Element  (${Value_column}//span[contains(text(),'-')])[${row}]
    ...                     message=No credit bookings could be found
         ${Value}           SeleniumLibrary.Get text    (${Value_column}//span[not(contains(text(),'-'))])[${row}]
         ${Value}           String.Fetch From Right     string=${Value}    marker=€
                            Append To List              ${Credit_Value}  ${Value}
         ${Total_Credit}    evaluate                    ${Total_Credit}+${Value}
    END
    [Return]  ${Total_Credit}


Calculate Debit
    [Documentation]
    ...  This keyword calculates the total debit of the selected order. It will sum up all the negative amounts from the
    ...  table and retuns that value as an absolute value.
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Total_Debit` | int | the total amount of credit | 623 |
    ${Debit_Value}  create list
    ${Total_Debit}  set variable  ${EMPTY}
    ${Debit_count}  SeleniumLibrary.Get Element Count  ${Value_column}//span[contains(text(),'-')]
    FOR  ${row}    IN RANGE  1   ${Debit_count}
                        SeleniumLibrary.Page Should Contain Element  (${Value_column}//span[contains(text(),'-')])[${row}]
        ...             message=No debit bookings could be found
        ${Value}        SeleniumLibrary.Get text    (${Value_column}//span[contains(text(),'-')])[${row}]
        ${Value}        String.Fetch From Right     string=${Value}    marker=-
                        Append To List              ${Debit_Value}  ${Value}
        ${Total_Debit}  evaluate                    ${Total_Debit} + ${Value}
    END
    [Return]  ${Total_Debit}


Calculate Total Balance
    [Documentation]
    ...  This keyword does a simple calculation if the total amount of debit - credit equals zero
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Debit` | The total amount of debit as an absolute value | 233 | Mandatory |
    ...                 | `Credit` | The total amount of credit as an absolute value | 233 | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Balance` | int | the balance between the debit and credit | 0 |
    [Arguments]  ${Debit}  ${Credit}
    ${Balance}  evaluate  ${Credit}-${Debit}
    [Return]  ${Balance}


Check Zero Balance
    [Documentation]
    ...  This keyword does a simple check if the result of debit - credit equals zero
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Balance` | The result of debit - credit | 0 | Mandatory |
    [Arguments]  ${Balance}
    should be equal as numbers  ${Balance}  0.0


Fetch Account Names
    [Documentation]
    ...  This keyword gets all entries for the account names from the table, sorts the list alphabetical and removes
    ...  the duplicates. The it will use this list to fetch all amounts of a specific account and add those values
    ...  together, to be used in the `Create Bookings CSV` keyword
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Account_list` | list | A list containing all account names from the GUI | [Cash in hand USD in stores (18841.USD), Garments (15311)] |
    ${Accounts}=        SeleniumLibrary.Get Element Count  ${Account_column}
    @{Account_list}=    create list
    FOR  ${row}    IN RANGE  1   ${Accounts}+1
    ${Account}  SeleniumLibrary.Get text    //order-bookings//table//tbody/tr[${row}]/td[position()=count(//thead//th[text()='Account']/preceding-sibling::th)+1]
                append to list              ${Account_list}     ${Account}
    END
                        sort list                   ${Account_list}
    @{Account_list}     remove duplicates           ${Account_list}
    [Return]  @{Account_list}


Fetch Account Data
    [Documentation]
    ...  This keyword calls the keyword `Fetch Table Data` in order to get all prices, organizations and dates per account line.
    ...  It will use the account list to check how many different lines per accounts there are. By calling `Fetch Table Data`
    ...  for each instance, it will gather and sum the amount/price of that account and return it. It will also fetch the
    ...  Date and Organization for the first occurance of the specific account and returns that.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Account_list` | A list containing all account names from the GUI | [Cash in hand USD in stores (18841.USD), Garments (15311)] | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Price_list` | list | A list containing all the values/amounts | [23, 11, 200] |
    ...                 | `Organization_list` | list | A list containing all the organization namesI | [Q023-US023 New York Columbus Avenue, Q023-US023 New York Columbus Avenue] |
    ...                 | `Date_list` | list | A list containing all dates | [October 15, 2020 1:10 PM, October 15, 2020 1:10 PM] |
    [Arguments]  @{Account_list}
    @{Price_list}=          create list
    @{Organization_list}=   create list
    @{Date_list}=           create list
    FOR  ${Accounts}  IN  @{Account_list}
    ${Account_occurance}=        SeleniumLibrary.Get Element Count
    ...  //order-bookings//table//tbody/tr/td[text()='${Accounts}']/ancestor::tr/td[position()=count(//thead//th[text()='Value']/preceding-sibling::th)+1]
    ${Value}  ${Organization}  ${Date}      Fetch Table Data    ${Account_occurance}    ${Accounts}
                                            append to list      ${Price_list}           ${Value}
                                            append to list      ${Organization_list}    ${Organization}
                                            append to list      ${Date_list}            ${Date}
    END
    [Return]  ${Price_list}  ${Organization_list}  ${Date_list}


Fetch Table Data
    [Documentation]
    ...  This keyword is called by `Fetch Account Data` and will loop over the GUI table with the given account in order
    ...  to get the total amount (debit-credit) of that account and the date + organization name.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Account_occurance` | An int with the amount of rows there are of the same account | 2 | Mandatory |
    ...                 | `Account` | The name of the account to be checked | Cash in hand USD in stores (18841.USD) | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Value_Amount` | int | A list containing all the values/amounts | [23, 11, 200] |
    ...                 | `Organization` | string | The name of the organization doing the order | Q023-US023 New York Columbus Avenue |
    ...                 | `Date` | string | The date of the first occurance of the account | October 15, 2020 1:10 PM |
    [Arguments]  ${Account_occurance}  ${Account}
    ${Value_Amount}  set variable  ${EMPTY}
    ${Organization}     SeleniumLibrary.Get text    (//order-bookings//table//tbody/tr/td[text()='${Account}']/ancestor::tr/td[position()=count(//order-bookings//th[text()='Organization']/preceding-sibling::th)+1])[1]
    ${Date}             SeleniumLibrary.Get text    (//order-bookings//table//tbody/tr/td[text()='${Account}']/ancestor::tr/td[position()=count(//order-bookings//th[text()='Date']/preceding-sibling::th)+1])[1]
    FOR  ${Occurance}  IN RANGE  1  ${Account_occurance}+1
    ${Value}            SeleniumLibrary.Get text    (//order-bookings//table//tbody/tr/td[text()='${Account}']/ancestor::tr/td[position()=count(//order-bookings//th[text()='Value']/preceding-sibling::th)+1])[${Occurance}]
    ${Value}            String.Fetch From Right     string=${Value}    marker=€
    ${Value_Amount}     evaluate                    ${Value_Amount} + ${Value}
    ${Value_Amount}     Evaluate                    "%.2f" % ${Value_Amount}
    END

    [Return]  ${Value_Amount}  ${Organization}  ${Date}


Create Bookings CSV
    [Documentation]
    ...  This keyword creates a csv file containing the data fetched from the GUI, where the amounts of the accounts
    ...  are combined. It will output the datafame in order to check if there are some mismatches with the expected result.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Filename` | The name that should be given to the created csv | StandardSalesTransCashSG.csv | Mandatory |
    ...                 | `Account_list` | A list containing the names of the accounts being used | [Cash in hand USD in stores (18841.USD), Garments (15311)] | Mandatory |
    ...                 | `Price_list` | A list containing all total amounts per account | [23, 100, 321] | Mandatory |
    ...                 | `Order_Nr` | The order number | 3455 | Mandatory |
    ...                 | `Date_list` | A list of the dates of booking per account | [October 15, 2020 1:10 PM, October 15, 2020 1:10 PM] | Mandatory |
    ...                 | `Organization_list` | A list of the organization names per account | [Q023-US023 New York Columbus Avenue, Q023-US023 New York Columbus Avenue] | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Booking_df` | dataframe | A dataframe containing all the date fetched from the GUI | X |
    [Arguments]  ${Filename}  ${Account_list}  ${Price_list}  ${Order_Nr}  ${Date_list}  ${Organization_list}
    ${Booking_df}           SO_DF.Create_DF_From_Multiple_Lists     ${Account_list}  ${Price_list}  ${Order_Nr}  ${Date_list}  ${Organization_list}
                            SO_DF.Write_To_CSV                      df=${Booking_df}  folder=${Booking_Output_Folder}  filename=${Filename}  header=True
    [Return]  ${Booking_df}


Throw Error
    [Documentation]  This keyword checks if there are mismatches in the data from the gui and the expected data.
    ...  it will first check if the 'wrong' list contains any item, if so it will fetch the rownumber and throws
    ...  an error telling for which account there is an mismatch found and should be checked.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `wrong_list` | The name that should be given to the created csv | StandardSalesTransCashSG.csv | Mandatory |
    ...                 | `Booking_Overview` | A list containing the names of the accounts being used | [Cash in hand USD in stores (18841.USD), Garments (15311)] | Mandatory |
    ...                 | `Price_list` | A list containing all total amounts per account | [23, 100, 321] | Mandatory |
    [Arguments]  ${wrong_list}  ${Booking_Overview}
    ${Empty_list}       run keyword and return status       should be empty  ${wrong_list}
    ${Faulty_account}   run keyword unless                  ${Empty_list}
    ...                 SO_DF.Fetch_Specific_Cel            ${Booking_Overview}  ${wrong_list}
                        run keyword unless                  ${Empty_list}
    ...                 fail                                msg= Mismatch in account name/number or ammount for the following account(s): ${Faulty_account}












