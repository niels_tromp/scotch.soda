*** Settings ***
Library  Collections
Library  String
Library  ${sourcedir}${/}SUT${/}SO_DF.py
Resource   ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot

*** Variables ***
${Value_column}=    //order-bookings//table//tbody/tr/td[position()=count(//thead//th[text()='Value']/preceding-sibling::th)+1]
${Account_column}=  //order-bookings//table//tbody/tr/td[position()=count(//thead//th[text()='Account']/preceding-sibling::th)+1]

${Folder}=          ${testdir}${/}results${/}evidence${/}
*** Keywords ***
Click On Card Containing ID
    [Arguments]  ${Financial_Period}
    SeleniumLibrary.Wait until element is visible   //span[text()='${Financial_Period}']/ancestor::li  5
    SeleniumLibrary.Click Element                   //span[text()='${Financial_Period}']/ancestor::li
    SeleniumLibrary.Wait until element is visible   //h3[text()='Stations']  5

Click On Bookkeeping
    SeleniumLibrary.Wait until element is visible   //ul/li/a[text()='Bookkeeping']  5
    SeleniumLibrary.Click Element                   //ul/li/a[text()='Bookkeeping']
    SeleniumLibrary.Wait until element is visible   //h3[contains(text(),'(')]  10  error= Financial Events not processed correctly


Click On Documents
    SeleniumLibrary.Wait until element is visible   //ul/li/a[text()='Documents']  5
    SeleniumLibrary.Click Element                   //ul/li/a[text()='Documents']
    SeleniumLibrary.Wait until element is visible   //tr/td//a[text()='Period Closing Mail']  10
    ...  Error=No End Of Day Report Found


Click End Of Day Report
    SeleniumLibrary.Click Element                   //tr/td//a[text()='Period Closing Mail']








