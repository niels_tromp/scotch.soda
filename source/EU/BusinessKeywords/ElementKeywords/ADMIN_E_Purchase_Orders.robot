*** Settings ***
Library  Collections
Library  String
Library  ${sourcedir}${/}SUT${/}SO_DF.py

*** Variables ***
${xpath_Supplier_field}=    //eva-select[@placeholder='Select a supplier']//span[@aria-label='Select box activate']
${xpath_Receiver_field}=    //eva-select[@placeholder='Select receiving party']//span[@aria-label='Select box activate']
${xpath_Supplier_input}=    //eva-select[@placeholder='Select a supplier']//input[@type='search']
${xpath_Receiver_input}=    //eva-select[@placeholder='Select receiving party']//input[@type='search']
${xpath_Date}=              //label[contains(text(),'Requested delivery date')]/../div/eva-inline-datepicker//button[1]
${xpath_Product}=           //tbody/tr[1]/td[position()=count(//thead//th//span[contains(text(),'Product')]/preceding::th)+1]//input
${xpath_Quantity}=          //tbody/tr[1]/td[position()=count(//thead//th//span[contains(text(),'Quantity')]/preceding::th)+1]//input
${xpath_Aded_Product}=      //tbody/tr[last()]/td[position()=count(//thead//th//span[contains(text(),'Custom ID')]/preceding::th)+1]
${xpath_Save_btn}=          //button[contains(@ng-click,'save')]
${xpath_Yes_btn}=           //button[contains(@ng-click,'yes')]
${xpath_Confirm_btn}=       //button[contains(@ng-click,'Confirm')]
${xpath_Place_Order_btn}=   //button[contains(@ng-click,'placeOrder')]
${xpath_Shipment_btn}=      //orders-component//a[contains(@href,'shipment')]
${xpath_DNN_field}=         //input[@name='shipmentId']
${xpath_Order_field}=       //label[text()='Order']/following-sibling::input
*** Keywords ***
Click New Purchase Order
    SeleniumLibrary.Wait until element is visible   //a[contains(@href,'purchase/orders/add')]  10
    SeleniumLibrary.Click Element                   //a[contains(@href,'purchase/orders/add')]
    SeleniumLibrary.Wait until element is visible   //h1/strong[text()='Create purchase order']  5


Enter Supplier
    [Arguments]  ${Supplier}
    SeleniumLibrary.Wait until element is visible   ${xpath_Supplier_field}  5
    SeleniumLibrary.Click Element                   ${xpath_Supplier_field}
    SeleniumLibrary.Input Text                      ${xpath_Supplier_input}    ${Supplier}
    SeleniumLibrary.Wait until element is visible   //ul/li//span/div[contains(text(),'${Supplier}')]  5
    SeleniumLibrary.Click Element                   //ul/li//span/div[contains(text(),'${Supplier}')]


Enter Receiver
    [Arguments]  ${Receiver}
    SeleniumLibrary.Wait until element is visible   ${xpath_Receiver_field}  5
    SeleniumLibrary.Click Element                   ${xpath_Receiver_field}
    SeleniumLibrary.Input Text                      ${xpath_Receiver_input}    ${Receiver}
    SeleniumLibrary.Wait until element is visible   //ul/li//span/div[contains(text(),'${Receiver}')]  5
    SeleniumLibrary.Click Element                   //ul/li//span/div[contains(text(),'${Receiver}')]


Enter Delivery Date
    SeleniumLibrary.Wait until element is visible       ${xpath_Date}  5
    SeleniumLibrary.Click Element                       ${xpath_Date}
    SeleniumLibrary.Wait until element is visible       (//span[@aria-current])[1]
    SeleniumLibrary.Click Element                       (//span[@aria-current])[1]
    SeleniumLibrary.Wait until element is visible       //h3[text()='Modify requested delivery dates']
    SeleniumLibrary.Click Element                       //button[contains(@ng-click,'yes')]
    SeleniumLibrary.Wait until element is not visible   //h3[text()='Modify requested delivery dates']
    SeleniumLibrary.Click Element                       //label[contains(text(),'Requested delivery date')]


Enter Product Details With CSV
    [Arguments]  ${csv}
    ${data}=    SO_DF.Create_DF_From_CSV    ${csv}
    Sleep  1s  #make sure the page has loaded
    FOR  ${csv_row}  IN  @{data.index}
    Enter Product Details  ${data.Product[${csv_row}]}  ${data.Quantity[${csv_row}]}
    END


Enter Product Details
    [Arguments]  ${Product_Code}  ${Quantity}
    SeleniumLibrary.Wait until element is visible   ${xpath_Product}  5
    SeleniumLibrary.Input Text                      ${xpath_Product}    ${Product_Code}
    SeleniumLibrary.Wait until element is visible   //eva-product-autocomplete//span  5
    SeleniumLibrary.Click Element                   //eva-product-autocomplete//span
    Sleep  2s
    SeleniumLibrary.Input Text                      ${xpath_Quantity}   ${Quantity}
    SeleniumLibrary.Click Element                   //tbody/tr[1]/td[@data-title-text='Actions']/button
    wait until keyword succeeds     2x  1s          SeleniumLibrary.Element Should Contain  ${xpath_Aded_Product}  ${Product_Code}


Click Save
    SeleniumLibrary.Wait until element is visible   ${xpath_Save_btn}  5
    SeleniumLibrary.Click Element                   ${xpath_Save_btn}


Acknowledge Place Order Popup
    SeleniumLibrary.Wait until element is visible       ${xpath_Yes_btn}  5
    SeleniumLibrary.Click Element                       ${xpath_Yes_btn}
    SeleniumLibrary.Wait until element is not visible   ${xpath_Yes_btn}  5


Click Confirm
    wait until keyword succeeds  2x  1s  run keywords
    ...         SeleniumLibrary.Wait until element is visible       ${xpath_Confirm_btn}  5
    ...  AND    SeleniumLibrary.Click Element                       ${xpath_Confirm_btn}
    ...  AND    SeleniumLibrary.Wait until element is visible       ${xpath_Place_Order_btn}  5


Add Reference
    [Arguments]  ${Reference}
    SeleniumLibrary.Wait until element is visible       //confirm-purchase-order-modal//input  5
    SeleniumLibrary.Input Text                          //confirm-purchase-order-modal//input  ${Reference}
    SeleniumLibrary.Click Element                       ${xpath_Place_Order_btn}




                                ##############
                                #            #
                                #  Shipment  #
                                #            #
                                ##############

Click New Shipment
    SeleniumLibrary.Wait until element is visible       ${xpath_Shipment_btn}   5
    SeleniumLibrary.Click Element                       ${xpath_Shipment_btn}
    SeleniumLibrary.Wait until element is visible       ${xpath_DNN_field}        5


Enter DNN
    [Arguments]  ${Delivery_Note_Number}
    SeleniumLibrary.Wait until element is visible   ${xpath_DNN_field}    5
    SeleniumLibrary.Input Text                      ${xpath_DNN_field}    ${Delivery_Note_Number}


Enter Order Nr
    [Arguments]  ${Order_Nr}
    SeleniumLibrary.Wait until element is visible       ${xpath_Order_field}  5
    SeleniumLibrary.Input Text                          ${xpath_Order_field}   ${Order_Nr}


Select Order Popup
    SeleniumLibrary.Wait until element is visible       //div[contains(@class,'orders-overscroll')]//h4  5
    SeleniumLibrary.Click Element                       //div[contains(@class,'orders-overscroll')]//h4


Select Products
    SeleniumLibrary.Wait until element is visible   //tbody/tr/td[3]  5
    Sleep  1s  #To be sure all items are loaded
    SeleniumLibrary.Click Element                   //tbody/tr/th/input
    SeleniumLibrary.Wait until element is visible   //tbody/tr/td[7]/input  5


Create Shipment
    SeleniumLibrary.Click Element                       //button[contains(@ng-click,'Shipment')]
    SeleniumLibrary.Wait until element is visible       //button[contains(@ng-click,'save')]  5


Click Save Shipment
    SeleniumLibrary.Wait until element is visible       //button[contains(@ng-click,'save')]  5
    SeleniumLibrary.Click Element                       //button[contains(@ng-click,'save')]
    SeleniumLibrary.Wait until element Is Visible       //div[text()='Order lines have been shipped']  5
    SeleniumLibrary.Wait until element is not visible   //div[text()='Order lines have been shipped']  10


Return To Overview
    wait until keyword succeeds  2x  1s  run keywords
    ...         SeleniumLibrary.Click Element                       //div[@uib-modal-window]
    ...  AND    SeleniumLibrary.Wait until element is visible       ${xpath_Shipment_btn}   5


                                ##############
                                #            #
                                #  Receive   #
                                #  Shipment  #
                                #            #
                                ##############

View Order
    [Arguments]  ${Order_Nr}
    SeleniumLibrary.Wait until element is visible   //td[text()='${Order_Nr}']/../td[11]  10
    SeleniumLibrary.Click Element                   //td[text()='${Order_Nr}']/../td[11]
    SeleniumLibrary.Wait until element is visible   //strong[text()='Edit purchase order']  5


Click Tab Shipments
    SeleniumLibrary.Wait until element is visible   //a[text()='Shipments']  5
    SeleniumLibrary.Click Element                   //a[text()='Shipments']
    SeleniumLibrary.Wait until element is visible   //button[contains(@ng-click,'Receive')]  5


Click Receive
    SeleniumLibrary.Click Element                   //button[contains(@ng-click,'Receive')]
    SeleniumLibrary.Wait until element is visible   //h3[contains(text(),'Receive shipment')]  5


Select All Lines
    SeleniumLibrary.Click Element                   (//label)[2]

Confirm Receival
    wait until keyword succeeds  2x  1s  run keywords
    ...         SeleniumLibrary.Wait until element is visible       //span[contains(text(),'Confirm')]  5
    ...  AND    SeleniumLibrary.Click Element                       //span[contains(text(),'Confirm')]
    ...  AND    SeleniumLibrary.Wait until element is not visible   //span[contains(text(),'Confirm')]  5
    SeleniumLibrary.Wait until element Is Visible       //div[@class='toast-message' and contains(text(),'Receive is successful')]  5
    SeleniumLibrary.Wait until element is not visible   //div[@class='toast-message' and contains(text(),'Receive is successful')]  10


