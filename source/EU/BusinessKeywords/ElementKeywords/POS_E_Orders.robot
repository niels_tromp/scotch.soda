*** Variables ***
${btn_orders}=  //ion-icon[contains(@src,'content-copy')]

*** Keywords ***
Select Orders Menu
    wait until keyword succeeds     3x  1s  SeleniumLibrary.Click Element   ${btn_orders}
    SeleniumLibrary.Wait until element is visible                           //ion-text/p[text()='Last modified orders']  10


Select Order
    [Arguments]  ${Order_Nr}
    SeleniumLibrary.Click Element                   //ion-card//span[text()=' ${Order_Nr} ']/ancestor::ion-card
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),'${Order_Nr}')]  10


Select Create Return
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create return ']  10
    SeleniumLibrary.Click Element                   //ion-button[text()=' Create return ']
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Confirm return ']  10


Enter Stock Location
    [Arguments]  ${Stock_location}
    SeleniumLibrary.Wait until element is visible    //ion-label[text()='Stock location']  10
    Sleep  1s  #To prevent empty options in the popup
    SeleniumLibrary.Click Element                   //ion-label[text()='Stock location']/ancestor::ion-item
    SeleniumLibrary.Wait until element is visible   //ion-alert//button//div[text()='${Stock_location}']  10
    SeleniumLibrary.Click Element                   //ion-alert//button//div[text()='${Stock_location}']
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                       //ion-alert//button/span[text()='OK']
    ...  AND    SeleniumLibrary.Wait until element is not visible   //ion-alert//button//div[text()='${Stock_location}']  5


Enter Return Reason
    [Arguments]  ${Return_reason}
    SeleniumLibrary.Click Element                   //ion-label[text()='Return reason']/ancestor::ion-item
    SeleniumLibrary.Wait until element is visible   //ion-alert//div//h2[text()='Return reason']  10
    SeleniumLibrary.Click Element                   //ion-alert//button//div[text()="${Return_reason}"]
    wait until keyword succeeds                     2x  0.5s   run keywords
    ...         SeleniumLibrary.Click Element                       //ion-alert//button/span[text()='OK']/parent::button
    ...  AND    SeleniumLibrary.Wait until element is not visible   //ion-alert//div//h2[text()='Return reason']  5

Enter Remarks
    [Arguments]  ${Remark}
    SeleniumLibrary.Click Element                                       //ion-label[text()='Remarks']/ancestor::ion-item
    wait until keyword succeeds  3x  1s  SeleniumLibrary.Input Text     //ion-textarea//textarea  ${Remark}
    SeleniumLibrary.Click Element                                       //ion-title[text()=' Create return ']


Click Confirm Return
    SeleniumLibrary.Click Element   //ion-button[text()=' Confirm return ']








