*** Settings ***
Library     String
Library     KeePassLibrary
Library     SeleniumLibrary
Library     ${sourcedir}${/}SUT${/}SO_KeePass.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_MainKeywords.robot

*** Variables ***
${POS_Login}=       //ion-card-content/form/div/ion-button[text()='Login ']
${ADMIN_Login}=     //button[text()='Login']
${POS_element}=     //ion-button[text()='Username ']
${ADMIN_element}=   //eva-login//p[text()='Scotch & Soda']
${KDBX}=    ${sourcedir}${/}..${/}..${/}Keepass${/}Scotch&Soda.kdbx
${KEY}=     ${sourcedir}${/}..${/}..${/}Keepass${/}Scotch&Soda.key
*** Keywords ***
Get KeePass Credentials
    [Arguments]  ${TITLE}
    ${entry}=  SO_KeePass.Get Entry By Environment  ${KDBX}  ${KEY}  ${TITLE}
    [Return]   ${entry}


Open Browser
    [Arguments]  ${IP_adress}
        SeleniumLibrary.Open Browser                    ${IP_adress}  Chrome
        SeleniumLibrary.Maximize Browser Window


Click Login
    [Arguments]  ${Domain}
    run keyword if  '${Domain}'== 'POS'     wait until keyword succeeds  3x  1s  Click Login on POS
	...    ELSE IF  '${Domain}'== 'ADMIN'   Click Login on ADMIN


Select organization
    [Arguments]  ${Domain}  ${Organization}=${EMPTY}
    run keyword if  '${Domain}'== 'POS'      Select organization on POS    ${Organization}
	...    ELSE IF  '${Domain}'== 'ADMIN'    Select organization on ADMIN



                                    #####################
                                    #                   #
                                    #######  POS  #######
                                    #                   #
                                    #####################

Input POS Credentials
    [Arguments]  ${Title}
                SeleniumLibrary.Wait Until Element is visible       ${POS_element}    20  error=The page didn't load within 20 seconds
                SeleniumLibrary.Click Element                       ${POS_element}
    ${User}=    Get KeePass Credentials                             ${Title}
                Input POS Username                                  ${User}
                Input POS Password                                  ${User}


Input POS Username
    [Documentation]  This keyword used to give username as input\n\n
    ...  *Parameters:*
    ...  | =Parameters= | =Description= | =Example= |
    ...  | ``USER`` | The username | TST2_MARINGS |
    ...  | ``ACCOUNT_TITLE`` | The title of the account in the keepass file | GUI |
    [Arguments]  ${User}
    ${User}     set variable                    ${User.username}
                Wait Until Keyword Succeeds     5x  1s
    ...         SeleniumLibrary.Input Text      //ion-input[@name='email']/input  ${User}


Input POS Password
    [Documentation]  This keyword used to give password as input\n\n
    ...  *Parameters:*
    ...  | =Parameters= | =Description= | =Example= |
    ...  | ``PASSWORD`` | The password of the account | WelKom_12_3! |
    ...  | ``ACCOUNT_TITLE`` | The title of the account in the keepass file | GUI |
    [Arguments]     ${User}
    ${prev_lvl}     Set Log Level                   NONE
    ${Password}     set variable  ${USER.password}
                    Wait Until Keyword Succeeds     5x  1s
    ...             SeleniumLibrary.input password  //ion-input[@name='password']/input  ${Password}
    	            Set Log Level                   ${prev_lvl}


Click Login on POS
    SeleniumLibrary.Click Element                       ${POS_element}
    SeleniumLibrary.wait until element is not visible   //app-keyboard
    SeleniumLibrary.Click Element                       ${POS_Login}
    POS_S_MainKeywords.Wait For Page To Load
    SeleniumLibrary.Wait until element is visible                       //ion-title[text()='Select your organization']


Select organization on POS
    [Arguments]  ${Organization}
    SeleniumLibrary.Scroll Element Into View    //ion-content/div[contains(text(),'${Organization}')]
    SeleniumLibrary.Click Element               //ion-content/div[contains(text(),'${Organization}')]
    run keyword and return status               POS_S_MainKeywords.Wait For Page To Load


Enter Pin
    SeleniumLibrary.Wait until element Is Visible   //pincode-unlock//pincode-numpad//span[text()='0']
    FOR  RANGE  1 5
    SeleniumLibrary.Click Element   //pincode-unlock//pincode-numpad//span[text()='0']
    END


Select Station on POS
    SeleniumLibrary.Wait until element is visible                   //div//ion-radio  10  error=Station popup didn't show on time or the financial period is not open
    SeleniumLibrary.Click Element                   //div//ion-radio
    SeleniumLibrary.Wait Until Element Is Enabled   //ion-button[text()='Apply ']
    SeleniumLibrary.Click Element                   //ion-button[text()='Apply ']
    SeleniumLibrary.Wait until element is visible                   //ion-router-outlet//div[@class='side-nav']  10

Fetch Build Number
                    SeleniumLibrary.Wait until element is visible   //div[contains(@class,'version')]  10
    ${Build_Nr}=    SeleniumLibrary.Get Text        //div[contains(@class,'version')]
    ${Build_Nr}=    String.Fetch From Right         string=${Build_Nr}    marker=upgrade${space}
    [Return]  ${Build_Nr}




                                    #####################
                                    #                   #
                                    ####### ADMIN #######
                                    #                   #
                                    #####################

Input ADMIN Credentials
    [Arguments]  ${Title}
                SeleniumLibrary.Wait Until Element is visible       ${ADMIN_element}    20
                SeleniumLibrary.Click Element                       ${ADMIN_element}
    ${User}=    Get KeePass Credentials                             ${Title}
                Input ADMIN Username                                ${User}
                Input ADMIN Password                                ${User}


Input ADMIN Username
    [Documentation]  This keyword used to give username as input\n\n
    ...  *Parameters:*
    ...  | =Parameters= | =Description= | =Example= |
    ...  | ``USER`` | The username | TST2_MARINGS |
    ...  | ``ACCOUNT_TITLE`` | The title of the account in the keepass file | GUI |
    [Arguments]  ${User}
    ${User}     set variable                    ${User.username}
                Wait Until Keyword Succeeds     5x  1s
    ...         SeleniumLibrary.Input Text      //input[@placeholder='Username']  ${User}


Input ADMIN Password
    [Documentation]  This keyword used to give password as input\n\n
    ...  *Parameters:*
    ...  | =Parameters= | =Description= | =Example= |
    ...  | ``PASSWORD`` | The password of the account | WelKom_12_3! |
    ...  | ``ACCOUNT_TITLE`` | The title of the account in the keepass file | GUI |
    [Arguments]  ${User}
    ${prev_lvl}     Set Log Level                   NONE
    ${Password}     set variable                    ${User.password}
                    Wait Until Keyword Succeeds     5x  1s
    ...             SeleniumLibrary.input password  //input[@placeholder='Password']  ${Password}
    	            Set Log Level                   ${prev_lvl}


Select organization on ADMIN
    SeleniumLibrary.Click Element   ${ADMIN_Login}
    SeleniumLibrary.Wait until element is visible   //span[text()='Select an organization']  5
    SeleniumLibrary.Click Element   //span[text()='Select an organization']
    SeleniumLibrary.Click Element   //ul/li//div[text()='Scotch & Soda']


Click Login On ADMIN
    SeleniumLibrary.Click Element   ${ADMIN_Login}
    SeleniumLibrary.Wait until element is visible   //table/tbody/tr[15]  10






