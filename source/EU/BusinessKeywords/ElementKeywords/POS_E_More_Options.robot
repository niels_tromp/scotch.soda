*** Settings ***
Library     String


Resource   ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot

*** Variables ***
${btn_Income_Expenses}=         //ion-icon[@name='newspaper']/following::span[text()='Income and expenses']
${btn_Move_Cash}=               //ion-icon[@name='newspaper']/following::span[text()='Move cash']
${btn_Bank_Deposits}=           //ion-icon[@name='newspaper']/following::span[text()='Bank deposits']
${btn_New}=                     //ion-buttons//button/div[text()=' New ']
${btn_Income}=                  //ion-card//ion-text[text()='Income ']/ancestor::ion-card
${Select_Station_Field}=        //ion-item/ion-label[text()='Select Station']
${Label_Cash_Drawers}=          //eva-label//p[text()='Cash drawers']
${btn_Expense}=                 //ion-card//ion-text[text()=' Expense']/ancestor::ion-card
${btn_Income/Expense_Type}=     //ion-label[contains(text(),'Select')]/ancestor::ion-item
${Tax_Field}=                   //ion-label[text()='Select tax type']/ancestor::ion-item
${Amount_Field}=                //ion-label[text()='Amount']/ancestor::ion-item
${Description_Field}=           //ion-label[text()='Description']/ancestor::ion-item

${xpath_Station_type}=          //ion-row/p[text()='{}']/following-sibling::select-device-component[1]//ion-select
${xpath_Station_Selector}=      //div[contains(@class,'alert-wrapper')]//div[contains(text(),'{}')]
${xpath_10_Euro}=               //p[text()=' Notes']/following::ion-card-content/eva-cash[2]//ion-col[1]//input
${xpath_1_Eurocent}=            (//p[text()=' Coins']/following::ion-card-content/eva-cash[1]//ion-col[1]//input)[1]

${xpath_Sealbag_Nr}=            //ion-input[@ng-reflect-name='sealbagNumber']/input
${xpath_New_Deposit}=           //ion-button[text()=' Add new deposit ']


*** Keywords ***
Select More Options Menu
    SeleniumLibrary.Page Should Not Contain Element     //div[contains(@class,'alert')]//button/span[contains(text(),'es')]
    ...                                                 message= Cash register is still closed, please open it manually
    SeleniumLibrary.Wait until element is visible       //ion-icon[contains(@src,'more')]  10
    Sleep  0.5s  #To be sure the Tasks button is not clicked
    wait until keyword succeeds  3x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                       //ion-icon[contains(@src,'more')]
    ...  AND    SeleniumLibrary.Wait until element is visible       //ion-title[text()=' More options ']  10



                                #######################
                                #                     #
                                # Income and expenses #
                                #                     #
                                #######################

Select Income And Expense
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}  10
    SeleniumLibrary.Scroll Element Into View        ${btn_Income_Expenses}
    SeleniumLibrary.Click Element                   ${btn_Income_Expenses}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Income and expenses ']  10


Click On New Item
    [Arguments]  ${Title}
    SeleniumLibrary.Wait until element is visible   ${btn_New}  10
    SeleniumLibrary.Click Element                   ${btn_New}
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),'${Title}')]  10


Select Income
    SeleniumLibrary.Wait until element is visible   ${Select_Station_Field}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_Income}
    ...  AND    SeleniumLibrary.Wait until element is visible   ${btn_Income}\[contains(@class,'selected')]


Select Expense
    SeleniumLibrary.Wait until element is visible   ${Select_Station_Field}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_Expense}
    ...  AND    SeleniumLibrary.Wait until element is visible   ${btn_Expense}\[contains(@class,'selected')]


Enter Income/Expense Type
    [Arguments]  ${Type}
                    SeleniumLibrary.Wait until element is visible   ${btn_Income/Expense_Type}  10
    ${Filled_In}=   run keyword and return status                   SeleniumLibrary.Page Should Not Contain Element
    ...                                                             //ion-content//div[contains(text(),'${Type}')]
                    run keyword unless  ${Filled_In}  return from keyword
                    SeleniumLibrary.Click Element                   ${btn_Income/Expense_Type}
                    SeleniumLibrary.Wait until element is visible   //h2[contains(text(),'Select')]  10
                    SeleniumLibrary.Click Element                   //div[contains(text(),'${Type}')]
                    wait until keyword succeeds  2x  0.5s  run keywords
    ...             SeleniumLibrary.Click Element                       //button/span[text()='OK']
    ...  AND        SeleniumLibrary.Wait until element is not visible   //button//div[contains(text(),'${Type}')]


Enter Tax
    [Arguments]  ${Tax}
    SeleniumLibrary.Wait until element is visible   ${Tax_Field}  10
    SeleniumLibrary.Click Element                   ${Tax_Field}
    SeleniumLibrary.Wait until element is visible   //h2[text()='Select tax type']  10
    SeleniumLibrary.Click Element                   //div[text()=' ${Tax} ']
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                       //button/span[text()='OK']
    ...  AND    SeleniumLibrary.Wait until element is not visible   //div[text()=' ${Tax} ']


Enter Amount
    [Arguments]  ${Amount}
    SeleniumLibrary.Wait until element is visible   ${Amount_Field}  10
    SeleniumLibrary.Input Text                      //ion-input[@formcontrolname='amount']/input  ${Amount}


Enter Description
    [Arguments]  ${description}
    SeleniumLibrary.Wait until element is visible   ${Description_Field}  10
    SeleniumLibrary.Input Text                      //ion-input[@formcontrolname='description']/input  ${Description}


Select Station
    SeleniumLibrary.Click Element                   //ion-label[contains(text(),'Station')]/following::ion-select
    SeleniumLibrary.Wait until element is visible   //div[contains(@class,'alert-wrapper')]//div[contains(text(),'Sentinel')]  5
    SeleniumLibrary.Click Element                   //div[contains(@class,'alert-wrapper')]//div[contains(text(),'Sentinel')]
    SeleniumLibrary.Click Element                   //div[contains(@class,'alert')]//button/span[text()='select']


Click Add Income/Expense
    [Arguments]  ${Amount}  ${Type}
    SeleniumLibrary.Wait Until Element Is Enabled   //ion-button[text()=' Add ${Type} ']  5
    SeleniumLibrary.Click Element                   //ion-button[text()=' Add ${Type} ']
    SeleniumLibrary.Wait until element is visible   //ion-row[last()]/ion-col[5]//span[contains(text(),'${Amount}')]  10


                                #############
                                #           #
                                # Move Cash #
                                #           #
                                #############
Select Move Cash
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}  10
    SeleniumLibrary.Scroll Element Into View        ${btn_Move_Cash}
    SeleniumLibrary.Click Element                   ${btn_Move_Cash}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Move cash ']  10


Select Correct Station
    [Documentation]
    ...  This keyword checks if the correct source or destination station is selected. Unfortunately it is not possible to fetch the
    ...  name in the field, so the popup should be opened en is verified if the given station is selected or not.
    [Arguments]  ${Station_position}  ${Station_Name}
    ${xpath_Source_Station}=    Replace String                                  ${xpath_Station_type}       {}  ${Station_position}
                                SeleniumLibrary.Wait until element is visible   ${xpath_Source_Station}     10
                                SeleniumLibrary.Click Element                   ${xpath_Source_Station}
    ${xpath_Station_Selector}=  Replace String                                  ${xpath_Station_Selector}   {}  ${Station_Name}
                                SeleniumLibrary.Wait until element is visible   ${xpath_Station_Selector}   10
                                sleep  0.1s
                                SeleniumLibrary.Click Element                   ${xpath_Station_Selector}
                                SeleniumLibrary.Click Element                   //div[contains(@class,'alert')]//button/span[text()='select']


Enter 10 Euro
    [Arguments]  ${Amount}
    SeleniumLibrary.Wait until element is visible   //p[text()=' Coins']  10
    SeleniumLibrary.Scroll Element Into View        ${xpath_10_Euro}
    SeleniumLibrary.Input Text                      ${xpath_10_Euro}  ${Amount}
    SeleniumLibrary.Click Element                   //p[text()=' Notes']

Enter Fixed amount
    [Arguments]  ${Amount}
    SeleniumLibrary.Wait until element is visible   //p[text()=' Coins']  10
    SeleniumLibrary.Input Text                      ${xpath_1_Eurocent}  ${Amount}
    SeleniumLibrary.Click Element                   //p[text()=' Coins']

Click Move Cash
    SeleniumLibrary.Wait until element is visible   //ion-button[contains(text(),'Move cash')]  10
    SeleniumLibrary.Click Element                   //ion-button[contains(text(),'Move cash')]
    SeleniumLibrary.Wait until element is visible   //page-move-cash-list  10


                                ################
                                #              #
                                # Bank Deposit #
                                #              #
                                ################

Select Bank Deposits
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}  10
    SeleniumLibrary.Scroll Element Into View        ${btn_Bank_Deposits}
    SeleniumLibrary.Click Element                   ${btn_Bank_Deposits}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Bank deposits ']  10


Enter Sealbag Number
                SeleniumLibrary.Scroll Element Into View    //h4[text()='Payment method']
    ${Number}=  evaluate                                    random.randint(111111, 999999)
                SeleniumLibrary.Input Text                  ${xpath_Sealbag_Nr}  ${Number}
                SeleniumLibrary.Click Element               //h4[text()='Payment method']
    [Return]    ${Number}


Click Add New Deposit
    SeleniumLibrary.Wait until element is visible   ${xpath_New_Deposit}  10
    SeleniumLibrary.Click Element                   ${xpath_New_Deposit}
    SeleniumLibrary.Wait until element is visible   //eva-toast  10
    SeleniumLibrary.Element Should Not Contain      //eva-toast  There is not enough of Cash available to make this transaction. Please try again.
    ...                                             message= Transaction failed, there is not enough money in the safe
    SeleniumLibrary.Element Should Contain          //eva-toast   Cash deposit successfully created
    SeleniumLibrary.Wait until element is visible   //page-bank-deposits-list  10


Check Amount of Deposits
                    SeleniumLibrary.Wait until element is visible   //ion-card-content/ion-row[1]/ion-col[2]  5
                    Sleep  0.5s  #Needed for the page to load completely
    ${Deposits}=    SeleniumLibrary.Get Text                        //ion-card-content/ion-row[1]/ion-col[2]
    ${Deposits}=    convert to integer                              ${Deposits}
    [Return]        ${Deposits}


Check New Amount Of Deposits
    [Arguments]  ${Before}
    SeleniumLibrary.Wait until element is visible  //ion-card-content/ion-row[1]/ion-col[2 and text()=' ${Before+1} ']



                                ####################
                                #                  #
                                # Financial period #
                                #                  #
                                ####################

Click On Open Financial Period
    SeleniumLibrary.Click Element                   //ion-card//span[text()='Open financial period']/parent::div
    SeleniumLibrary.Wait until element is visible   //ion-col/span[text()='Cash drawer']/ancestor::ion-row//ion-badge[contains(text(),'CLOSED')]  10


Click On Close Financial Period
    SeleniumLibrary.Wait until element is visible   //ion-card//span[text()='Close financial period']/parent::div[not(contains(@class,'disable'))]  5
    SeleniumLibrary.Click Element                   //ion-card//span[text()='Close financial period']/parent::div
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Close financial period ']  5


Click On Button Close Financial Period
    SeleniumLibrary.Wait until element is visible   //ion-button[contains(text(),' Close financial period')]  5
    SeleniumLibrary.Click Element                   //ion-button[contains(text(),' Close financial period')]
    SeleniumLibrary.Wait until element is visible   //h2[text()='Close financial period']  5


Acknowledge Popup
    SeleniumLibrary.Wait until element is visible   //div[contains(@class,'alert')]//button/span[contains(text(),'es')]  10
    Sleep  1s
    SeleniumLibrary.Click Element                   //div[contains(@class,'alert')]//button/span[contains(text(),'es')]
    SeleniumLibrary.Wait until element is not visible               //div[contains(@class,'alert')]//button/span[contains(text(),'es')]  20
    Sleep  0.5s


Click Reports
    SeleniumLibrary.Click Element                   //ion-card//span[text()='Reports']/parent::div
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Financial periods ']  5


Open Current Financial Period
    SeleniumLibrary.Wait until element is visible   //ion-text[text()=' Open ']/ancestor::ion-row  10
    ...                                             error=Financial reports did not load
    SeleniumLibrary.Click Element                   //ion-text[text()=' Open ']/ancestor::ion-row
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),' Financial period #')]  5


Fetch Financial Period Nr
    ${Financial_Period_Nr}=     SeleniumLibrary.get text    //ion-title[contains(text(),' Financial period #')]
    ${Financial_Period_Nr}=     String.Fetch From Right     string=${Financial_Period_Nr}    marker=#
    [Return]  ${Financial_Period_Nr}


Fetch Cash Amount
                        SeleniumLibrary.Wait until element is visible   (//eva-price/span[@itemprop='priceSpecification'])[1]
    ${Cash_Amount}=     SeleniumLibrary.get text                        (//eva-price/span[@itemprop='priceSpecification'])[1]
    ${Cash_Amount}=     String.Fetch From Right                         string=${Cash_Amount}    marker=€${SPACE}
    [Return]  ${Cash_Amount}




                                ################
                                #              #
                                # Cash Drawers #
                                #     Safe     #
                                #              #
                                ################


#Handle Open Cash Drawer Popup
#    SeleniumLibrary.Wait until element is visible                   //div[contains(@class,'alert')]/div[contains(text(),'Your cashdrawer')]  10
#    SeleniumLibrary.Click Element                   //div[contains(@class,'alert')]//span[text()='yes']
#    SeleniumLibrary.Wait until element is visible   //p[text()=' Notes']  10


Click On Cash Drawer
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Click Element                   //ion-col/span[text()='Cash drawer']/ancestor::ion-row
    ...  AND    SeleniumLibrary.Wait until element is visible   //p[text()=' Notes']  15


Click On Safe
    wait until keyword succeeds  2x  0.5s  SeleniumLibrary.Click Element    //ion-col/span[text()='Safe']/ancestor::ion-row


Click Close Station
    SeleniumLibrary.Wait until element is visible   //ion-button[contains(text(),'Close')]  10
    Sleep  1s  # To be sure the action is not to quick
    SeleniumLibrary.Click Element                   //ion-button[contains(text(),'Close')]


Click Open Station
    SeleniumLibrary.Wait until element is visible   //ion-button[contains(text(),'Open')]  10
    Sleep  1s  # To be sure the action is not to quick
    SeleniumLibrary.Click Element                   //ion-button[contains(text(),'Open')]


Close Count Correction Popup
    [Arguments]  ${State}
    ${Popup}  run keyword and return status    SeleniumLibrary.Wait until element is visible  //ion-alert//h2[text()='Count correction']  5
    run keyword if  ${Popup}  wait until keyword succeeds  2x  1s  run keywords
    ...         SeleniumLibrary.Click Element   //ion-alert//button/span[text()='OK']
    ...  AND    SeleniumLibrary.Wait until element is not visible  //ion-alert//h2[text()='Count correction']  5
    ...  AND    Click ${State} Station


Enter Correction Reason
    [Arguments]  ${Correction_Reason}
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),'Count correction')]  5
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Input Text      //ion-label[text()='Correction reason']/ancestor::ion-item//input
    ...                                         ${Correction_Reason}
    ...  AND    SeleniumLibrary.Wait until element Is Enabled   //ion-button[text()='Confirm ' and not(@disabled)]  5


Click Confirm
    SeleniumLibrary.Click Element                   //ion-button[text()='Confirm ']
    SeleniumLibrary.Wait until element is visible   //ion-col/span[text()='Cash drawer']  10


Close Limit Popup
                SeleniumLibrary.Wait until element is visible   //h2[contains(@class,'alert-title')]  5
                Sleep  1s  #To be sure the popup is visible
    ${status}=  Run Keyword And Return Status                   variable should exist   ${Regression}
                run keyword if  ${status}                       ADMIN_S_MainKeywords.Take Screenshot
    ...                                                         Limit_Popup  ${Folder}${/}Screenshots${/}${TEST NAME}
                SeleniumLibrary.Click Element                   //button/span[text()='Move Cash']
                SeleniumLibrary.Wait until element is visible   //ion-card-content[contains(text(),'closed')]
                Sleep  1s  #To be sure the next screen is visible
                run keyword if  ${status}                       ADMIN_S_MainKeywords.Take Screenshot
    ...                                                         Limit_Warning  ${Folder}${/}Screenshots${/}${TEST NAME}



                                ############
                                #          #
                                # Giftcard #
                                #          #
                                ############

Click Check Giftcard Amount
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}
    SeleniumLibrary.Scroll Element Into View        //p[contains(text(),'System')]
    SeleniumLibrary.Wait until element is visible   //ion-card//span[contains(text(),'giftcard')]  5
    SeleniumLibrary.Click Element                   //ion-card//span[contains(text(),'giftcard')]
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),'giftcard')]  5


Enter Giftcard Number
    [Arguments]  ${Giftcard_Nr}
    SeleniumLibrary.Input Text  //ion-input[@ng-reflect-name='cardNumber']/input  ${Giftcard_Nr}


Click Check Balance
    SeleniumLibrary.Wait Until Element Is Enabled   //ion-button[text()=' Check balance ']
    SeleniumLibrary.Click Element                   //ion-button[text()=' Check balance ']
    SeleniumLibrary.Wait until element is visible   //div[contains(@class,'gift-card-balance')]  5


                                ############
                                #          #
                                #  System  #
                                #          #
                                ############

Click Configure Manual Scanner
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}  10
    SeleniumLibrary.Scroll Element Into View        //p[contains(text(),'Information')]
    SeleniumLibrary.Click Element                   //ion-card//span[contains(text(),'Configure manual')]

Check Barcodes
    SeleniumLibrary.Wait until element is visible   //div[contains(@class,'barcode')]/img  5
    Sleep  0.5s  #To be sure the image is rendered
    ADMIN_S_MainKeywords.Take Screenshot            Manual-scanner-barcode_1
    SeleniumLibrary.Wait until element is visible   //ion-segment-button[text()=' Manual scanner ']  5
    SeleniumLibrary.Click Element                   //ion-segment-button[text()=' Manual scanner ']
    SeleniumLibrary.Wait until element is visible   //figure//company-logo/img  5
    Sleep  0.5s  #To be sure the image is rendered
    ADMIN_S_MainKeywords.Take Screenshot            Manual-scanner-barcode_2


Click Set-up CFD
    SeleniumLibrary.Wait until element is visible   ${Label_Cash_Drawers}  10
    SeleniumLibrary.Scroll Element Into View        //p[contains(text(),'Information')]
    SeleniumLibrary.Click Element                   //ion-card//span[contains(text(),'Customer Facing')]
    SeleniumLibrary.Wait until element is visible   //ion-title[contains(text(),'Customer Facing')]  5


Check Pair QR-code
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create pairing QR ']  5
    SeleniumLibrary.Click Element                   //ion-button[text()=' Create pairing QR ']
    SeleniumLibrary.Wait until element is visible   //ion-card-content//canvas  5
    Sleep  0.5s  #To be sure the image is rendered
    ADMIN_S_MainKeywords.Take Screenshot            CFD-barcode