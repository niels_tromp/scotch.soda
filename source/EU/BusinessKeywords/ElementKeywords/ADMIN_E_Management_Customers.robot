*** Settings ***
Library     Collections
Library     String

*** Keywords ***
Enter Name
    [Arguments]  ${Name}
    SeleniumLibrary.Wait until element is visible   //input[@name='name']   5
    SeleniumLibrary.Input Text                      //input[@name='name']   ${Name}
    SeleniumLibrary.Wait until element is visible   //tbody/tr[1]/td[2]     5


Select Customer
    SeleniumLibrary.Click Element                   //tbody/tr[1]/td[5]/a
    SeleniumLibrary.Wait until element is visible   //h4[text()='General info']   5

Click Delete
    SeleniumLibrary.Scroll Element Into View        //div/a[contains(text(),'Back')]
    SeleniumLibrary.Click Element                  (//div/button[contains(text(),'Delete')])[1]
    SeleniumLibrary.Wait until element is visible   //h3[text()='Delete user']  5


Acknowledge Deletion Popup
    SeleniumLibrary.Click Element                       //button[contains(@ng-click,'yes')]
    SeleniumLibrary.Wait until element is not visible   //h3[text()='Delete user']  5
    SeleniumLibrary.Wait until element is visible       //div[contains(text(),'User is deleted')]  10








