*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_General_Profile.robot


*** Keywords ***
Change Language
    [Documentation]
    ...  This keyword will change the language to the given language when the page is displayed/translated in English.
    [Arguments]  ${Language}
    ADMIN_E_General_Profile.Select Language  ${Language}
    ADMIN_E_General_Profile.Click Safe

DE_Change Language
    [Documentation]
    ...  This keyword will change the language to the given language when the page is displayed/translated in German.
    [Arguments]  ${Language}
    ADMIN_E_General_Profile.Select Language  ${Language}
    ADMIN_E_General_Profile.DE_Click Safe