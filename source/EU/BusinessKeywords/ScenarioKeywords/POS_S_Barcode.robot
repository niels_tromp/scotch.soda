*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Barcode.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Search.robot


*** Keywords ***
Search Product On Barcode
    [Documentation]
    ...  This keyword used the barcode menu to enter a product barcode and checks if the right product is found.\n
    ...  First the search menu is selected because the barcode button will not directly work.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Barcode` | The barcode-code of the product | 8718859333713 | Mandatory |
    ...                 | `Product_Code` | The product code of the specific item and size | 150634_3047-L | Mandatory |
    [Arguments]  ${Barcode}  ${Product_Code}
    POS_E_Search.Select Search Menu
    POS_E_Barcode.Select Barcode Menu
    POS_E_Barcode.Enter Barcode         ${Barcode}  ${Product_Code}

