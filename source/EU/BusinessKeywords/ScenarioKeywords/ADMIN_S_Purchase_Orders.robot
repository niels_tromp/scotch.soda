*** Settings ***
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Purchase_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Purchase_Orders.robot


*** Keywords ***
Create New Purchase Order
    [Documentation]
    ...  This keyword will create a new purchase order with the given "Supplier", "Receiver" and products and quantities
    ...  given in the csv file. A Reference is also given to the new PO.
    ...     | =Parameter= | =Description= | =Example= |
    ...     | Supplier | The supplier of the goods | EU=AX Main, US/CAD=Bergen | Mandatory |
    ...     | Receiver | The receiver of the goods, most of the time the code of the store | AT003 | Mandatory |
    ...     | csv | The csv file location and name with the product names and quantities to be ordered | filepath | Mandatory |
    ...     | Reference | The reference given to the Purchase order | Default=TestAutomation | Optional |
    ...     *Return Value:*
    ...     | =Value= | =Type= | =Description= | =Example= |
    ...     | `Order_Nr` | String | The purchase order number | 2344 |

    [Arguments]  ${Supplier}  ${Receiver}  ${csv}  ${Reference}=TestAutomation
                    ADMIN_E_Purchase_Orders.Click New Purchase Order
                    Set Supplier And Receiver                           ${Supplier}  ${Receiver}
                    Set Delivery Date
                    Enter Products                                      ${csv}
    ${Order_Nr}=    Place Order                                         ${Reference}
    [Return]  ${ORder_Nr}

Set Supplier And Receiver
    [Documentation]
    ...  This keyword will populate the fields for the Supplier and Receiver when creating a Purchase Order
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Supplier | The supplier of the goods | EU=AX Main, US/CAD=Bergen | Mandatory |
    ...  | Receiver | The receiver of the goods, most of the time the code of the store | AT003 | Mandatory |
    [Arguments]  ${Supplier}  ${Receiver}
    ADMIN_E_Purchase_Orders.Enter Supplier  ${Supplier}
    ADMIN_E_Purchase_Orders.Enter Receiver  ${Receiver}


Set Delivery Date
    [Documentation]
    ...  This keyword will set the delivery date to the date of today
    ADMIN_E_Purchase_Orders.Enter Delivery Date


Enter Products
    [Documentation]
    ...  This keyword will fill in the products to be ordered by reading a csv file and filling in the correct product
    ...  names and quantities.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | csv | The csv file location and name with the product names and quantities to be ordered | filepath | Mandatory |
    [Arguments]  ${csv}
    ADMIN_E_Purchase_Orders.Enter Product Details With CSV  ${csv}


Place Order
    [Documentation]
    ...  This keyword will finalize the Purchase order and adds a reference to the order. It will return the created
    ...  order number.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Reference | The reference given to the Purchase order | Default=TestAutomation | Optional |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Order_Nr` | String | The purchase order number | 2344 |
    [Arguments]  ${Reference}
                    ADMIN_E_Purchase_Orders.Click Save
                    ADMIN_E_Purchase_Orders.Acknowledge Place Order Popup
    ${Order_Nr}=    ADMIN_A_Purchase_Orders.Get Order Number
                    ADMIN_E_Purchase_Orders.Click Confirm
                    ADMIN_E_Purchase_Orders.Add Reference  ${Reference}
    [Return]        ${Order_Nr}


Create New Shipment
    [Documentation]
    ...  This keyword will create a new shipment by searching on the given order number.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Order_Nr | The order number of the purchase order | 3242 | Mandatory |
    ...  | Delivery_Note_Number | The delivery note number that should be given to the shipment | Default=123456 | Optional |
    [Arguments]  ${Order_Nr}  ${Delivery_Note_Number}=123456
    ADMIN_E_Purchase_Orders.Click New Shipment
    ADMIN_E_Purchase_Orders.Enter DNN           ${Delivery_Note_Number}
    ADMIN_E_Purchase_Orders.Enter Order Nr      ${Order_Nr}
    ADMIN_E_Purchase_Orders.Select Order Popup
    ADMIN_E_Purchase_Orders.Select Products
    ADMIN_E_Purchase_Orders.Create shipment
    ADMIN_E_Purchase_Orders.Click Save Shipment
    ADMIN_E_Purchase_Orders.Return To Overview


Receive Order
    [Documentation]
    ...  This keyword will receive the purchase order so it will be reflected in the POS.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Order_Nr | The order number of the purchase order | 3242 | Mandatory |
    [Arguments]  ${Order_Nr}
    ADMIN_E_Purchase_Orders.View Order  ${Order_Nr}
    ADMIN_E_Purchase_Orders.Click Tab Shipments
    ADMIN_E_Purchase_Orders.Click Receive
    ADMIN_E_Purchase_Orders.Confirm Receival
