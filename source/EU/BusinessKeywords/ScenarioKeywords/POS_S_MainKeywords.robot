*** Keywords ***
Wait For Page To Load
    [Documentation]
    ...  This general keyword will wait for the loading popup to show and dissapear, to be shure the page has been loaded.
    SeleniumLibrary.Wait until element is visible        //ion-loading//div[text()='Loading please wait...']  5  error=The page didn't load within 5 seconds
    SeleniumLibrary.Wait until element is not visible    //ion-loading//div[text()='Loading please wait...']  10  error=The page didn't load within 10 seconds


Wait for Tax Calculation
    [Documentation]
    SeleniumLibrary.Wait until element is visible       //div[contains(text(),'Calculating tax estimates')]  5       error=The tax calculation popup didn't show within 5 seconds
    SeleniumLibrary.Wait until element is not visible   //div[contains(text(),'Calculating tax estimates')]  10      error=The tax camculation popup didn't dissapear within 10 seconds



Wait For Payment To Process
    [Documentation]
    ...  This general keyword will wait for the payment processing popup to show and dissapear, to be sure the payment
    ...  has been succesfully processed.
    SeleniumLibrary.Wait until element is visible        //ion-alert  5  error=The payment didn't start within 5 seconds
    SeleniumLibrary.Wait until element is not visible    //ion-alert  10  error=The payment didn't process within 10 seconds

Wait For Discount To Apply
    [Documentation]
    ...  This general keyword will wait for the discount to be applied. It will wait for the popup to show and dissapear,
    ...  to be sure the discount has been succesfully added.
    SeleniumLibrary.Wait until element is visible       //div[@class='eva-overlay-containers']//p[text()=' Discount added ']  5
    SeleniumLibrary.Wait until element is not visible   //div[@class='eva-overlay-containers']//p[text()=' Discount added ']  5


Wait For Order Type To Change
    [Documentation]
    ...  This general keyword will wait for the order type to change from Carry out to Delivery. It will wait for the popup to show and dissapear,
    ...  to be sure the order type has changed.
    SeleniumLibrary.Wait until element is visible       //div[@class='eva-overlay-containers']//p[text()=' Order type changed successfully ']  5
    SeleniumLibrary.Wait until element is not visible   //div[@class='eva-overlay-containers']//p[text()=' Order type changed successfully ']  5


Wait For Order To Validate
    [Documentation]
    ...  This general keyword will wait for the Order Validation popup to dissapear, to be sure the receipt screen
    ...  has been loaded and is clickable.
    run keyword and return status   SeleniumLibrary.Wait until element is visible       //ion-loading  5
    SeleniumLibrary.Wait until element is not visible   //ion-loading  10  error=The order hasn't been validated within 10 seconds


Wait For Drawer Alert To Close
    [Documentation]
    ...  This general keyword will wait for the popup for the cash drawer to appear and dissapear, to be sure the drawer
    ...  is closed.
    SeleniumLibrary.Wait until element is visible        //ion-alert//div[text()='Please close the cash drawer to continue!']  5  error=The cash drawer didn't close within 5 seconds
    SeleniumLibrary.Wait until element is not visible    //ion-alert//div[text()='Please close the cash drawer to continue!']  10  error=The cash drawer didn't close within 10 seconds






