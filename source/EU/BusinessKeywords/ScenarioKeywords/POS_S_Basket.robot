*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Barcode.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Checkout.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Checkout.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Customers.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_MainKeywords.robot


*** Keywords ***
Checkout Basket With Cash
    [Documentation]
    ...  This keyword processes the product by paying it with cash and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Cash_Amount` | The cash amount which is payed with, giving in nothig will pay the full amount | 125 | Optional |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Cash_Amount}=${EMPTY}  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Cash          ${Cash_Amount}
    POS_E_Checkout.Pay with Cash


Checkout Basket With Card
    [Documentation]
    ...  This keyword processes the product by paying it with card and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Card
    POS_E_Checkout.Pay with Card


Checkout Basket With Manual EFT
    [Documentation]
    ...  This keyword processes the product by paying it with EFT and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Manual EFT
    POS_E_Checkout.Pay with Manual EFT


Checkout Basket With Scotch EGC
    [Documentation]
    ...  This keyword processes the product by paying it with a Scotch Electronic Giftcard and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Scotch EGC
    POS_E_Checkout.Pay with Scotch EGC


Checkout Basket With Giftcard
    [Documentation]
    ...  This keyword processes the product by paying it with a Giftcard and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Giftcard_Nr` | A boolen if a big bag should be added to the order | ${True} ${False} | Optional |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    ...                 | `Return` | An boolean to determine if there will be a popup for a return amount and if the money should be returned. Default=${False} | ${True} ${False} | Optional |
    ...                 | | None wil expect no popup to be shown and the amount on the giftcard should be less than the needed amount to be payed | | |
    ...                 | | True wil expect a popup to be shown and the remaining amount on the giftcard should be given back as cash | | |
    ...                 | | False wil expect a popup to be shown and the remaining amount on the giftcard should sya on the giftcard | | |
    [Arguments]  ${Giftcard_Nr}  ${Remark}=${False}  ${Return}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Giftcard
    POS_E_Checkout.Pay with Giftcard    ${Giftcard_Nr}  ${Return}


Checkout Basket Without Paying
    [Documentation]
    ...  This keyword processes an order when a return is being done in return for another size. So no payment needs to be
    ...  done.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Click Proceed
    Sleep  3s  #added so backend sets order to completed



Checkout Return With Cash
    [Documentation]
    ...  This keyword processes the product by paying it with cash and will wait until the drawer is closed again.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Cash_Amount` | The cash amount which is payed with, giving in nothig will pay the full amount | 125 | Optional |
    ...                 | `Popup` | A boolen if the return popup should be closed | ${True}(Default)/${False} | Optional |
    ...                 | `Remark` | An order remark given to the order, usually the name of the testcase | ${True} ${False} | Optional |
    [Arguments]  ${Cash_Amount}=${EMPTY}  ${Popup}=${True}  ${Remark}=${False}
    POS_E_Basket.Select checkout
    POS_S_Checkout.Add Order Remark     ${Remark}
    POS_E_Checkout.Select Cash          ${Cash_Amount}
    POS_E_Checkout.Pay with Cash
    POS_E_Checkout.Close Return popup   ${Popup}


Select Endless Aisle
    [Documentation]
    ...  This keyword changes the order type to Delivery(Endless Aisle) by selecting 'Delivery' within the 'Carry out' option.
    POS_E_Basket.Select Carry Out
    POS_E_Basket.Select Delivery


Add Coupon
    [Documentation]
    ...  This keyword adds a coupon to an order by giving in the `${Coupon_Code}`. The default one is for 20% off the
    ...  total  price. To do so, after a product is selected and is placed inside the basket, it will click on discount,
    ...  click on the coupons button, enter the coupon code and apply the coupon to the basket.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Coupon_Code` | The numeric value of the coupon code to be used | 654321(default) | Optional |
    [Arguments]  ${Coupon_Code}=654321
    POS_E_Basket.Click On Discount
    POS_E_Basket.Click On Coupons
    POS_E_Basket.Enter Coupon Code  ${Coupon_Code}
    POS_E_Basket.Click Apply Coupon


Print Receipts
    [Documentation]
    ...  This keyword will 'print' the receipts after an order is completed. It will trigger events to ad an invoice
    ...  and document to the order, to be found in the ADMIN.
    Sleep  2s  # To be sure the cash drawer popup has closed
    POS_E_Basket.Click Print Receipt


Enter Giftcard Information
    [Documentation]
    ...  This keyword will enter giftcard information after a giftcard is selected. It will add the giftcard to the basket
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Number` | The giftcard number to be sold | 999000920001 | Mandatory |
    ...                 | `Amount` | The amount that should be placed on the giftcard | 20 | Mandatory |
    ...                 | `Product_Code` | The code or name of the product shown in the product card underneath 'Carry out'| 20345-23554-L | Mandatory |
    [Arguments]  ${Number}  ${Amount}  ${Product_Code}
    POS_E_Basket.Enter Giftcard Number          ${Number}
    POS_E_Basket.Enter Giftcard Amount          ${Amount}
    Sleep  0.5s  #To prevent error messages of needing a giftcard nomber or amount
    POS_E_Barcode.Click Add To Basket           Add to basket
    POS_E_Barcode.Check If Product Is Added     ${Product_Code}


Add Customer To Order
    [Documentation]
    ...  This keyword adds a customer to the order by searching it up by the first name.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Firstname` | The name of the Customer to be added | Default=Pieter | Optional |
    [Arguments]  ${Firstname}=Pieter
    POS_E_Basket.Click Add Customer
    POS_E_Customers.Search Customer  ${Firstname}


Remove Product From Order
    [Documentation]
    ...  This keyword removes a product from the order.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Product_Nr` | The list-number of the product that should be removed, counted from the top | 2 | Mandatory |
    [Arguments]  ${Product_Nr}
    POS_E_Checkout.Select Product       ${Product_Nr}
    POS_E_Basket.Click Minus
    POS_E_Barcode.Click Add To Basket   Update in basket

Add Same Product
    [Documentation]
    ...  This keyword will press the "+" sign of the given product to multiply the order
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `List_Element` | A numeric value which product in the list should be selected | 1,2,3 | Mandatory |
    ...                 | `Amount` | The list-number of the product that should be removed, counted from the top | 2 | Mandatory |
    [Arguments]  ${List_Element}  ${Amount}
    POS_E_Checkout.Select Product       ${List_Element}
    repeat keyword  ${Amount} times   POS_E_Basket.Click Plus
    POS_E_Barcode.Click Add To Basket   Update in basket


Change To Return Order
    [Documentation]
    ...  This keyword will change a normal sales order into a return order, when there is no previous order to be returned.\n\n
    ...  Functionally this is used when a product is returned with a receipt, but no order in the system.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `List_Element` | A numeric value which product in the list should be selected | Default=1 | Optional |
    [Arguments]  ${List_Element}=1
    POS_E_Checkout.Select Product           ${List_Element}
    POS_E_Basket.Select Return



Apply Price Modification
    [Documentation]
    ...  This keyword will add a price correction to a selected product
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The list-number of the product that should be removed, counted from the top | 2 | Mandatory |
    ...                 | `List_Element` | A numeric value which product in the list should be selected | Default=1 | Optional |
    [Arguments]  ${Amount}  ${List_Element}=1
    POS_E_Checkout.Select Product           ${List_Element}
    POS_E_Basket.Select More
    POS_E_Basket.Select Modify Price
    POS_E_Basket.Enter Alternative Price    ${Amount}
    POS_E_Basket.Select Correction Reason
    POS_E_Basket.Click Create Price Correction

