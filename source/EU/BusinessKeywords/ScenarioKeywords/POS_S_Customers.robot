*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Customers.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_More_Options.robot


*** Keywords ***
Create New Customer
    [Documentation]
    ...  This keyword creates a new Customer by filling all the detail fields. It will fill in the same content, but will
    ...  use a unique six-digit number for the email adress to make it a unique entry.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Firstname` | The type of discount given | variable amount, fixed amount, variable percentage, fixed percentage | Mandatory |
    ...                 | `Lastname` | The amount of discount to be given as percentage or amount money | 30 | Mandatory |
    ...                 | `Email` | A numeric value which product in the list should be selected | Default=1 | Mandatory |
    ...                 | `Phone` | The type of discount given | variable amount, fixed amount, variable percentage, fixed percentage | Mandatory |
    ...                 | `Adress` | The amount of discount to be given as percentage or amount money | 30 | Mandatory |
    ...                 | `Street` | The amount of discount to be given as percentage or amount money | 30 | Optional |
    ...                 | `Zipcode` | A numeric value which product in the list should be selected | Default=1 | Optional |
    ...                 | `Housenumber` | The amount of discount to be given as percentage or amount money | 30 | Optional |
    ...                 | `City` | A numeric value which product in the list should be selected | Default=1 | Optional |

    [Arguments]  ${Firstname}  ${Lastname}  ${Email}  ${Phone}
    ...          ${Adress}  ${Zipcode}  ${Street}=None  ${Housenumber}=None  ${City}=None

    POS_E_More_Options.Click On New Item        Title=Create customer
    POS_E_Customers.Enter Field Details 1       Field=FirstName         Argument=${Firstname}
    POS_E_Customers.Enter Field Details 1       Field=LastName          Argument=${Lastname}
    ${Number}=  evaluate                        random.randint(111111, 999999)
    POS_E_Customers.Enter Field Details 1       Field=EmailAddress      Argument=${Number}${Email}

    POS_E_Customers.Enter Field Details 2       Field=Phone number      Argument=${Phone}
    POS_E_Customers.Enter Field Details 3       Field=Search address    Argument=${Adress}  Zipcode=${Zipcode}
    run keyword unless  '${Street}'== 'None'    run keywords
    ...                                         POS_E_Customers.Enter Field Details 1
    ...                                         Field=Street            Argument=${Street}
    ...  AND                                    POS_E_Customers.Enter Field Details 1
    ...                                         Field=HouseNumber       Argument=${Housenumber}
    run keyword unless  '${City}'== 'None'       POS_E_Customers.Enter Field Details 1
    ...                                         Field=City              Argument=${City}
    POS_E_Customers.Click Create

