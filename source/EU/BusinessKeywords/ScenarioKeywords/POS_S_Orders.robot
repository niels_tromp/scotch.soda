*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Orders.robot
Resource   ${sourcedir}${/}AssertionKeywords${/}POS_A_Orders.robot

*** Keywords ***
Create Return
    [Documentation]
    ...  This keyword creates a return on an existing order. It will search for the order, enter a stock location,
    ...  return reason and remark before finishing the return.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | The order nummer of the order to be returned | 3022 | Mandatory |
    ...                 | `Stock_location` | The stock location of the returned position | Demo, Damaged, Sellable(Default) | Optional |
    ...                 | `Return_reason` | The reason of returning the product | Damaged, Too big(Default), Too small, I don't like it, Other | Optional |
    ...                 | `Remark` | An extra remark to add to the return | Default='Ik moet meer afvallen' | Optional |
    [Arguments]  ${Order_Nr}  ${Stock_location}=Sellable  ${Return_reason}=Too small  ${Remark}=Ik moet meer afvallen
                        POS_E_Orders.Select Orders Menu
                        POS_E_Orders.Select Order           ${Order_Nr}
                        POS_E_Orders.Select Create Return
                        POS_E_Orders.Enter Stock Location   ${Stock_location}
                        POS_E_Orders.Enter Return Reason    ${Return_reason}
                        POS_E_Orders.Enter Remarks          ${Remark}
                        POS_E_Orders.Click Confirm Return
    ${Return_Order_Nr}  POS_A_Orders.Assert Return Order Number
    [Return]  ${Return_Order_Nr}

