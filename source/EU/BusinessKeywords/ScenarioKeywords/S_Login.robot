*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}E_Login.robot
*** Keywords ***

Login
	[Documentation]  This keyword opens the browser and logs into the POS or ADMIN, based on the given parameters.
	...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Domain` | The domain that should be logged into. | POS or ADMIN | Mandatory |
    ...                 | `Organization` | The organization/store to be logged into | AT023 | Optional |
    ...                 | `Station` | A boolean which determines if the station can be selected or not. With a closed FP this is not possible. Default=True | True/False | Optional |
    ...                 | `Title` | The account to be logged in with. Special argument for employee discount | POS, AUT_FH_POS | Optional |

	[Arguments]  ${Domain}  ${Organization}=${EMPTY}  ${Station}=${True}  ${Title}=POS
	run keyword if  '${Domain}'== 'POS'      Login to POS    ${Domain}  ${Organization}  ${Station}  ${Title}
	...    ELSE IF  '${Domain}'== 'ADMIN'    Login to ADMIN  ${Domain}  ${Title}
	...    ELSE      Fail  msg=Given Domain is not known, please use 'POS' or 'ADMIN'


Login to POS
    [Documentation]
    ...  This keyword is called by 'Login' and will call the right keyword to log into the POS environment
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Domain` | The domain that should be logged into. | POS | Mandatory |
    ...                 | `Organization` | The organization/store to be logged into |AT001 | Mandatory |
    ...                 | `Station` | A boolean which determines if the station can be selected or not. With a closed FP this is not possible. Default=True | True/False | Optional |
    ...                 | `Title` | The account to be logged in with. Special argument for employee discount | POS, AUT_FH_POS | Optional |
    [Arguments]  ${Domain}  ${Organization}  ${Station}  ${Title}
    E_Login.Open Browser            ${POS_URL}
    E_Login.Input POS Credentials   ${Title}
    E_Login.Click Login             ${Domain}
    E_Login.Select Organization     ${Domain}  ${Organization}
    run keyword if  ${Station}  E_Login.Select Station on POS

Login to ADMIN
    [Documentation]
    ...   This keyword is called by 'Login' and will call the right keyword to log into the ADMIN environment
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Domain` | The domain that should be logged into. | ADMIN | Mandatory |
    ...                 | `Title` | The account to be logged in with. Special argument for employee discount | POS, AUT_FH_POS | Optional |
    [Arguments]  ${Domain}  ${Title}=ADMIN
    E_Login.Open Browser                ${ADMIN_URL}
    E_Login.Input ADMIN Credentials     ${Title}
    E_Login.Click Login                 ${Domain}


