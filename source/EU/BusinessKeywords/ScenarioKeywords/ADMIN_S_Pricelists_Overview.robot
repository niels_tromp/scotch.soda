*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Pricelists_Overview.robot


*** Keywords ***
Upload New Pricelist
    [Documentation]
    ...  This keyword Uploads a excel file with the pricelist used for the regression set
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Pricelist_Name | The pricelist name | SP_USA | Mandatory |
    ...  | Filepath | path of the pricelist | ${testdir}${/}input${/}Pricelist.csv | Mandatory |
    [Arguments]  ${Pricelist_Name}  ${Filepath}
    ADMIN_E_Pricelists_Overview.Insert Pricelist Name    ${Pricelist_Name}
    ADMIN_E_Pricelists_Overview.Click Edit Pricelist
    ADMIN_E_Pricelists_Overview.Upload New Pricelist     #${Filepath}
