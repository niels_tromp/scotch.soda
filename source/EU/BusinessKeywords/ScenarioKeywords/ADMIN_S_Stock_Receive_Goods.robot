*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Stock_Receive_Goods.robot

*** Keywords ***
Select Stock Receive Goods
    [Documentation]
    ...  this keyword receives goods for the store where it is send. This keyword is part of the Interbranch-flow.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Order_Nr | The order number of the sales order | 34556 | Mandatory |
    [Arguments]  ${Order_Nr}
    ADMIN_E_Stock_Receive_Goods.Click Fully Receive  ${Order_Nr}
    ADMIN_E_Stock_Receive_Goods.
