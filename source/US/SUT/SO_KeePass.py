
""" This library expects that the Keepass
    file is organized as follows:

    | DOMAIN | |
    | | ENVIRONMENT1 |
    | | ENVIRONMENT2 |
    | | ... |
    | DOMAIN2 |
    | | ENVIRONMENTx |
    | | ENVIRONMENTy |
    | | ... |

    Domain stands for the domain an environment is located in.
    Environments are unique and can contain one or more Generis Domains (currently not used)
    """

from pykeepass import PyKeePass
from KeePassLibrary import KeePassLibrary  # Yes, we are using the Robot KeePass library ;)


def get_entry_by_environment (keePassFile, keePassKey, title):
    """
    This function retrieves the username and password for a given title and environment.

    *Parameters*
    | =Parameter= | =Description= | =Mandatory or Optional= |
    | ``keePassFile`` | The file that contains the keepass database | Mandatory |
    | ``keePassKey`` | The file that contains the key to logon to the keepass database | Mandatory |
    | ``title`` | The key we are looking for in the keepass database | Mandatory |
    | ``environment`` | Within the keepass database everything is organized per environment. | Mandatory |
    |  | This parameter defines the environment we need to look at | |
    | ``domein`` | The domain to look in. If not given it is None | Optional (Default=None) |
    | =Returnvalue available?= | =Description= |
    | ``Yes`` | The username and password belonging to the title for the given environment |
    |  | None if title is not present |
    """



    keePair = None  # Set the default return value
    keePass = KeePassLibrary()  # Create an instance of the KeePassLibrary class

    keePass.load_database(keePassFile, None, keePassKey, None)  # Open the keepass database for reading

    # We now retrieve all entries. Unfortunately, the routine to retrieve all keys within a path is not working...
    # So we now retrieve all keys with the given title and look for the one we need...
    keyList = keePass.get_entries_by_title(title)    # Get all entries that match the title
    keePath = ""
    # We look for a title in a specific path. So we are going to add that to the keePath as well.
    # Otherwise we could get the wrong key back
    keePath += title

    # Now browse to the list until we found the one
    for keePassEntry in keyList:
        if keePassEntry.path.find(keePath) == 0:  # Found it!
            keePair = keePassEntry                # set the return value
            break                                 # end the loop

    keePass.close_database()  # Close the keepass database
    return keePair

