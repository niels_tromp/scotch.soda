*** Settings ***
Library     ${sourcedir}${/}SUT${/}SO_DF.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Cookbook_Events.robot

*** Variables ***
${Folder}=                      ${testdir}${/}results${/}evidence${/}
${xpath_Status_sub_column}=     count(//th//span[contains(text(),'Status')]/preceding::th)+1]
${xpath_Account_sub_column}=    count(//tr//th[contains(text(),'Account')]/preceding-sibling::th)+1]
${xpath_Status_column}=         //cookbook-financial-events-overview//td[position()=${xpath_Status_sub_column}
${xpath_Order_column}=          //cookbook-financial-events-overview//td[position()=count(//th//span[contains(text(),'Order')]/preceding::th)+1]
${xpath_Account_column}=        //order-bookings//table//tbody/tr/td[position()=count(//thead//th[text()='Account']/preceding-sibling::th)+1]
${xpath_Order_Remark}=          //order-detail-summary//div//p[text()='Receipt remark']/following-sibling::p[text()='{}']

*** Keywords ***
Check amount of rows
    [Documentation]
    ...  This keyword checks in the order number column on the screen 'Cookbook>Events'how many records are shown and
    ...  return this amount.
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `RC` | int | the amount of rows found in the table | 4 |
    ${RC}=      Get element count   ${xpath_Order_column}
    [Return]    ${RC}


Check Transaction Status
    [Documentation]
    ...  This keyword checks all the transaction statuses on the screen 'Cookbook>Events' of the selected transaction in the 'Status column' in the table.
    ...  All those statuses should be equal to the given status as argument.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Status` | The expected status to be found in the table | Processed | Mandatory |
    [Arguments]  ${Status}
                        SeleniumLibrary.Wait until element Is Visible   ${xpath_Status_column}
    ${xpath_Status}=    Get element count               ${xpath_Status_column}
    FOR  ${RC}  IN RANGE   1   ${xpath_Status}
        ${GUI_Status}   SeleniumLibrary.Get Text    //cookbook-financial-events-overview//tbody//tr[${RC}]/td[position()=${xpath_Status_sub_column}
                        should be equal             ${GUI_Status}   ${Status}
    ...                 msg=Mismatch in status found for row ${RC}: '${GUI_Status}' was found where '${Status}' was expected
    END


Verify Booking Overview
    [Documentation]
    ...  This keyword will check if the booking overview account names and values correspond with the expected value,
    ...  given as input-csv. First it will convert the given csv into a dataframe.\n Secondly combine it with the
    ...  dataframe which is created from the data in the GUI. \n Thirdly it will check if the account names and values
    ...  from the GUI correspond with the expected values by comparing the two columns in the combined dataframe.
    ...  When there are mismatches, it will output two lists with rows where there is a mismatch for the account name
    ...  or value. There will be an error thrown in which row the mismatch is found.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Expected_Booking` | A csv with the expected account names and booking values | ${testdir}${/}input${/}expected booking.csv | Mandatory |
    ...                 | `GUI_Booking` | A dataframe containing the data found in the GUI | `df_object` | Mandatory |
    [Arguments]  ${Expected_Booking}  ${GUI_Booking}
                                            SeleniumLibrary.Scroll Element Into View    //order-bookings//tbody//tr[last()]
    ${Expected_Booking_DF}                  SO_DF.Create_DF_From_CSV                    ${Expected_Booking}
    ${Combined_DF}                          SO_DF.Combine DFs                           ${Expected_Booking_DF}  ${GUI_Booking}
#                                            SO_DF.Write_To_Csv                          df=${Combined_DF}  folder=${Folder}  filename=combined_df.csv
    ${wrong_accounts}  ${wrong_amounts}     SO_DF.Verify Account And Amount             ${Combined_DF}
                                            ADMIN_E_Cookbook_Events.Throw Error         ${wrong_accounts}  ${Combined_DF}
                                            ADMIN_E_Cookbook_Events.Throw Error         ${wrong_amounts}   ${Combined_DF}


Verify Receipt Remark
    [Documentation]
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | The remark given in the POS that should be visible in the ADMIN | random text | Mandatory |
    [Arguments]  ${Remark}
    ${Order_Remark}=  Replace String  ${xpath_Order_Remark}  {}  ${Remark}
    SeleniumLibrary.Page Should Contain Element     ${Order_Remark}


Verify Empty Booking
    [Documentation]
    ...  This keyword will check if no bookings are available for the given order.
    SeleniumLibrary.Wait until element Is Visible   //td[text()='No bookings available']  5
    ...  error=The booking list was not visible or was filled


