*** Settings ***
Library     String
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot

*** Keywords ***
Assert Order Number
    [Documentation]
    ...  This keyword fetches the order number from the POS application after a payment is done.
    ...  It will return the found order number after the '#'
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Order_Nr` | String | The order number | 2344 |
                            SeleniumLibrary.Wait until element is visible   //ion-text/p[contains(text(),'Order')]  10
    ${Screen_Order_Nr}=     SeleniumLibrary.get text                        //ion-text/p[contains(text(),'Order')]
    ${Order_Nr}=            String.Fetch From Right                         string=${Screen_Order_Nr}    marker=#
    ${status}=              Run Keyword And Return Status                   variable should exist   ${Regression}
                            run keyword if  ${status}                       POS_E_Basket.Write Order_Nr to DF  ${Order_Nr}
    [Return]  ${Order_Nr}

Check Correct Tax
    [Documentation]
    ...  This keyword checks if the correct tax amount is displayed before purchasiing an item. It will check the
    ...  displayed amount with the given tax as argument.
    ...  It will return the found order number after the '#'
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Tax` | The expected tax of the payment | 8.88 | Mandatory |
    ...                 | `Row` | With special cases, multiple taxes are calculated. With this parameter you can decide which line should be checked | 1(default),2,3 | Optional |
    [Arguments]  ${Tax}  ${Row}=1
                    SeleniumLibrary.Wait until element is visible   //checkout-totals//ion-col[contains(text(),'TAX -')]  10
    ${GUI_Tax}=     SeleniumLibrary.get text                        (//checkout-totals//ion-col[contains(text(),'TAX -')])[${Row}]
    ${GUI_Tax}=     String.Fetch From Right                         string=${GUI_Tax}    marker=-${SPACE}
                    should be equal                                 ${Tax}  ${GUI_Tax}
    ...             msg=Mismatch in shown tax: '${GUI_Tax}' was found where '${Tax}' was expected