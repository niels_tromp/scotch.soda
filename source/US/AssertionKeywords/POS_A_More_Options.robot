*** Settings ***
Library     String
Library     ${sourcedir}${/}SUT${/}SO_Datetime.py
*** Keywords ***
Check Move Cash Entry
    [Documentation]
    ...  This keyword checks if a given entry is visible in the GUI. It will use the arguments to check the table for the
    ...  entry based upon the from- and to- stations, amount and the current datetime. For the datetime it will format
    ...  the current datetime without a zero-padding for the month, day and hour.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `From` | The account name from which money should be moved | Safe/Station | Mandatory |
    ...                 | `To` | The account name where the money should be moved to | Safe/Station | Mandatory |
    ...                 | `Amount` | The amount of 10 dollars should be given back | 3 | Mandatory |
    [Arguments]  ${From}  ${To}  ${Amount}
                    SeleniumLibrary.Wait until element is visible   //ion-card-content//ion-row[2]/ion-col[1]  5
                    Sleep  0.5s  #To be sure the whole table has loaded
    ${GUI_Date}=    SeleniumLibrary.Get Text                        //ion-card-content//ion-row[2]/ion-col[1]
    ${Date}=        SO_Datetime.Get Current Datetime
    ${Date}=        SO_Datetime.Format datetime For MoveCash           ${Date}
                    Should Contain                  ${GUI_Date}  ${Date}
    ...                                             msg= Datetime in gui '${GUI_Date}' does not correspond with datetime of expected value'${Date}'
    SeleniumLibrary.Page Should Contain Element     //ion-card-content//ion-row[2]/ion-col[2]//h5[contains(text(),'${From}')]
    ...                                             message= Station 'From' does not correspond with expected value '${From}'
    SeleniumLibrary.Page Should Contain Element     //ion-card-content//ion-row[2]/ion-col[3]//h5[contains(text(),'${To}')]
    ...                                             message= Station 'To' does not correspond with expected value '${To}'
    SeleniumLibrary.Page Should Contain Element     //ion-card-content//ion-row[2]/ion-col[5]//span[contains(text(),'${Amount}')]
    ...                                             message= Amount does not correspond with expected value '${Amount}'


Check Sealbag Number
    [Documentation]
    ...  This keyword checks if the sealbag number is found in the table.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Sealbag_Nr` | The randomly generated six-digit selabag number | 3456433 | Mandatory |
    [Arguments]  ${Sealbag_Nr}
    SeleniumLibrary.Page Should Contain Element     //page-bank-deposits-list//ion-row/ion-col[text()='${Sealbag_Nr}']
    ...                                             message= Enty with sealbag nr `${Sealbag_Nr}` is not visible in the table


Check Station State
    [Documentation]
    ...  This keyword checks in what state the cash drawer  or safe is. By giving in the arguments `${Station}`  and
    ...  `${State}` it will check the given station for the given state it's in.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Station` | The station you want to check the state from | Cash draweer/Safe | Mandatory |
    ...                 | `State` | The state the station should be in | Open/Closed | Mandatory |
    [Arguments]  ${Station}  ${State}
    ${State_U}=     String.Convert To Upper Case  ${State}
                    SeleniumLibrary.Wait until element is visible   //ion-col/span[text()='${Station}']  10
                    SeleniumLibrary.Page Should Contain Element     //ion-col/span[text()='${Station}']/ancestor::ion-row//ion-badge[contains(text(),'${State_U}')]
    ...             message=${Station} is not ${State}


Verify Giftcard Balance
    [Documentation]
    ...  This keyword will verify the displayed giftcard balance with the given giftcard balance
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Giftcard_Amount` | The amount that should be on the giftcard | 1243 | Mandatory |
    [Arguments]  ${Giftcard_Amount}
    ${GUI_Value}=   SeleniumLibrary.Get Text    //div[contains(@class,'gift-card-balance')]//eva-price
    ${GUI_Value}=   String.Fetch From Right     string=${GUI_Value}    marker=$
                    Should be equal             ${GUI_Value}   ${Giftcard_Amount}.00
