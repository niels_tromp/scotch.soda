*** Settings ***
Library    String

*** Keywords ***
Assert Return Order Number
    [Documentation]
    ...  This keyword fetches the order number fo the created return from the POS application after a return is created.
    ...  It will return the found order number after the '#'
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Order_Nr` | String | 2344 |
                            SeleniumLibrary.Wait until element is visible   //ion-toolbar/ion-title[text()='Order details ']/span  10
    ${Screen_Order_Nr}=     SeleniumLibrary.Get Text                        //ion-toolbar/ion-title[text()='Order details ']/span
    ${Order_Nr}=            String.Fetch From Right                         string=${Screen_Order_Nr}    marker=#
    ${status}=              Run Keyword And Return Status                   variable should exist   ${Regression}
                            run keyword if  ${status}                       POS_E_Basket.Write Order_Nr to DF  ${Order_Nr}
    [Return]  ${Order_Nr}

