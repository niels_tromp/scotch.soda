*** Settings ***
Library     ${sourcedir}${/}SUT${/}SO_DF.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot

*** Variables ***
${Folder}=          ${testdir}${/}results${/}evidence${/}

*** Keywords ***
Verify Bookkeeping
    [Documentation]
    ...  This keyword checks the cards containing the different bookings overviews. By giving in the "Station" and "Moment"
    ...  it will decide which amount(positive/negative) should be shown with the correct account. The keyword and
    ...  calculation only work when the financial period has been closed and opened and one cash transfer or modification being done.
    ...  When any type of sale is being done inbetween the opening and closing the station, this keyword will not perform as designed.
    ...  The following cases are used: \n\n - Opening and closing variances \n\n - Income and Expense \n\n - Bank deposit
    ...  \n\n - Cash limit
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Station` | The station that should be checked | Sation/Safe | Mandatory |
    ...                 | `Moment` | The moment a dfference is created in counting the station/safe | Opening/Closing | Mandatory |
    ...                 | `Amount` | The amount of the difference that was created in total amount of dollars | 10.00 | Mandatory |
    [Arguments]  ${Station}  ${Moment}  ${Amount}
    ADMIN_S_MainKeywords.Take Screenshot        Cash difference
    ${status}=  Run Keyword And Return Status       variable should exist                   ${Regression}
                run keyword if  ${status}           ADMIN_S_MainKeywords.Take Screenshot    Finance_Overview  ${Folder}${/}Screenshots${/}${TEST NAME}
    # Cash Difference Opening/Closing
    run keyword if  '${Station}'=='Station' and '${Moment}'=='Opening'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash difference')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Cash difference' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash in hand')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Cash in hand' is not present

    run keyword if  '${Station}'=='Station' and '${Moment}'=='Closing'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash difference')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Cash difference' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash in hand')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Cash in hand' is not present

    run keyword if  '${Station}'=='Safe' and '${Moment}'=='Opening'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'cash-safe')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Cash-safe' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash difference')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Cash difference' is not present
    # Income
    run keyword if  '${Station}'=='Station' and '${Moment}'=='Income'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Suspense cash-safe')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Suspense cash-safe' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Suspense cash transport')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Suspense cash transport' is not present
    # Expense
    run keyword if  '${Station}'=='Station' and '${Moment}'=='Expense'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Cash in hand')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Cash in hand' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Other payables')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Other payables' is not present
    # Bankdeposit
    run keyword if  '${Station}'=='Safe' and '${Moment}'=='BankDeposit'  run keywords
    ...         SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Suspense cash-safe')]/../../div//span[text()='$-${Amount}']
    ...                                                         message=Card with 'Suspense Cash-safe' is not present
    ...  AND    SeleniumLibrary.Page Should Contain Element     //h3[contains(text(),'Suspense cash transport')]/../../div//span[text()='$${Amount}']
    ...                                                         message=Card with 'Cash transport' is not present

