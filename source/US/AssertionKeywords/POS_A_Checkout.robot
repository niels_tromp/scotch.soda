*** Settings ***
Library    String

*** Variables ***
${btn_Order_Option}=    //checkout-tile/ion-button//span[text()=' {} ']/ancestor::checkout-tile
${btn_Payment_Method}=  //checkout-tile/ion-button//span[text()='{}']/ancestor::checkout-tile

*** Keywords ***
Verify All Buttons
    [Documentation]
    ...  This keyword checks on the checkout screen if all buttons for the 'Order' options and 'Payment methods' are
    ...  available and functional
    SeleniumLibrary.Wait until element is visible   //checkout-tile/ion-button//span[text()='Cash']/ancestor::checkout-tile  10

    ${btn_Order_Remark}=        Replace String  ${btn_Order_Option}  {}  Order remark
    ${btn_Partial_Shipment}=    Replace String  ${btn_Order_Option}  {}  Partial shipment
    ${btn_Tax_Exempt}=          Replace String  ${btn_Order_Option}  {}  Tax exempt
    ${btn_Sold_By}=             Replace String  ${btn_Order_Option}  {}  Sold By

    ${btn_Cash}=                Replace String  ${btn_Payment_Method}  {}  Cash
    ${btn_Card}=                Replace String  ${btn_Payment_Method}  {}  Card
    ${btn_Giftcard}=            Replace String  ${btn_Payment_Method}  {}  Giftcard
    ${btn_Manual_EFT}=          Replace String  ${btn_Payment_Method}  {}  Manual EFT
    ${btn_Scotch_EGC}=          Replace String  ${btn_Payment_Method}  {}  Scotch EGC
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Order_Remark}
    ...                                     message=Order remark button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Partial_Shipment}
    ...                                     message=Partial Shipment button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Tax_Exempt}
    ...                                     message=Tax exempt button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Sold_By}
    ...                                     message=Sold by button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Cash}
    ...                                     message=Cash payment button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Card}
    ...                                     message=Card payment button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Giftcard}
    ...                                     message=Giftcard payment button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Manual_EFT}
    ...                                     message=Manual EFT payment button is missing
    run keyword and continue on failure     SeleniumLibrary.Page Should Contain Element  ${btn_Scotch_EGC}
    ...                                     message=Scotch EGC payment button is missing

