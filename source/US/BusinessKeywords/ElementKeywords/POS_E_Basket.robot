*** Settings ***
Library     ${sourcedir}${/}SUT${/}SO_DF.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_MainKeywords.robot

*** Variables ***
${btn_basket}=      //ion-icon[contains(@name,'cart')]
${btn_checkout}=    //sales-panel//ion-button[contains(text(),'Checkout')]
${btn_discount}=    //eva-top-buttons//ion-icon[@ng-reflect-name='pricetag']
${btn_coupons}=     //h2[text()='Manual discounts']/ancestor::ion-toolbar//ion-icon
${btn_Minus}=       //quantity-controls//ion-button/ion-icon[@aria-label='remove']/ancestor::ion-button

*** Keywords ***
Select Basket Menu
    SeleniumLibrary.Wait until element is visible   ${btn_basket}
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_basket}
    ...  AND    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Basket ']  5


Select Checkout
    SeleniumLibrary.Wait until element is visible   ${btn_checkout}  10
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_checkout}
    ...  AND    run keyword and return status  POS_S_Mainkeywords.Wait for Tax Calculation
    ...  AND    SeleniumLibrary.Wait until element is visible   //h4[contains(text(),'Order')]  10


Click On Proceed
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   //checkout-panel/ion-footer/ion-button
    ...  AND    SeleniumLibrary.Wait until element is visible   //ion-text/p[contains(text(),'Order')]  5


Click On Discount
    SeleniumLibrary.Wait until element is visible   ${btn_discount}  5
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_discount}
    ...  AND    SeleniumLibrary.Wait until element is visible   //h2[text()='Manual discounts']  5


Click On Coupons
    SeleniumLibrary.Wait until element is visible   ${btn_coupons}  5
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   ${btn_coupons}
    ...  AND    SeleniumLibrary.Wait until element is visible   //h2[text()='Search for coupons']  5


Enter Coupon Code
    [Arguments]  ${Coupon_Code}
    SeleniumLibrary.Input Text          //input[@placeholder='Coupon code']     ${Coupon_Code}
    SeleniumLibrary.Click Element       //h2[text()='Search for coupons']


Click Apply Coupon
    SeleniumLibrary.Wait Until Element Is Enabled   //ion-button[text()=' Apply coupon ']  5
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   //ion-button[text()=' Apply coupon ']
    ...  AND    SeleniumLibrary.Wait until element is visible   //h6[text()='Coupon discount']  5


Click Print Receipt
    SeleniumLibrary.Wait until element is visible   //ion-button[contains(text(),'Print receipt')]  5
    SeleniumLibrary.Click Element                   //ion-button[contains(text(),'Print receipt')]
    SeleniumLibrary.Wait until element is visible   //eva-empty-state  10


Enter Giftcard Number
    [Arguments]  ${Number}
    SeleniumLibrary.Wait until element is visible   //ion-input[@ng-reflect-name='serialnumber']    10
    SeleniumLibrary.Input Text                      //ion-input[@ng-reflect-name='serialnumber']/input    ${Number}
    SeleniumLibrary.Click Element                   //h6[text()='Modify price']


Enter Giftcard Amount
    [Arguments]  ${Amount}
    SeleniumLibrary.Wait until element is visible   //ion-input[@ng-reflect-name='amount']  5
    SeleniumLibrary.Input Text                      //ion-input[@ng-reflect-name='amount']/input    ${Amount}
    SeleniumLibrary.Click Element                   //h6[text()='Modify price']


Click Add Customer
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Add customer ']  5
    SeleniumLibrary.Click Element                   //ion-button[text()=' Add customer ']
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Customers ']  5


Click Minus
    SeleniumLibrary.Wait until element is visible   //span[contains(text(),'Colours')]  5
    wait until keyword succeeds  2x  2s             SeleniumLibrary.Click Element   ${btn_Minus}


Select Carry Out
    SeleniumLibrary.Wait until element is visible   //div[contains(text(),'Carry out')]/ancestor::button  5
    SeleniumLibrary.Click Element                   //div[contains(text(),'Carry out')]/ancestor::button


Select Delivery
    SeleniumLibrary.Wait until element is visible   //p[text()='Select type']  5
    SeleniumLibrary.Click Element                   //ion-icon[contains(@src,'ship')]/ancestor::ion-button
    SeleniumLibrary.Wait until element is visible   //ion-text[text()='Deliver ']  5
    POS_S_Mainkeywords.Wait For Order Type To Change


Write Order_Nr to DF
    [Arguments]  ${Order_Nr}
    ${df}=  SO_DF.Create_Regression_List_CSV    Arg1=${TEST NAME}  Arg2=${Order_Nr}
            SO_DF.Write_To_Csv  df=${df}  folder=${Folder}  filename=TC and Order_Nr.csv    mode=a