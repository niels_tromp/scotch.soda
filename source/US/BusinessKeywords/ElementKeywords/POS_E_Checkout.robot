*** Settings ***
Library     String
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_Mainkeywords.robot

*** Variables ***
${btn_Customers}=       //ion-icon[contains(@src,'person')]
${btn_Order_Option}=    //checkout-tile/ion-button//span[text()=' {} ']/ancestor::checkout-tile
${btn_Payment_Method}=  //checkout-tile/ion-button//span[text()='{}']/ancestor::checkout-tile

*** Keywords ***
Select Product
    [Arguments]  ${List_Element}
    SeleniumLibrary.Wait until element is visible   //ion-item-sliding//ion-item  10
    SeleniumLibrary.Click Element                   (//ion-list//ion-item/div)[${List_Element}]


Click On Discount Icon
    SeleniumLibrary.Wait until element is visible   //button[@data-test='discount']  10
    SeleniumLibrary.Click Element                   //button[@data-test='discount']


Select Discount
    [Arguments]  ${Type}  ${Amount}
    SeleniumLibrary.Wait until element is visible   //eva-discount-manual  10
    run keyword if  '${Type}'=='variable amount'    Select Variable Amount Discount  ${Amount}
    ...  ELSE IF  '${Type}'=='fixed amount'         Select Fixed Amount Discount
    ...  ELSE IF  '${Type}'=='variable percentage'  Select Variable Percentage Discount  ${Amount}
    ...  ELSE IF  '${Type}'=='fixed percentage'     Select Fixed Percentage Discount
    ...  ELSE  fail  msg=No valid type is given, please select the following: "variable amount, fixed amount,
    ...              variable percentage, fixed percentage"


Select Variable Amount Discount
    [Arguments]  ${Amount}
    wait until keyword succeeds  3x  0.5s   run keywords
    ...         SeleniumLibrary.Click Element       (//eva-discount-manual-line)[1]
    ...  AND    SeleniumLibrary.Input Text          //ion-input//input     ${Amount}
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create discount ']  10
    Apply Discount


Select Fixed Amount Discount
    SeleniumLibrary.Click Element                   (//eva-discount-manual-line)[2]
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create discount ']  10
    Apply Discount


Select Variable Percentage Discount
    [Arguments]  ${Amount}
    wait until keyword succeeds  3x  0.5s   run keywords
    ...         SeleniumLibrary.Click Element       (//eva-discount-manual-line)[3]
    ...  AND    SeleniumLibrary.Input Text          //ion-input//input     ${Amount}
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create discount ']  10
    Apply Discount


Select Fixed Percentage Discount
    SeleniumLibrary.Click Element                   (//eva-discount-manual-line)[4]
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create discount ']  10
    Apply Discount


Apply Discount
    wait until keyword succeeds  2x  0.5s   run keywords
    ...  SeleniumLibrary.Click Element              //ion-button[text()=' Create discount ']
    ...  AND  POS_S_Mainkeywords.Wait For Discount To Apply
    ...  AND  SeleniumLibrary.Wait until element is visible         //shopping-cart-line//ion-icon[@name='pricetag']  5



Add Big Bag
    [Arguments]  ${Bag}
    ${btn_Bag}=     Replace String  ${btn_Order_Option}  {}  Bag
    run keyword if  ${Bag}  run keywords
    ...                     SeleniumLibrary.Wait until element is visible   ${btn_Bag}  10
    ...  AND                SeleniumLibrary.Click Element                   ${btn_Bag}
    ...  AND                SeleniumLibrary.Wait until element is visible   //ion-item//h6[text()='Big Bag']  10


Click Proceed
    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Proceed ']  10
    SeleniumLibrary.Click Element                   //ion-button[text()=' Proceed ']
#    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Create discount ']  10


Select Order Remark
    ${btn_Order_Remark}=  Replace String            ${btn_Order_Option}  {}  Order remark
    SeleniumLibrary.Wait until element is visible   ${btn_Order_Remark}  5
    SeleniumLibrary.Click Element                   ${btn_Order_Remark}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Order remark ']


Select Partial Shipment
    ${btn_Partial_Shipment}=  Replace String        ${btn_Order_Option}  {}  Partial shipment
    SeleniumLibrary.Wait until element is visible   ${btn_Partial_Shipment}  5
    SeleniumLibrary.Click Element                   ${btn_Partial_Shipment}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Partial shipment ']


Select Tax Exempt
    ${btn_Tax_Exempt}=  Replace String              ${btn_Order_Option}  {}  Tax exempt
    SeleniumLibrary.Wait until element is visible   ${btn_Tax_Exempt}  5
    SeleniumLibrary.Click Element                   ${btn_Tax_Exempt}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Tax exempt ']


Select Sold By
    ${btn_Sold_By}=  Replace String                 ${btn_Order_Option}  {}  Sold By
    SeleniumLibrary.Wait until element is visible   ${btn_Sold_By}  5
    SeleniumLibrary.Click Element                   ${btn_Sold_By}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()='Select a user']


Select Other Seller
    SeleniumLibrary.Wait until element is visible       //item-search//ion-content/div  5
    SeleniumLibrary.Click Element                       //item-search//ion-content/div
    SeleniumLibrary.Wait until element is not visible   //item-search//ion-content/div  5


Select Cash
    [Arguments]  ${Cash_Amount}=${EMPTY}
    ${btn_Cash}=  Replace String                    ${btn_Payment_Method}  {}  Cash
    SeleniumLibrary.Wait until element is visible   ${btn_Cash}  5
    Sleep  1.5s  # To be sure the correct 'Open amount' is displayed and no return popup will be shown
    SeleniumLibrary.Click Element                   ${btn_Cash}
    SeleniumLibrary.Wait until element is visible   //app-keyboard-numeric
    Sleep  0.5s
    run keyword unless  '${Cash_Amount}'=='${EMPTY}'  run keywords
    ...                  SeleniumLibrary.Click Element  //ion-input[@ng-reflect-name="amount"]
    ...  AND             SeleniumLibrary.Click Element  //app-keyboard-numeric//ion-button[contains(text(),'${Cash_Amount}')]
    SeleniumLibrary.Click Element                      //checkout-panel//h4[text()='Cash Details']
    SeleniumLibrary.Wait until element is visible      //ion-button[contains(text(),'with Cash')]  5


Select Card
    ${btn_Card}=  Replace String                    ${btn_Payment_Method}  {}  Card
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Wait until element is visible   ${btn_Card}  5
    ...  AND    Sleep  1.5s  # To be sure the correct 'Open amount' is displayed and no return popup will be shown
    ...  AND    SeleniumLibrary.Click Element                   ${btn_Card}
    ...  AND    SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Pay with Card ']  5


Select Giftcard
    ${btn_Giftcard}=  Replace String                ${btn_Payment_Method}  {}  Giftcard
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Wait until element is visible   ${btn_Giftcard}  5
    ...  AND    Sleep  1.5s  # To be sure the correct 'Open amount' is displayed and no return popup will be shown
    ...  AND    SeleniumLibrary.Click Element                   ${btn_Giftcard}
    ...  AND    SeleniumLibrary.Wait until element is visible   //ion-content/div/h4[text()='Giftcard Details']  5


Select Manual EFT
    ${btn_Manual_EFT}=  Replace String              ${btn_Payment_Method}  {}  Manual EFT
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Wait until element is visible   ${btn_Manual_EFT}  5
    ...  AND    Sleep  1.5s  # To be sure the correct 'Open amount' is displayed and no return popup will be shown
    ...  AND    SeleniumLibrary.Click Element                   ${btn_Manual_EFT}
    ...  AND    SeleniumLibrary.Wait until element is visible   //h4[text()='Manual EFT Details']


Select Scotch EGC
    ${btn_Scotch_EGC}=  Replace String              ${btn_Payment_Method}  {}  Scotch EGC
    wait until keyword succeeds  2x  0.5s  run keywords
    ...         SeleniumLibrary.Wait until element is visible   ${btn_Scotch_EGC}  5
    ...  AND    Sleep  1.5s  # To be sure the correct 'Open amount' is displayed and no return popup will be shown
    ...  AND    SeleniumLibrary.Click Element                   ${btn_Scotch_EGC}
    ...  AND    SeleniumLibrary.Wait until element is visible   //h4[text()='Scotch EGC Details']


Pay with Cash
    wait until keyword succeeds  3x  1s  SeleniumLibrary.Click Element      //ion-button[contains(text(),'with Cash')]
    POS_S_MainKeywords.Wait For Payment to process
    POS_S_MainKeywords.Wait For Order To Validate
    SeleniumLibrary.Wait until element Is Enabled                   //ion-text/p[contains(text(),'Order')]


Pay With Card
    SeleniumLibrary.Click Element                   //ion-button[text()=' Pay with Card ']
    SeleniumLibrary.Wait until element is visible       //h2[text()='Performing payment']  5
    SeleniumLibrary.Wait until element is not visible   //h2[text()='Performing payment']  30


Pay With Giftcard
    [Arguments]  ${Giftcard_Nr}
    ${btn_Giftcard}=    Replace String                                  ${btn_Payment_Method}  {}  Giftcard
                        SeleniumLibrary.Input Text                      //ion-input[@formcontrolname='CardNumber']//input  ${Giftcard_Nr}
                        SeleniumLibrary.Click Element                   ${btn_Giftcard}
                        SeleniumLibrary.Wait until element Is Enabled   //ion-button[text()=' Check balance ']  5
                        SeleniumLibrary.Click Element                   //ion-button[text()=' Check balance ']
#                        Handle Giftcard Return Popup                    Return=${False}
                        SeleniumLibrary.Wait until element is visible   //ion-button[text()=' Pay with Giftcard ']  5
                        SeleniumLibrary.Click Element                   //ion-button[text()=' Pay with Giftcard ']


Pay With Manual EFT
    SeleniumLibrary.Click Element  //ion-button[text()=' Pay with Manual EFT ']
    POS_S_MainKeywords.Wait For Payment to process


Pay With Scotch EGC
    SeleniumLibrary.Click Element                   //ion-button[text()=' Pay with Scotch EGC ']
    POS_S_MainKeywords.Wait For Payment to process


Handle Giftcard Return Popup
    [Arguments]  ${Return}
    SeleniumLibrary.Wait until element is visible  //ion-title[text()='Return amount']  5
    run keyword if      ${Return}    SeleniumLibrary.Click Element  //ion-button/span[text()='Yes']
    run keyword unless  ${Return}    SeleniumLibrary.Click Element  //ion-button/span[text()='No']


Close Return popup
    [Arguments]  ${Popup}
    run keyword if  ${Popup}    run keywords
    ...                         SeleniumLibrary.Wait until element is visible       //return-amounts-modal//ion-footer/ion-button  10
    ...  AND                    SeleniumLibrary.Click Element                       //return-amounts-modal//ion-footer/ion-button
    ...  AND                    SeleniumLibrary.Wait until element is not visible   //return-amounts-modal//ion-footer/ion-button  5


Input Order Remark
    [Arguments]  ${Remark}
    SeleniumLibrary.Input Text                      //ion-textarea//textarea  ${Remark}
    SeleniumLibrary.Click Element                   //ion-button[text()=' Submit']
    SeleniumLibrary.Wait until element is visible   //ion-col[text()=' Subtotal ']  5


Select Tax Exempt Reason
    [Arguments]  ${Tax_Exempt}
                    SeleniumLibrary.Wait until element is visible       //ion-select  5
                    SeleniumLibrary.Click Element                       //ion-select
                    SeleniumLibrary.Wait until element is visible       //h2[text()='Tax exempt reason']  5
    ${Tax_Exempt}   String.Convert To Upper Case                        ${Tax_Exempt}
                    SeleniumLibrary.Click Element                       //div[contains(text(),'${Tax_Exempt}')]
                    SeleniumLibrary.Wait until element is visible       //div[contains(text(),'${Tax_Exempt}')]/ancestor::button[@aria-checked='true']  5
                    SeleniumLibrary.Click Element                       //button/span[text()='OK']/..
                    SeleniumLibrary.Wait until element is not visible   //h2[text()='Tax exempt reason']  5


Click Submit
    SeleniumLibrary.Click Element                   //ion-button[text()=' Submit']
    SeleniumLibrary.Wait until element is visible   //checkout-totals  5