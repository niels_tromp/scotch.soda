*** Settings ***
Library     Collections
Library     String

*** Keywords ***
Enter Search Cirteria
    [Documentation]
    ...  This keyword enters the order number or Fianacial period nr in the screen Finance>General Ledgers. It will first wait until the table is visible,
    ...  checks how many rows are present before and after the order number is filled in to be sure the page updated
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Number` | The order number or financial period number | 2344 | Mandatory |
    ...                 | `Column` | The  input field where the number should be added | ProcessID/FinancialPeriodID | Mandatory |
    [Arguments]  ${Number}  ${Column}
                SeleniumLibrary.Wait until element Is Visible   //general-ledgers-overview//tbody//tr  5
                sleep  0.5s
    ${Before}   Get Element Count               //general-ledgers-overview//tbody//tr
                SeleniumLibrary.Input Text      //general-ledgers-overview//input[@name='${Column}']  ${Number}
                sleep  1
    ${After}    Get Element Count               //general-ledgers-overview//tbody//tr
                return from keyword if          ${Before} < ${After}








