*** Settings ***
Library     Collections
Library     String
#Library     AutoItLibrary

*** Variables ***
${Name_column}=    //price-lists-overview//thead/tr[2]/th[position() = count(//thead/tr/th//span[contains(text(),'Name')]/preceding::th)+1]//input

${Folder}=          ${testdir}${/}results${/}evidence${/}
*** Keywords ***
Insert Pricelist Name
    [Arguments]  ${Pricelist_Name}
    SeleniumLibrary.Wait until element is visible   //price-lists-overview//tr[10]  5
    SeleniumLibrary.Input Text                      ${Name_column}  ${Pricelist_Name}
    SeleniumLibrary.Wait until element is visible   //price-lists-overview//tr/td[contains(text(),'${Pricelist_Name}')]


Click Edit Pricelist
    SeleniumLibrary.Click Element                   //price-lists-overview//tr/td[7]/a
    SeleniumLibrary.Wait until element is visible   //div/div[contains(text(),'SP_USA')]  10
    SeleniumLibrary.Click Element                   //button//span[text()='Show details']
    SeleniumLibrary.Wait until element is visible   //eva-file-upload  5

Upload New Pricelist
    [Arguments]  ${Filepath}=C:${/}Scotch_Soda${/}setup${/}Product_Prices${/}input${/}x.txt
    SeleniumLibrary.Wait until element is visible   //table//tr[6]  5
    SeleniumLibrary.Scroll Element Into View        //eva-file-upload
    SeleniumLibrary.Click Element                   //eva-file-upload//label
#    sleep    2s
#    SeleniumLibrary.Choose File                     locator=//eva-file-upload//label/span    file_path=${Filepath}

#    sleep           2s
    Send            ${Filepath}   # This sends the file path to the entery field where the cursor is focused
    sleep           3s
    Control Click  strTitle=Open  strText=Open  strControl=1  strButton=Button1  nNumClicks=1
    # In some cases some parameters cannot be identified easily so yo might just use only the buttong name as the following
    # Control Click   ${EMPTY}   ${EMPTY}    Button1    ${EMPTY}    1











