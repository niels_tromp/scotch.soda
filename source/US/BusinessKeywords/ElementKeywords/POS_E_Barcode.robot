*** Variables ***
${btn_Barcode}=     //ion-icon[contains(@src,'scanner')]
${btn_Cancel}=      //button/span[text()='Cancel']

*** Keywords ***
Select Barcode Menu
    SeleniumLibrary.Wait until element is visible   ${btn_Barcode}
    wait until keyword succeeds     3x  1s  run keywords
    ...         SeleniumLibrary.Click Element      ${btn_Barcode}
    ...  AND    SeleniumLibrary.Wait until element is visible   //h2[text()='Barcode']  10

Cancel Barcode Scanner
    SeleniumLibrary.Wait until element is visible   ${btn_Cancel}
    SeleniumLibrary.Click Element                   ${btn_Cancel}

Enter Barcode
    [Arguments]  ${Barcode}  ${Product_Code}
    SeleniumLibrary.Input Text                      //input[@placeholder='BARCODE']     ${Barcode}
    Sleep  0.5s  # to be sure the barcode is processed
    SeleniumLibrary.Click Element                   //button/span[text()='Ok']
    SeleniumLibrary.Wait until element is visible   //ion-button//span[contains(text(),'Add to basket')]  20
    ...                                             error=Adding to basket failed
    POS_E_Barcode.Click Add To Basket
    POS_E_Barcode.Check If Product Is Added         ${Product_Code}

Click Add To Basket
    [Arguments]  ${btn_Name}=Add to basket
    wait until keyword succeeds  2x  1s  run keywords
    ...         SeleniumLibrary.Click Element                   //ion-button//span[contains(text(),'${btn_Name}')]
    ...  AND    SeleniumLibrary.Wait until element is visible   //p[contains(text(),'Summary')]  15


Check If Product Is Added
    [Arguments]  ${Product_Code}
    SeleniumLibrary.Wait until element is visible   //ion-item//ion-text/p[contains(text(),'${Product_Code}')]  20
    ...                                             error=Error, Product is not shown in basket or clicking on the button failed
    SeleniumLibrary.Wait until element is visible   //ion-item//div[contains(@class,'control') and text()='1']  10
    ...                                             error=Two products were added



