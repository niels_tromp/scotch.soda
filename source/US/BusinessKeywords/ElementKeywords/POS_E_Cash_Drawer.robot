*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_MainKeywords.robot
*** Variables ***
${btn_Cash_Drawer}=  //ion-icon[contains(@src,'eject')]

*** Keywords ***
Select Cash Drawer Menu
    SeleniumLibrary.Click Element       ${btn_Cash_Drawer}
    POS_S_MainKeywords.Wait For Drawer Alert To Close