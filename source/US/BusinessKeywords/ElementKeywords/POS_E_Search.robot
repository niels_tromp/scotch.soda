*** Variables ***
${btn_Search}=  //ion-icon[contains(@aria-label,'search')]

*** Keywords ***
Select Search Menu
    SeleniumLibrary.Click Element       ${btn_Search}
    SeleniumLibrary.Wait until element is visible       //ion-title[text()=' Search products ']  5


Split Product Code
    [Arguments]  ${Product_Code}
    ${Short_Product_Code}=    String.Fetch From Left     string=${Product_Code}    marker=-
    [Return]  ${Short_Product_Code}


Enter Text Into Searchbar
    [Arguments]  ${Product_Name}
    SeleniumLibrary.Wait until element is visible   //eva-product-search/ion-item   5
    SeleniumLibrary.Input Text      //eva-product-search//input     ${Product_Name}


Select Product
    [Arguments]  ${Product_Code}
    SeleniumLibrary.Wait until element is visible   //ion-card//span/p[contains(text(),'${Product_Code}')]  10
    ...                                             error=Loading search results unsuccesful
    SeleniumLibrary.Scroll Element Into View        //ion-card//span/p[contains(text(),'${Product_Code}')]
    SeleniumLibrary.Click Element                   //ion-card//p[text()='${Product_Code}']/ancestor::ion-card


Select Size
    [Arguments]  ${Size}
    SeleniumLibrary.Wait until element is visible   //span[text()=' Colours ']  10
    SeleniumLibrary.Scroll Element Into View        (//ion-list[contains(@class,'product-sizes')]/ion-card)[last()]
    SeleniumLibrary.Click Element                   //ion-list/ion-card//h3[text()='${Size}']/..


Select Services
    SeleniumLibrary.Wait until element is visible   //button//div[contains(text(),'Services')]  5
    SeleniumLibrary.Click Element                   //button//div[contains(text(),'Services')]


Click On Big Bag
    SeleniumLibrary.Wait until element is visible   //span[contains(text(),'Shopping Bag')]  5
    SeleniumLibrary.Click Element                   //span[contains(text(),'Shopping Bag')]/ancestor::ion-card//ion-button