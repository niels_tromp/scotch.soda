*** Variables ***
${btn_Logout}=      //ion-icon[contains(@name,'exit-outline')]
${POS_element}=     //ion-button[text()='E-mail ']

*** Keywords ***
Logout
    SeleniumLibrary.Click Element       ${btn_Logout}
    SeleniumLibrary.Wait until element is visible       //ion-button/span[text()='Confirm']  5
    Sleep  0.2s
    SeleniumLibrary.Click Element       //ion-button/span[text()='Confirm']
    SeleniumLibrary.Wait until element is visible       ${POS_element}  5