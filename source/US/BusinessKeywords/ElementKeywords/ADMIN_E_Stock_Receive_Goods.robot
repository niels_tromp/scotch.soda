*** Settings ***
Library  Collections
Library  String
Library  ${sourcedir}${/}SUT${/}SO_DF.py

*** Variables ***
${xpath_btn_Shipment}=  //tbody/tr/td//div[contains(text(),'{}')]/../../../td[10]/button[contains(@ng-click,'Shipment')]
*** Keywords ***
Click Fully Receive
    [Arguments]  ${Order_Nr}
                            SeleniumLibrary.Wait until element is visible   //tbody/tr/td//div[contains(text(),'${Order_Nr}')]  5
    ${xpath_btn_Shipment}=  replace string                                  ${xpath_btn_Shipment}  {}  ${Order_Nr}
                            SeleniumLibrary.Click Element                   ${xpath_btn_Shipment}
                            SeleniumLibrary.Wait until element is visible   //h3[text()='Confirm full shipment']  5


Acknowledge Popup
    SeleniumLibrary.Wait until element is visible   //h3[text()='Confirm full shipment']  5
    SeleniumLibrary.Click Element                   //button[contains(@ng-click,'yes')]
    # Niet mee verder gegaan omdat 'Fully receive' niet werkte. Ik had de verkeerde datum
    # Goederen worden nu binnegehaald in het Purchace Orders menu






