*** Variables ***
${btn_Customers}=   //ion-icon[contains(@src,'person')]
${FieldInput_1}=    //ion-input[@formcontrolname='{}']/input
${FieldInput_2}=    //ion-input/input[@placeholder='{}']
${FieldInput_3}=    //ion-label[text()='{}']/following-sibling::ion-input/input

*** Keywords ***
Select Customers Menu
    SeleniumLibrary.Click Element                       ${btn_Customers}
    SeleniumLibrary.Wait until element is visible       //ion-title[text()=' Customers ']  5

Enter Field Details 1
    [Arguments]  ${Field}  ${Argument}
    ${Textfield}=   Replace String                  ${FieldInput_1}     {}  ${Field}
    SeleniumLibrary.Wait until element is visible   ${Textfield}        5
    SeleniumLibrary.Input Text                      ${Textfield}        ${Argument}

Enter Field Details 2
    [Arguments]  ${Field}  ${Argument}
    ${Textfield}=   Replace String                  ${FieldInput_2}     {}  ${Field}
    SeleniumLibrary.Wait until element is visible   ${Textfield}        5
    SeleniumLibrary.Input Text                      ${Textfield}        ${Argument}

Enter Field Details 3
    [Arguments]  ${Field}  ${Argument}  ${Zipcode}
    ${Textfield}=   Replace String                  ${FieldInput_3}     {}  ${Field}
    SeleniumLibrary.Scroll Element Into View        ${Textfield}
    SeleniumLibrary.Wait until element is visible   ${Textfield}        5
    SeleniumLibrary.Input Text                      ${Textfield}        ${Argument}
    Sleep  2s  # To let the autocomplete fill in the fields
    SeleniumLibrary.Click Element                   //ion-item//p
    SeleniumLibrary.Wait until element is visible   //ul/li[contains(text(),'${Argument}')]  5
    SeleniumLibrary.Click Element                   //ul/li[contains(text(),'${Argument}')]
    SeleniumLibrary.Wait until element is visible   //ion-item//p[contains(text(),'${Zipcode}')]  5
    SeleniumLibrary.Click Element                   //ion-item//p[contains(text(),'${Zipcode}')]

Click Create
    SeleniumLibrary.Click Element                   //ion-item//p
    SeleniumLibrary.Click Element                   //ion-button[text()=' Create']
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Customers ']


Search Customer
    [Arguments]  ${Customer}
    SeleniumLibrary.Wait until element is visible   //ion-title[text()=' Customers ']  5
    SeleniumLibrary.Input Text                      //ion-input/input[@placeholder='Search for customers']  ${Customer}
    SeleniumLibrary.Wait until element is visible   //div//app-customer-search-results-item//ion-card-title[contains(text(),'${Customer}')]  10
    ...                                             Error=Customer '${Customer}' does not exist
    SeleniumLibrary.Click Element                   //div//app-customer-search-results-item[1]//ion-button
    SeleniumLibrary.Wait until element is visible   //h3[contains(text(),'${Customer}')]