*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Finance_General_Ledgers.robot


*** Keywords ***
Search Ledgers
    [Documentation]
    ...  This keyword will enter the search ciriteria on the General Ledgers page
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Financial_Period` | The financial period number that should be searched for | 3545 | Mandatory |
    [Arguments]  ${Financial_Period}
    ADMIN_E_Finance_General_Ledgers.Enter Search Cirteria  ${Financial_Period}  Column=FinancialPeriodID
