*** Settings ***
Resource    ${sourcedir}${/}AssertionKeywords${/}POS_A_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_More_Options.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}POS_S_MainKeywords.robot

*** Keywords ***
Add Income Or Expense
    [Documentation]
    ...  This keyword creates a new entry for external income or expenses. Based on the `${Cost_Type}` it will
    ...  determine if it should be an income or expense.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Cost_Type` | The type of transaction that should be done | Income/Expense | Mandatory |
    ...                 | `Type` | The type of Income or Expense being done | Package materials, Windows(Expense Default), Free publicity ect | Optonal |
    ...                 | `Tax` | The amount of tax being applied | High(Default), Low, Zero, Exempt, Clothing | Optional |
    ...                 | `Amount` | The amount of incone or expense | 13=Default | Optional |
    ...                 | `Description` | A brief description of the income or expense | Broken shelfes | Optional |

    [Arguments]  ${Cost_Type}  ${Type}=Windows  ${Tax}=High  ${Amount}=13  ${Description}=Vogelpoep
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Select Income And Expense
    POS_E_More_Options.Click On New Item        Title=Add new income or expense
    run keyword if  '${Cost_Type}'=='Income'    Add Income      ${Type}  ${Tax}  ${Amount}  ${Description}
    ...  ELSE IF    '${Cost_Type}'=='Expense'   Add Expense     ${Type}  ${Tax}  ${Amount}  ${Description}


Add Income
    [Documentation]
    ...  This sub-keyword builds forth on "Add Income OR Expense" and will handle the flow for adding an Income
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Type` | The type of Income or Expense being done | Package materials, Windows(Expense Default), Free publicity ect | Optonal |
    ...                 | `Tax` | The amount of tax being applied | High(Default), Low, Zero, Exempt, Clothing | Optional |
    ...                 | `Amount` | The amount of incone or expense | 13=Default | Optional |
    ...                 | `Description` | A brief description of the income or expense | Broken shelfes | Optional |
    [Arguments]  ${Type}  ${Tax}  ${Amount}  ${Description}
    POS_E_More_Options.Select Income
    POS_E_More_Options.Enter Income/Expense Type    ${Type}
    POS_E_More_Options.Enter Tax                    ${Tax}
    POS_E_More_Options.Enter Amount                 ${Amount}
    POS_E_More_Options.Enter Description            ${Description}
    POS_E_More_Options.Select Station
    POS_E_More_Options.Click Add Income/Expense     ${Amount}  Income


Add Expense
    [Documentation]
    ...  This sub-keyword builds forth on "Add Income OR Expense" and will handle the flow for adding an Expense
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Type` | The type of Income or Expense being done | Package materials, Windows(Expense Default), Free publicity ect | Optonal |
    ...                 | `Tax` | The amount of tax being applied | High(Default), Low, Zero, Exempt, Clothing | Optional |
    ...                 | `Amount` | The amount of incone or expense | 13=Default | Optional |
    ...                 | `Description` | A brief description of the income or expense | Broken shelfes | Optional |
    [Arguments]  ${Type}  ${Tax}  ${Amount}  ${Description}
    POS_E_More_Options.Select Expense
    POS_E_More_Options.Enter Income/Expense Type    ${Type}
    POS_E_More_Options.Enter Tax                    ${Tax}
    POS_E_More_Options.Enter Amount                 ${Amount}
    POS_E_More_Options.Enter Description            ${Description}
    POS_E_More_Options.Select Station
    POS_E_More_Options.Click Add Income/Expense     ${Amount}  Expense


Move Cash
    [Documentation]
    ...  This keyword selects the station the money is moved from and to and will enter a multiply of 10 dollars
    ...  to be moved. It will also check if the move is done correctly by verifying the data in the table.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Source_Station` | The source statio the money is moved from | Station | Mandatory |
    ...                 | `Destination_Station` | The destionation of the moved money | Safe | Mandatory |
    ...                 | `Amount` | The amount of 10 dollars to be moved  | Default=1 | Optional |
    [Arguments]  ${Source_Station}  ${Destination_Station}  ${Amount}=1
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Select Move Cash
    POS_E_More_Options.Click On New Item        Title=Move cash
    POS_E_More_Options.Select Correct Station   Station_position=Source station       Station_Name=${Source_Station}
    POS_E_More_Options.Select Correct Station   Station_position=Destination station  Station_Name=${Destination_Station}
    POS_E_More_Options.Enter 10 Dollar          ${Amount}
    POS_E_More_Options.Click Move Cash
    POS_A_More_Options.Check Move Cash Entry    From=${Source_Station}  To=${Destination_Station}  Amount=${Amount}


Bank Deposits
    [Documentation]
    ...  This keyword moves cash from the safe to the bank via a sealbag. It will go to More Options> Bank deposit
    ...  fetch the amount of deposits available, create a new one, enter a multiply of 10 dollars based on `${Amount}`,
    ...  create a random six-digit sealbag number and creates a new deposit. By using the old amount of deposits, it
    ...  will check if that number is increased by one, to be sure the page has loaded and it can verify the deposit
    ...  has succeeded.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Source_Station` | The source statio the money is moved from | Station | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Sealbag_Number` | The random generated 6-digit sealbag number | Int | 234433 |
    [Arguments]  ${Amount}=1
                        POS_E_More_Options.Select More Options Menu
                        POS_E_More_Options.Select Bank Deposits
    ${Deposits_Before}  POS_E_More_Options.Check Amount of Deposits
                        POS_E_More_Options.Click On New Item                Title=Bank deposits
                        POS_E_More_Options.Enter 10 Dollar                  ${Amount}
    ${Sealbag_Number}=  POS_E_More_Options.Enter Sealbag Number
                        POS_E_More_Options.Click Add New Deposit
                        POS_E_More_Options.Check New Amount Of Deposits     ${Deposits_Before}
    [Return]            ${Sealbag_Number}

                                ################
                                #              #
                                # Cash Drawers #
                                #     Safe     #
                                #              #
                                ################



Open Cash Drawer Via GUI
    [Documentation]
    ...  This keyword opens the Cash Drawer in the More Options menu. When opening the cash drawer the amount should be given
    ...  which is the amount of 10 dollar bills the be counted. Optionally the arguments `Correction` and `Correction_Reason`
    ...  can be given when there is a difference in money when the cash drawer was closed.\n\n
    ...  Because it can not be certain if a count correction is needed, the keyword will be called, and will pass on to
    ...  the next keyword wether it fails or not. This will make it easier for the test to succeed and no manual setup is needed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The amount of 10 dollars to be counted in the cash drawer | 12 | Mandatory |
    ...                 | `Correction` | A boolean th signal if a correction needs to be done. Default=${True} | True/False | Optional |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Amount}  ${Correction}=${True}  ${Correction_Reason}=Surplus
    POS_E_More_Options.Select More Options Menu
    ${Closed}  run keyword and return status  POS_A_More_Options.Check Station State  Station=Cash drawer  State=closed
    run keyword unless  ${Closed}    fail  Cash Drawer was not closed
    run keyword if  ${Closed}  run keywords
    ...         POS_E_More_Options.Click On Cash Drawer
    ...  AND    POS_E_More_Options.Enter 10 Dollar          ${Amount}
    ...  AND    POS_E_More_Options.Click Open Station
    ...  AND    run keyword and return status  run keyword if  ${Correction}  POS_E_More_Options.Close Count Correction Popup  Open
    ...  AND    run keyword and return status  run keyword if  ${Correction}  Enter Count Corection  ${Correction_Reason}
    ...  AND    POS_A_More_Options.Check Station State  Station=Cash drawer  State=open


Open Cash Drawer Via Popup
    [Documentation]
    ...  This keyword opens the cash drawer when the popup is shown when opening the POS aaplication. For this the
    ...  financial period should be open. By giving in `${Correction}` with ${True} or ${False} will determine if a
    ...  count correction needs to be done with the given reason ${Correction_Reason}.\n\n
    ...  Because it can not be certain if a count correction is needed, the keyword will be called, and will pass on to
    ...  the next keyword wether it fails or not. This will make it easier for the test to succeed and no manual setup is needed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The amount of 10 dollars to be counted in the cash drawer | 12 | Mandatory |
    ...                 | `Correction` | A boolean th signal if a correction needs to be done. Default=${True} | True/False | Optional |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Amount}  ${Correction}=${True}  ${Correction_Reason}=Surplus
    POS_E_More_Options.Handle Open Cash Drawer Popup
    POS_E_More_Options.Enter 10 Dollar          ${Amount}
    POS_E_More_Options.Click Open Station
    run keyword and return status  run keyword if  ${Correction}   POS_E_More_Options.Close Count Correction Popup  Open
    run keyword and return status  run keyword if  ${Correction}   Enter Count Corection  ${Correction_Reason}
    POS_A_More_Options.Check Station State  Station=Cash drawer  State=open


Close Cash Drawer
    [Documentation]
    ...  This keyword closes the Cash Drawer in the More Options menu. When closing the cash drawer the amount should be given
    ...  which is the amount of 10 dollar bills the be counted. Optionally the arguments `Correction` and `Correction_Reason`
    ...  can be given when there is a difference in money when the cash drawer was opened.\n\n
    ...  Because it can not be certain if a count correction is needed, the keyword will be called, and will pass on to
    ...  the next keyword wether it fails or not. This will make it easier for the test to succeed and no manual setup is needed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The amount of 10 dollars to be counted in the cash drawer | 12 | Mandatory |
    ...                 | `Correction` | A boolean th signal if a correction needs to be done. Default=${True} | True/False | Optional |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Amount}  ${Correction}=${True}  ${Correction_Reason}=Surplus
    POS_E_More_Options.Select More Options Menu
    ${Open}  run keyword and return status  POS_A_More_Options.Check Station State  Station=Cash drawer  State=open
    run keyword unless  ${Open}    fail  Cash Drawer was not open
    run keyword if  ${Open}  run keywords
    ...         POS_E_More_Options.Click On Cash Drawer
    ...  AND    POS_E_More_Options.Enter 10 Dollar          ${Amount}
    ...  AND    POS_E_More_Options.Click Close Station
    ...  AND    run keyword and return status  run keyword if  ${Correction}  POS_E_More_Options.Close Count Correction Popup  Close
    ...  AND    run keyword and return status  run keyword if  ${Correction}  Enter Count Corection  ${Correction_Reason}
    ...  AND    POS_A_More_Options.Check Station State  Station=Cash drawer  State=closed


Enter Count Corection
    [Documentation]
    ...  This keyword enters the reason for the count difference or correction to be done and clicks on confirm.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Correction_Reason}
    POS_E_More_Options.Enter Correction Reason  ${Correction_Reason}
    POS_E_More_Options.Click Confirm


Open Safe Via GUI
    [Documentation]
    ...  This keyword opens the Safe in the More Options menu. When opening the safe the amount should be given
    ...  which is the amount of 10 dollar bills the be counted. Optionally the arguments `Correction` and `Correction_Reason`
    ...  can be given when there is a difference in money when the safe was closed.\n\n
    ...  Because it can not be certain if a count correction is needed, the keyword will be called, and will pass on to
    ...  the next keyword wether it fails or not. This will make it easier for the test to succeed and no manual setup is needed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The amount of 10 dollars to be counted in the cash drawer | 12 | Mandatory |
    ...                 | `Correction` | A boolean th signal if a correction needs to be done. Default=${True} | True/False | Optional |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Amount}  ${Correction}=${True}   ${Correction_Reason}=Surplus
    POS_E_More_Options.Select More Options Menu
    ${Closed}  run keyword and return status  POS_A_More_Options.Check Station State  Station=Safe    State=closed
    run keyword unless  ${Closed}  fail  Safe was not closed
    run keyword if  ${Closed}  run keywords
    ...         POS_E_More_Options.Click On Safe
    ...  AND    POS_E_More_Options.Enter 10 Dollar              ${Amount}
    ...  AND    POS_E_More_Options.Click Open Station
    ...  AND    run keyword and return status  run keyword if  ${Correction}  POS_E_More_Options.Close Count Correction Popup  Open
    ...  AND    run keyword and return status  run keyword if  ${Correction}  Enter Count Corection  ${Correction_Reason}
    ...  AND    POS_A_More_Options.Check Station State  Station=Safe    State=open


Close Safe
    [Documentation]
    ...  This keyword closes the Safe in the More Options menu. When closing the cash drawer the amount should be given
    ...  which is the amount of 10 dollar bills the be counted. Optionally the arguments `Correction` and `Correction_Reason`
    ...  can be given when there is a difference in money when the safe was opened.\n\n
    ...  Because it can not be certain if a count correction is needed, the keyword will be called, and will pass on to
    ...  the next keyword wether it fails or not. This will make it easier for the test to succeed and no manual setup is needed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Amount` | The amount of 10 dollars to be counted in the cash drawer | 12 | Mandatory |
    ...                 | `Correction` | A boolean th signal if a correction needs to be done. Default=${True} | True/False | Optional |
    ...                 | `Correction_Reason` | The reason for the correction | Surplus | Optional |
    [Arguments]  ${Amount}  ${Correction}=${True}  ${Correction_Reason}=Surplus
    POS_E_More_Options.Select More Options Menu
    ${Open}  run keyword and return status  POS_A_More_Options.Check Station State  Station=Safe    State=open
    run keyword unless  ${Open}  fail  Safe was not open
    run keyword if  ${Open}  run keywords
    ...         POS_E_More_Options.Click On Safe
    ...  AND    POS_E_More_Options.Enter 10 Dollar          ${Amount}
    ...  AND    POS_E_More_Options.Click Close Station
    ...  AND    run keyword and return status  run keyword if  ${Correction}  POS_E_More_Options.Close Count Correction Popup  Close
    ...  AND    run keyword and return status  run keyword if  ${Correction}  Enter Count Corection  ${Correction_Reason}
    ...  AND    POS_A_More_Options.Check Station State  Station=Safe    State=closed


                                ####################
                                #                  #
                                # Financial Period #
                                #                  #
                                ####################


Close Financial Period
    [Documentation]
    ...  This keyword Closes a financial period by opening the 'More options' menu and clicking on the option
    ...  'Close Financial Period'. It will then click on 'close financial period' and acknowlegdes the popup.\n\n
    ...  Make sure the safe and all stations are closed before performing this step.
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Click On Close Financial Period
    run keyword and return status  POS_E_More_Options.Acknowledge Popup


Open Financial Period Via GUI
    [Documentation]
    ...  This keyword opens a financial period by clicking on the popup shown right after the GUI is opened and there
    ...  has been logged into the POS.\n\nMake sure the FP was closed before this keyword, otherwise no popup is shown.
    ...  Also add the argument `Station=${False}` to 'S_Login.Login' because the popup of the station will not be shown.
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Click On Open Financial Period


Open Financial Period Via Popup
    [Documentation]
    ...  This keyword will acknowledge the popup that is shown when logged into when no financial period is open.
    POS_E_More_Options.Acknowledge Popup


Get Financial Period Nr
    [Documentation]
    ...  This keyword will fetch the current Financial period number by navigating to 'More options>Reports'
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Financial_Period_Nr` | The financial period number | Int | 433 |
                                POS_E_More_Options.Select More Options Menu
                                POS_E_More_Options.Click Reports
                                POS_E_More_Options.Open Current Financial Period
    ${Financial_Period_Nr}=     POS_E_More_Options.Fetch Financial Period Nr
    [Return]  ${Financial_Period_Nr}


                                ############
                                #          #
                                # Giftcard #
                                #          #
                                ############

Check Giftcard Amount
    [Documentation]
    ...  This keyword will navigate to the "Check Giftcard Amount" screen, enter the giftcard number and press 'Check Balance'
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Giftcard_Nr` | The giftcard number | 999000920008 | Mandatory |
    [Arguments]  ${Giftcard_Nr}
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Click Check Giftcard Amount
    POS_E_More_Options.Enter Giftcard Number        ${Giftcard_Nr}
    POS_E_More_Options.Click Check Balance


                                ############
                                #          #
                                #  System  #
                                #          #
                                ############


Connect Companion App
    [Documentation]
    ...  This keyword will navigate to the screen where the companion app can be connected to the station and will check
    ...  if a QR-code will be generated
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Click Configure Manual Scanner
    POS_E_More_Options.Check Barcodes


Connect Customer Facing Display
    [Documentation]
    ...  This keyword will navigate to the screen where the CFD can be connected to the station and will check
    ...  if a QR-code will be generated
    POS_E_More_Options.Select More Options Menu
    POS_E_More_Options.Click Set-up CFD
    POS_E_More_Options.Check Pair QR-code
