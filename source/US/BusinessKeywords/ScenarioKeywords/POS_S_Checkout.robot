*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Checkout.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Basket.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot


*** Keywords ***
Add Discount
    [Documentation]
    ...  This keyword adds a discount on the selected product, where the argument `${Type}` selects which discount
    ...  should be used and `${Amount}` can be used to select a variable amount of discount.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Type` | The type of discount given | variable amount, fixed amount, variable percentage, fixed percentage | Mandatory |
    ...                 | `Amount` | The amount of discount to be given as percentage or amount money | 30 | Optional |
    ...                 | `List_Element` | A numeric value which product in the list should be selected | Default=1 | Optional |
    [Arguments]  ${Type}  ${Amount}=${EMPTY}  ${List_Element}=1
    POS_E_Checkout.Select Product           ${List_Element}
    POS_E_Checkout.Click On Discount Icon
    POS_E_Checkout.Select Discount          ${Type}  ${Amount}


Add Tax Exempt
    [Documentation]
    ...
    [Arguments]  ${Tax_Exempt}=Diplomat
    POS_E_Checkout.Select Tax Exempt
    POS_E_Checkout.Select Tax Exempt Reason     ${Tax_Exempt}
    POS_E_Checkout.Click Submit


Select All Buttons
    [Documentation]
    ...  This keyword will select all buttons in the checkout screen to check if they all function correctly and will
    ...  make a screenshot after the button has been pressed to verify the correct information is shown.
    ...  The lines that are greyed-out are for buttons that can be present and used in other Americal States
#    POS_E_Checkout.Add Big Bag  ${True}
#    ADMIN_S_MainKeywords.Take Screenshot  Big Bag
    POS_E_Checkout.Select Order Remark
    ADMIN_S_MainKeywords.Take Screenshot  Order Remark
    POS_E_Checkout.Select Partial Shipment
    ADMIN_S_MainKeywords.Take Screenshot  Partial Shipping
    POS_E_Checkout.Select Tax Exempt
    ADMIN_S_MainKeywords.Take Screenshot  Tax Exempt
    POS_E_Checkout.Select Sold By
    ADMIN_S_MainKeywords.Take Screenshot  Sold By
    SeleniumLibrary.Click Element         //item-search//ion-toolbar/ion-buttons
    POS_E_Checkout.Select Cash            Cash_Amount=${EMPTY}
    ADMIN_S_MainKeywords.Take Screenshot  Cash
    POS_E_Checkout.Select Card
    ADMIN_S_MainKeywords.Take Screenshot  Card
    POS_E_Checkout.Select Giftcard
    ADMIN_S_MainKeywords.Take Screenshot  Giftcard
    POS_E_Checkout.Select Manual EFT
    ADMIN_S_MainKeywords.Take Screenshot  Manual EFT
    POS_E_Checkout.Select Scotch EGC
    ADMIN_S_MainKeywords.Take Screenshot  Scotch EGC


Add Order Remark
    [Documentation]
    ...  This keyword will add a order remark with the given argument. This is usually the testcase name so it can be
    ...  located more easily in the backend.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Remark` | The order remark to be filled in | TRB-0001.1 | Mandatory |
    [Arguments]  ${Remark}
    Run Keyword unless  '${Remark}'=='${False}'  Run Keywords
    ...         POS_E_Checkout.Select Order Remark
    ...  AND    POS_E_Checkout.Input Order Remark   ${Remark}





