*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Search.robot
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}POS_E_Barcode.robot


*** Keywords ***
Search Product On Name
    [Documentation]
    ...  This keyword will search for a procude based upon the ${Product_Name}. It will select the correct produced based
    ...  upon the ${Product_Code} that is linked to it.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Product_Name` | The name of the product that should be searched for | Blue denim | Mandatory |
    ...                 | `Product_Code` | The product code | 150634_3047-L | Mandatory |
    [Arguments]  ${Product_Name}  ${Product_Code}
    POS_E_Search.Select Search Menu
    POS_E_Search.Enter Text Into Searchbar  ${Product_Name}
    POS_E_Search.Select Product             ${Product_Code}


Search Product On Product Code
    [Documentation]
    ...  This keyword will search for a procude based upon the ${Product_Name}. It will select the correct produced based
    ...  upon the ${Product_Code} that is linked to it.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Product_Code` | The product code without the size marker | 150634_3047 | Mandatory |
    ...                 | `Size` | The products' size | S/M/L | Mandatory |
    [Arguments]  ${Product_Code}  ${Size}
                            POS_E_Search.Select Search Menu
    ${Short_Product_Code}=  POS_E_Search.Split Product Code         ${Product_Code}
                            POS_E_Search.Enter Text Into Searchbar  ${Short_Product_Code}
                            POS_E_Search.Select Product             ${Product_Code}
                            POS_E_Search.Select Size                ${Size}
                            POS_E_Barcode.Click Add To Basket
                            POS_E_Barcode.Check If Product Is Added  ${Short_Product_Code}


Add Big Bag
    [Documentation]
    ...  Thisd keyword will add a big bag to the order via the 'Services' tab in the search options
    POS_E_Search.Select Search Menu
    POS_E_Search.Select Services
    POS_E_Search.Enter Text Into Searchbar  Shopping Bag
    POS_E_Search.Click On Big Bag


