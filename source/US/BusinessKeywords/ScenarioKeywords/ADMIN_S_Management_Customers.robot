*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Management_Customers.robot


*** Keywords ***
Delete Customer
    [Documentation]
    ...  This keyword deletes a customer by giving in the first name.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= |
    ...  | Name | The first name of the customer that should be deleted | Default=Pieter | Mandatory |
    [Arguments]  ${Name}=Pieter
    ADMIN_E_Management_Customers.Enter Name         ${Name}
    ADMIN_E_Management_Customers.Select Customer
    ADMIN_E_Management_Customers.Click Delete
    ADMIN_E_Management_Customers.Acknowledge Deletion Popup

