*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Finance_Overview.robot


*** Keywords ***
Open Bookkeeping
    [Documentation]
    ...  This keyword opens the bookkeeping of the given financial period. It will click on the card on the screen
    ...  with the given (closed) financial period and will go to the 'Bookkeeping' tab.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Financial_Period` | The financial period number | 231 | Mandatory |
    [Arguments]  ${Financial_Period}
    ADMIN_E_Finance_Overview.Click On Card Containing ID  ${Financial_Period}
    ADMIN_E_Finance_Overview.Click On Bookkeeping


Open Documents
    [Documentation]
    ...  This keyword opens the documents of the given financial period. It will click on the card on the screen
    ...  with the given (closed) financial period and will go to the 'Documents' tab.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Financial_Period` | The financial period number | 231 | Mandatory |
    [Arguments]  ${Financial_Period}
    ADMIN_E_Finance_Overview.Click On Card Containing ID  ${Financial_Period}
    ADMIN_E_Finance_Overview.Click On Documents
