*** Settings ***
Library  String
Library  Screenshot
Library  Collections
Library  OperatingSystem

*** Variables ***
${xpath_Cookbook}=      //eva-header//li//span[text()='Cookbook']
${xpath_Pricelists}=    //eva-header//li//span[text()='Pricelists']
${xpath_Finance}=       //eva-header//li//span[text()='Finance']
${xpath_Stock}=         //eva-header//li//span[text()='Stock']
${xpath_Purchase}=      //eva-header//li//span[text()='Purchase']
${xpath_Management}=    //eva-header//li//span[text()='Management']
${xpath_Recipes}=       //cookbook-recipes-overview//h3[contains(text(),'Recipes')]
${xpath_PL_Overview}=   //price-lists-overview//h3[contains(text(),'Price list overview')]
${xpath_Events}=        //cookbook-events-overview//h3[contains(text(),'Events')]
*** Keywords ***
Booking Selector
    [Documentation]
    ...  This keyword selects the correct booking file based on the given arguments for the "Organization".
    ...  To select the correct csv file, a string is created from a base string and the store-code is added to the string.
    ...  when adding a new state for testing witht the framework, it should be added to the dictionary and a file should be created
    ...  in the input folders of the testcases for these new states.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Organization` | The organization derived from the ENV file, where the system should log into | US003 | Mandatory |
    ...                 | `Booking` | A string containing the location of the input folder and file | ${testdir}${/}input${/}expected booking_{}.csv | Mandatory |
    [Arguments]  ${Organization}  ${Booking}
    ${Store_dict}   Create Dictionary               US006=US  US023=US  US024=US
                    Dictionary Should Contain Key   ${Store_dict}  ${Organization}  msg=Store is not present in the dictionary
    ${Store}        Get from Dictionary             ${Store_dict}  ${Organization}
    ${Booking}=     Replace String                  ${Booking}  {}  ${Store}
                    set test variable               ${Expected_Booking}  ${Booking}

Select Store
    [Documentation]
    ...  This keyword Selects the correct store from the dropdown list on top of the ADMIN page. It will wait for the page
    ...  to reload to be sure the next action is done in the correct store.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Store` | The number of the store | 023 | Mandatory |
    [Arguments]  ${Store}
    SeleniumLibrary.Wait until element Is Visible   //span[contains(@class,'organization') and contains(text(),'Scotch & Soda')]  10
    SeleniumLibrary.Click Element   //span[contains(@class,'organization')]
    Sleep  1s  #To be sure the page has reloaded
    SeleniumLibrary.Wait until element Is Visible   //eva-organization-unit-flow//input  5
    SeleniumLibrary.Input Text      //eva-organization-unit-flow//input  ${Store}
    SeleniumLibrary.Wait until element Is Visible   //li/span[contains(text(),'${Store}')]  5
    SeleniumLibrary.Click Element   //li/span[contains(text(),'${Store}')]
    SeleniumLibrary.Wait until element is visible   //table/tbody/tr[15]  10
    SeleniumLibrary.Wait until element is visible   //span[contains(@class,'organization') and contains(text(),'${Store}')]


Select Management Customers
    [Documentation]
    ...  This keyword clicks on Cookbook and Cookbook>Events
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Management}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Management}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'management/customers')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Management menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Management}/ancestor::li//a[contains(@href,'management/customers')]
    SeleniumLibrary.Wait until element Is Visible               //h1[text()='Management - Customers']    10


Select Finance Overview
    [Documentation]
    ...  This keyword clicks on Finance and Finance>Overview
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Finance}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Finance}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'finance/overview')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Finance menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Finance}/ancestor::li//a[contains(@href,'finance/overview')]
    SeleniumLibrary.Wait until element Is Visible               //h3[text()='Periods']    10


Select Finance General Ledgers
    [Documentation]
    ...  This keyword clicks on Finance and Finance>General Ledgers
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Finance}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Finance}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'general-ledgers')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Finance menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Finance}/ancestor::li//a[contains(@href,'general-ledgers')]
    SeleniumLibrary.Wait until element Is Visible               //h3[text()='General ledgers']    10


Select Stock Receive Goods
    [Documentation]
    ...  This keyword clicks on Stock and Stock>Receive Goods
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Stock}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Stock}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'stock/products')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Stock menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Stock}/ancestor::li//a[contains(@href,'stock/shipments')]
    SeleniumLibrary.Wait until element Is Visible               //h3[text()='Receive goods']    10


Select Purchase Orders
    [Documentation]
    ...  This keyword clicks on Purchase and Purchase>Orders
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Purchase}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Purchase}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'purchase/orders')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Purchase menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Purchase}/ancestor::li//a[contains(@href,'purchase/orders')]
    SeleniumLibrary.Wait until element Is Visible               //h1/strong[text()='Purchase orders']    10


Select Pricelists Overview
    [Documentation]
    ...  This keyword clicks on Pricelists and Pricelists>Overview
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Pricelists}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element                          ${xpath_Pricelists}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     //a[contains(@href,'pricelists')]/ancestor::ul[contains(@class,'in collapse')]
    ...                                                         5  error:The Pricelist menu was not opened in time
    SeleniumLibrary.Click Element                               ${xpath_Pricelists}/ancestor::li//span[contains(text(),'Overview')]
    SeleniumLibrary.Wait until element Is Visible               ${xpath_PL_Overview}   10


Select Cookbook Recipes
    [Documentation]
    ...  This keyword clicks on Cookbook and Cookbook>Recipes
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Cookbook}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element          ${xpath_Cookbook}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     ${xpath_Cookbook}/ancestor::li//a[contains(@href,'recipes')]/../../../ul[contains(@class,'in collapse')]
    ...                                         10  error:The Cookbook menu was not opened in time
    SeleniumLibrary.Click Element               ${xpath_Cookbook}/ancestor::li//a[contains(@href,'recipes')]
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Recipes}   10


Select Cookbook Events
    [Documentation]
    ...  This keyword clicks on Cookbook and Cookbook>Events
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Cookbook}  10
    wait until keyword succeeds  2x  0.5s  run keywords
    ...  SeleniumLibrary.Click Element          ${xpath_Cookbook}
    ...  AND  SeleniumLibrary.Wait until element Is Visible     ${xpath_Cookbook}/ancestor::li//a[contains(@href,'events')]/../../../ul[contains(@class,'in collapse')]
      ...                                       10  error:The Cookbook menu was not opened in time
    SeleniumLibrary.Click Element               ${xpath_Cookbook}/ancestor::li//a[contains(@href,'events')]
    SeleniumLibrary.Wait until element Is Visible               ${xpath_Events}    10


Take Screenshot
    [Documentation]  This keyword captures a keyword from the (end)screen of a assertion keyword.
    ...  A generic directory is chosen where the test is runned from. This can be changed in the future.
    ...  The keyword accepts one keyword, which is the name for the screenshot.
    ...  *Parameters:*
    ...  | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...  | FileName | Name of file that will be created | FileName=Recipes_table | Mandatory |
    ...  | Directory | The directory where the file will be stored | Default=${Testdir}${/}results${/}Screenshots${/} | Optional |
    [Arguments]  ${FileName}  ${Directory}=${Testdir}${/}results${/}Screenshots${/}
    OperatingSystem.create directory        ${Directory}
    Screenshot.set screenshot directory     ${Directory}
    Screenshot.Take Screenshot              ${FileName}
