*** Settings ***
Resource   ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Cookbook_Events.robot


*** Keywords ***
Process Financial Event
    [Documentation]
    ...  This keyword processes the financial event of a given order number or financial period. To do so the status
    ...  should be 'unprocessed' en should be set to 'prcessed'. It wil firstly select 100 rows to show, check the
    ...  amount of rows in the table, enter the order number check the amount of rows again, compare it, to be sure
    ...  the table has updated. Only then it will press the button to process the events. After that it will reload
    ...  the page to check if the stats has changed, ultimately from 'unprocessed' to 'processed'.\n\nBoth
    ...  Order_Nr or Financial_Period can be given to be processed.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | The order number that should be processed | 3545 | Optional |
    ...                 | `Financial_Period` | The fianacial period that should be processed | 344 | Optional |
    ...                 | `Status` | The expected status to be found in the table | Default=Processed | Optional |
    [Arguments]     ${Order_Nr}=${None}  ${Financial_Period}=${None}  ${Status}=Processed
                    ADMIN_E_Cookbook_Events.Select 100 rows
    ${Rows_before}  ADMIN_A_Cookbook_Events.Check amount of rows
                    run keyword unless  '${Order_Nr}'=='${None}'
    ...             ADMIN_E_Cookbook_Events.Enter Search Cirteria       Number=${Order_Nr}          Column=OrderID
                    run keyword unless  '${Financial_Period}'=='${None}'
    ...             ADMIN_E_Cookbook_Events.Enter Search Cirteria       Number=${Financial_Period}  Column=FinancialPeriodID
    ${Rows_after}   ADMIN_A_Cookbook_Events.Check amount of rows
                    ADMIN_E_Cookbook_Events.Check rows difference       ${Rows_before}  ${Rows_after}
                    ADMIN_E_Cookbook_Events.Process Events

Open Order
    [Documentation]
    ...  This keyword searches on an order by filling in the `${Order_Nr}` into the given search criteria column.
    ...  This can be the Order_Nr or the Financial period. It will select the order by clicking on the clickable
    ...  order number.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | The order number | 3466 | Mandatory |
    ...                 | `Column` | The column where the order number should be filled | OrderID | Mandatory |
    [Arguments]   ${Order_Nr}  ${Column}
    ADMIN_E_Cookbook_Events.Enter Search Cirteria   Number=${Order_Nr}  Column=${Column}
    ADMIN_E_Cookbook_Events.Select Order            ${Order_Nr}

Calculate Balance
    [Documentation]
    ...  This keyword enters and selects the order, fetches the amount of debit and credit in the table and checks if the
    ...  sum of those is zero.
    ...                  *Return Value:*s
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Debit` | int | the total amount of debit shown in the table | 124 |
    ...                 | `Credit` | int | the amount of credit shown in the table | 410 |
    ...                 | `Balance` | int | the sum of the debit-credit, which should be zero | 0 |
                        ADMIN_E_Cookbook_Events.Select Tab Bookings
    ${status}=          Run Keyword And Return Status                       variable should exist   ${Regression}
                        run keyword if  ${status}                           ADMIN_S_MainKeywords.Take Screenshot
    ...                                                                     Booking_Overview  ${Folder}${/}Screenshots${/}${TEST NAME}
    ${Total_Debit}      ADMIN_E_Cookbook_Events.Calculate Debit
    ${Total_Credit}     ADMIN_E_Cookbook_Events.Calculate Credit
    ${Balance}          ADMIN_E_Cookbook_Events.Calculate Total Balance     ${Total_Debit}  ${Total_Credit}
                        ADMIN_E_Cookbook_Events.Check Zero Balance          ${Balance}


Create Booking Overview
    [Documentation]
    ...  This keyword creates a booking overview from the GUI table and puts it into a csv file. First it will fetch
    ...  all the account names from the table, sort them alphabetically and removes the duplicates. With that list it will
    ...  fetch the prices, organization and date for those lines. With that data it will create the csv which will contain
    ...  one line per account name with the combined prices/values and organization name and date.
    ...                  *Parameters:*
    ...                 | =Parameter= | =Description= | =Example= | =Mandatory or Optional= |
    ...                 | `Order_Nr` | The order number of the booking | 2331 | Mandatory |
    ...                  *Return Value:*
    ...                 | =Value= | =Type= | =Description= | =Example= |
    ...                 | `Booking_df` | dataframe | A dataframe with all the data of the booking shown in the GUI | X |
    [Arguments]  ${Order_Nr}
    @{Account_list}                                     ADMIN_E_Cookbook_Events.Fetch Account Names
    ${Price_list}  ${Organization_list}  ${Date_list}   ADMIN_E_Cookbook_Events.Fetch Account Data      @{Account_list}
    ${Booking_df}                                       ADMIN_E_Cookbook_Events.Create Bookings CSV     ${TEST NAME}.csv  ${Account_list}
    ...                                                                                                 ${Price_list}  ${Order_Nr}
    ...                                                                                                 ${Date_list}  ${Organization_list}
    [Return]  ${Booking_df}


Check Documents
    [Documentation]
    ...  This keyword checks in the Documents tab if there are any documents present
    ADMIN_E_Cookbook_Events.Select Tab Documents
    SeleniumLibrary.Wait until element Is Visible                   //order-blobs//table//tbody/tr[1]  5  error=No Document entry found


Check Invoices
    [Documentation]
    ...  This keyword checks in the Invoices tab if there are any documents present
    ADMIN_E_Cookbook_Events.Select Tab Invoices
    SeleniumLibrary.Wait until element Is Visible                   //order-invoice-list//table//tbody/tr[1]  5  error=No Invoices entry found


