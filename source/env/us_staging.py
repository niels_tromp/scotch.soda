POS_URL = 'https://eva-pos-scotch-beta-staging.firebaseapp.com/login'
ADMIN_URL = 'https://admin.scotch.test.eva-online.cloud/#!/login'
Organization = 'US024'
Supplier=        'Bergen US'
Dev_Scotch_Product_Code_1 = '150634-3047'
Dev_Scotch_Size_1 = 'L'
Dev_Scotch_Size_2 = 'M'
Dev_Scotch_Product_Code_2 = '152108-0008'
Dev_Scotch_Product_Code_3 = '124889-50'

Dev_Licenced_Product_Code_1 = 'SS6006-477'
Dev_Licenced_Size_1 = 'OS'
Dev_Licenced_Product_Code_2 = '19731086-S290'
Dev_Licenced_Size_2 = '42-EU'
Dev_Licenced_Product_Code_3 = '151609-220'