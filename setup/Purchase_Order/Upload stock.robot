*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Home.robot
Resource    ${sourcedir}${/}AssertionKeywords${/}ADMIN_A_Cookbook_Events.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Purchase_Orders.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Stock_Receive_Goods.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ElementKeywords${/}ADMIN_E_Purchase_Orders.robot


*** Variables ***
${testdir}=     ${CURDIR}
${csv}=         ${testdir}${/}input${/}products.csv

*** Test Cases ***
Upload products to store stock
                    S_Login.Login                                       ADMIN  ${Organization}
                    ADMIN_S_MainKeywords.Select Store                   ${Supplier}
                    ADMIN_S_MainKeywords.Select Purchase Orders
    ${Order_Nr}=    ADMIN_S_Purchase_Orders.Create New Purchase Order   ${Supplier}  ${Organization}  ${csv}
                    ADMIN_S_MainKeywords.Select Purchase Orders
                    ADMIN_S_Purchase_Orders.Create New Shipment         ${Order_Nr}
                    ADMIN_S_Purchase_Orders.Receive Order               ${Order_Nr}
#    [Teardown]      SeleniumLibrary.Close Browser

