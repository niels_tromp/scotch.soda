*** Settings ***
Variables   ${sourcedir}${/}..${/}env${/}${ENV}.py
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}S_Login.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_MainKeywords.robot
Resource    ${sourcedir}${/}BusinessKeywords${/}ScenarioKeywords${/}ADMIN_S_Pricelists_Overview.robot

*** Variables ***
${testdir}=         ${CURDIR}
${Pricelist_Name}=  SP_USA
${Pricelist}=       ${testdir}${/}input${/}Pricelist.csv


*** Test Cases ***
Product prices
    [Documentation]
    ...     | Author: | Niels Tromp |
    ...     | Date: | 22-10-2020 |
    ...     | Goal: | Execute a standard sale transaction and check the financial booking |
    ...     | Expected result: | The transaction is succeful and the bookings are as expected |
    ...     Steps:
    ...     | Test: |  |
    ...     | 1 | Log into the POS |
    ...     | 2 | Search for a Scotch product |
    ...     | 3 | Checkout the item with a cash payment |
    ...     | 4 | Close the browser and log into the ADMIN |
    ...     | 5 | Go to Cookbook>Events |
    ...     | 6 | Process the financial events |
    ...     | 7 | Verify the bookings |
    ...     | Teardown: |  |
    ...     | 8 | Close the browser |
#    [Tags]


                        S_Login.Login                                       ADMIN  ${Organization}
                        ADMIN_S_MainKeywords.Select Pricelists Overview
                        ADMIN_S_Pricelists_Overview.Upload New Pricelist    ${Pricelist_Name}   ${Pricelist}



    [Teardown]          SeleniumLibrary.Close Browser

